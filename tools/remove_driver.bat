@echo off
set name=MwMon

call stop_driver.bat

powershell -C "Get-CimInstance Win32_SystemDriver -Filter \"name='%name%'\" | Invoke-CimMethod -MethodName Delete"
rundll32.exe setupapi,InstallHinfSection DefaultUninstall 132 .\%name%.inf
del /f /q c:\windows\system32\drivers\%name%drv64.sys
