#!/usr/bin/env python3
"""
Generate hooks.c template code for the given API (to be improved/adapted as needed).

Author: vtopan/gmail.
"""

import re
import sys


IGNORE_PARAM_TOKENS = ['_Frees_ptr_opt_', '_In_', '_Out_', 'IN', 'OUT', 'OPTIONAL', 'const']
IGNORE_FTYPE_TOKENS = ['DPAPI_IMP']
PARAM_TYPE_ALIASES = {
    'PVOID': ['LPVOID', 'LPCVOID', 'HLOCAL', 'HGLOBAL'],
    'LPSTR': ['LPCSTR'],
    'LPWSTR': ['LPCWSTR', 'PCWSTR'],
    }
PARAM_TYPE_MAP = {e:k for k, v in PARAM_TYPE_ALIASES.items() for e in v}
PARAM_FMTS = {
    r'\"%s\"': ['LPSTR'],
    r'\"%S\"': ['LPWSTR'],
    '0x%X': ['DWORD', 'INT'],
    r'\"%wZ\"': ['PUNICODE_STRING'],
    }
PARAM_FMT_MAP = {e:k for k, v in PARAM_FMTS.items() for e in v}
PARAM_MACRO_MAP = {
    'LPSTR': 'PRINT_STR',
    'LPWSTR': 'PRINT_WSTR',
    }
RX_POINTERS = re.compile(r'^(L?P|HANDLE)|\*')


hooks = {}
filename = sys.argv[1] if len(sys.argv) > 1 else 'sample_api_def.txt'
for ftype, fname, params, dllname in re.findall(r'^([A-Za-z][\w \*]+?) ([A-Z]\w+)\($(.+?)\);(?:\s+//\s+(\w+\.dll.*?)$)?', open(filename).read(), flags=re.M|re.S):
    ftype = ' '.join([x for x in ftype.split(' ') if x not in IGNORE_FTYPE_TOKENS])
    params = [re.sub(r'\s+', ' ', x.strip()).rstrip(',') for x in params.split('\n') if x.strip()]
    params = [[PARAM_TYPE_MAP.get(ee, ee) for ee in e.split(' ') if ee not in IGNORE_PARAM_TOKENS] for e in params]
    ftype = PARAM_TYPE_MAP.get(ftype, ftype)
    dlls = [x.strip() for x in dllname.split(',')] if dllname else ['kernel32.dll']
    code = [f'HOOKFUN({ftype})\nhook_{fname}(']
    code.append(',\n'.join(('    %s' % ' '.join(p)) for p in params))
    code += ['    )', '{', f'    {ftype} result = 0;\n\n    HOOK_ENTER({fname});\n']
    call = f'    result = ({ftype})((fun{len(params)}arg)(Hooks[HOOKID_{fname}].orig_addr))('
    call_params = []
    for p in params:
        if len(p) != 2:
            raise ValueError(f'Failed parsing parameter for {fname}: {" ".join(p)}')
        is_pointer = RX_POINTERS.search(p[0]) or '*' in p[1]
        call_params.append((f'(PVOID)' if not is_pointer else '') + p[1].strip('*'))
    code.append(call + ', '.join(call_params) + ');')
    pformat = ', '.join(PARAM_FMT_MAP.get(p[0].upper(), '0x%p') for p in params)
    call = f'    log("APICALL: {fname}({pformat}) => 0x%p", '
    call_params = []
    for p in params:
        cparam = p[1].strip('*')
        if p[0] in PARAM_MACRO_MAP:
            cparam = f'{PARAM_MACRO_MAP[p[0]]}({cparam})'
        call_params.append(cparam)
    code.append(call + ', '.join(call_params) + ', result);')
    code += ['\n    HOOK_LEAVE();\n', '    return result;', '}\n']

    code = '\n'.join(code) + '\n'
    hooks[fname] = {'type': ftype, 'params': params, 'dlls': dlls, 'code': code}

hook_ids = [f'    HOOKID_{e},' for e in hooks]
hook_entries = ['    {hook_%s, "%s", "%s"},' % (k, v['dlls'][0], k) for k, v in hooks.items()]

print('\n'.join(hook['code'] for hook in hooks.values()))
print('\n'.join(hook_ids) + '\n\n\n')
print('\n' + '\n'.join(hook_entries) + '\n')
