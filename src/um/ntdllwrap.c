/*
 * ntdll wrapper.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#include "ntdll.h"


FUN_swprintf_s ntdll_swprintf_s = NULL;
FUN_snprintf ntdll_snprintf = NULL;
FUN_vsprintf_s ntdll_vsprintf_s = NULL;


void
init_ntdll_wrappers(
    void
    )
{
    HMODULE ntdll = LoadLibraryA("ntdll.dll");
    ntdll_swprintf_s = (FUN_swprintf_s)GetProcAddress(ntdll, "swprintf_s");
    ntdll_snprintf = (FUN_snprintf)GetProcAddress(ntdll, "_snprintf");
    ntdll_vsprintf_s = (FUN_vsprintf_s)GetProcAddress(ntdll, "vsprintf_s");
}