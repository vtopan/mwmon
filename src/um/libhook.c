/*
 * mwmon/libhook: Win32 API hooking library.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#include "libhook.h"


// externs from hooks.c
extern HOOK Hooks[];
extern PCHAR HookedDlls[];
extern DWORD HookedDllCount;
extern DWORD HookCount;
extern FORWARDER_HOOK ForwarderHooks[];
extern DWORD ForwarderHookCount;


static PVOID LLD_COOKIE = NULL;
char* BITS_STR[2] = {"32", "64"};
DWORD TLSID_INHOOK;
FUN_LdrRegisterDllNotification LdrRegisterDllNotification;
PVOID HOOK_GATES;   // dynamically allocated above all DLLs (above ntdll.dll in practice)
int GENHOOK_CRT_IDX = -1;
CRITICAL_SECTION GH_CRIT_SEC = {0};
PBYTE DYN_GEN_HOOK_SLOT = NULL;
SSIZE_T DYN_GEN_HOOK_CRT = -1;
SSIZE_T MAX_DYN_GEN_HOOKS = CFG_HOOK_GETPROC_EXPORTS ? (GENHOOK_SLOT_SIZE / GENHOOK_STUB_SIZE - 1) : 0;


/**
 * Inject the mwmon DLL in a process.
 *
 * Returns a thread handle on success, NULL on failure.
 */
__declspec(dllexport)
HANDLE
inject_mwmon_dll(
    HANDLE process
    )
{
    DWORD result = ERROR_SUCCESS;
    BOOL is64 = FALSE;
    char* dll_path;
    HMODULE kernel32;
    PBYTE addr, ll_addr;
    BYTE loader[512];
    SIZE_T pathlen, written;
    HANDLE thread = NULL;

#ifdef _M_X64
    CHECK_CALL_BOOL(IsWow64Process(process, &is64));
    is64 = !is64;
#endif

    if (SELF64 != is64)
    {
        // todo: un-lazily resolve cross-bits LoadLibraryA address
        err("Mismatched target: self=%s, remote process=%s", BITS_STR[SELF64], BITS_STR[is64]);
        return FALSE;
    }

    dll_path = is64 ? MWMON_DLL_PATH_64 : MWMON_DLL_PATH_32;

    CHECK_CALL_PTR(addr = VirtualAllocEx(process, NULL, INJECT_SIZE, MEM_RESERVE | MEM_COMMIT,
            PAGE_EXECUTE_READWRITE));
    log("[*] Allocated DLL load stub @ 0x%p...", addr);
    kernel32 = LoadLibrary("kernel32.dll");
    ll_addr = (PBYTE)GetProcAddress(kernel32, "LoadLibraryA");
    FreeLibrary(kernel32);

    pathlen = strlen(dll_path) + 1;

    loader[0] = 0xE8;                                           // call rel32

//    if (is64)
#ifdef _M_X64
    {
        *(__int64*)(loader + 1) = pathlen;
        memcpy(loader + 5, dll_path, pathlen);                  // fixme: 9 instead of 5? use SHELL_*
        loader[pathlen + 5] = 0x59;                             // pop RCX
        *(__int32*)(loader + pathlen + 6) = 0x28EC8348;         // sub RSP, 28
        *(__int16*)(loader + pathlen + 10) = 0xB848;            // mov RAX, ...
        *(__int64*)(loader + pathlen + 12) = (__int64)ll_addr;
        *(__int16*)(loader + pathlen + 20) = 0xD0FF;            // call RAX
        *(__int32*)(loader + pathlen + 22) = 0x28C48348;        // add RSP, 28
        *(__int32*)(loader + pathlen + 26) = 0x04C2;            // ret 0x4
    }
//    else
#else
    {
        *(__int32*)(loader + 1) = (__int32)pathlen;
        memcpy(loader + 5, dll_path, pathlen);
        loader[pathlen + 5] = 0xB8;                             // mov EAX, ...
        *(__int32*)(loader + pathlen + 6) = (__int32)ll_addr;
        *(__int16*)(loader + pathlen + 10) = 0xD0FF;            // call EAX
        *(__int32*)(loader + pathlen + 12) = 0x04C2;            // ret 0x4 = C20400 (clean remote thread parameter from stack)
        *(__int16*)(loader + pathlen + 15) = 0xFEEB;            // jmp $-2
    }
#endif

    CHECK_CALL_BOOL(WriteProcessMemory(process, addr, loader, sizeof(loader), &written));
    CHECK_CALL_BOOL(thread = CreateRemoteThread(process, NULL, 0, (LPTHREAD_START_ROUTINE)addr, 0, 0, NULL));

done:
    return thread;
}


/**
 * Find export (assume PE is 32/64 as this binary).
 *
 * @param imagebase The DLL image base (address)
 * @param name Export name
 * @param exp_ptr (optional, out) Pointer to exported function RVA in EAT
 * @param index (optional, out) Index of exported symbol in EAT
 * @param pexp_dir (optional, out) Pointer to the EAT (PIMAGE_EXPORT_DIRECTORY)
 */
__declspec(dllexport)
DWORD
find_export(
    char* imagebase,
    char* name,
    char** exp_ptr,
    DWORD* index,
    PIMAGE_EXPORT_DIRECTORY* pexp_dir
    )
{
    PIMAGE_DOS_HEADER dosh = (PIMAGE_DOS_HEADER)imagebase;
	PIMAGE_NT_HEADERS nth;
	PIMAGE_EXPORT_DIRECTORY expdir;
    DWORD i;

    nth = (PIMAGE_NT_HEADERS)(imagebase + dosh->e_lfanew);
    expdir = (PIMAGE_EXPORT_DIRECTORY)(imagebase + nth->OptionalHeader.DataDirectory[0].VirtualAddress);
    if (pexp_dir)
    {
        *pexp_dir = expdir;
    }
    for (i = 0; i < expdir->NumberOfNames; i++)
    {
        if (!strcmp(name,
                imagebase + *(DWORD*)(imagebase + expdir->AddressOfNames + i * 4)))
        {
            WORD ordinal = *(WORD*)(imagebase + expdir->AddressOfNameOrdinals + i * 2);
            if (exp_ptr)
            {
                *exp_ptr = imagebase + expdir->AddressOfFunctions + ordinal * 4;
            }
            if (index)
            {
                *index = i;
            }
            return ERROR_SUCCESS;
        }
    }

    return ERROR_PROC_NOT_FOUND;
}


/**
 * Find import (assume PE is 32/64 as this binary).
 */
__declspec(dllexport)
DWORD
find_import(
    char* imagebase,
    char* dll_name,
    char* name,
    char** imp_ptr
    )
{
    PIMAGE_DOS_HEADER dosh = (PIMAGE_DOS_HEADER)imagebase;
	PIMAGE_NT_HEADERS nth;
	PIMAGE_IMPORT_DESCRIPTOR iid;

    nth = (PIMAGE_NT_HEADERS)(imagebase + dosh->e_lfanew);
    iid = (PIMAGE_IMPORT_DESCRIPTOR)(imagebase + nth->OptionalHeader.DataDirectory[1].VirtualAddress);

    while (1)
    {
        PSIZE_T thunk = (PSIZE_T)(imagebase + iid->OriginalFirstThunk);
        DWORD i;

        if (!iid->OriginalFirstThunk)
        {
            break;
        }

        if ((!iid->Name) || (lstrcmpiA(dll_name, imagebase + iid->Name)))
        {
            iid++;
            continue;
        }

        for (i = 0; i < 10000; i++)
        {
            if (!*thunk)
            {
                break;
            }
            if (!IMAGE_SNAP_BY_ORDINAL(*thunk))
            {
                PIMAGE_IMPORT_BY_NAME iin = (PIMAGE_IMPORT_BY_NAME)(imagebase + *thunk);
                if (!lstrcmpA(name, iin->Name))
                {
                    *imp_ptr = imagebase + (size_t)iid->FirstThunk + i * sizeof(SIZE_T);
                    return ERROR_SUCCESS;
                }
            }
            thunk++;
        }

        if (lstrcmpiA(dll_name, imagebase + iid->Name) == 0)
        {
            break;
        }

        iid++;
    }

    return ERROR_PROC_NOT_FOUND;
}


/**
 * Installs a hook.
 */
__declspec(dllexport)
BOOL
patch_with_hook(
    HOOK* hook,
    char* dll_name,
    char* function_name,
    HMODULE dll_imagebase
    )
{
    DWORD result = 0;
    PVOID* patch_addr;
    DWORD page_prot;
    SIZE_T delta;
    HMODULE program = GetModuleHandleA(NULL);

    if (!(hook->orig_addr))
    {
        hook->orig_addr = (char*)GetProcAddress(dll_imagebase, function_name);
    }
    if (!(hook->orig_addr))
    {
        err("Failed resolving %s:%s!", dll_name, function_name);
        return FALSE;
    }
    log("[-] Hooking %s:%s (old: 0x%p, new: 0x%p)...", dll_name, function_name, hook->orig_addr,
            hook->hook_gate_addr);

    CHECK_CALL_STATUS(find_export((PCHAR)dll_imagebase, function_name, (char**)&patch_addr, NULL, NULL));
    delta = ((SIZE_T)(hook->hook_gate_addr) - (SIZE_T)dll_imagebase);
#if CFG_DEBUG_HOOKS
    log("[-] Patching hook VA 0x%p @ EAT(%s:%s) = 0x%p (new value:0x%08X; old value: 0x%08X)", hook->hook_gate_addr, dll_name,
            function_name, patch_addr, (DWORD)delta, *(DWORD*)patch_addr);
    log("[-]   Hook struct. @ 0x%p, code @ 0x%p", hook, hook->hook_addr);
#endif
    CHECK_CALL_BOOL(VirtualProtect((void*)((size_t)patch_addr & ~0xFFF), 0x1000, PAGE_READWRITE,
            &page_prot));
    *(DWORD*)patch_addr = (DWORD)delta;
    CHECK_CALL_BOOL(VirtualProtect((void*)((size_t)patch_addr & ~0xFFF), 0x1000, page_prot,
            &page_prot));

    result = find_import((char*)program, dll_name, function_name, (char**)&patch_addr);
    if (result == ERROR_SUCCESS)
    {
#if CFG_DEBUG_HOOKS
        log("[-] Patching hook VA 0x%p @ IAT(%s:%s) = 0x%p (old value: 0x%p)", hook->hook_gate_addr, dll_name,
                function_name, patch_addr, *(size_t*)patch_addr);
#endif
        CHECK_CALL_BOOL(VirtualProtect((void*)((size_t)patch_addr & ~0xFFF), 0x1000,
                PAGE_READWRITE, &page_prot));
        *patch_addr = hook->hook_gate_addr;
        CHECK_CALL_BOOL(VirtualProtect((void*)((size_t)patch_addr & ~0xFFF), 0x1000, page_prot,
                &page_prot));
    }
    else
    {
        // function not imported, do nothing
#if CFG_DEBUG_HOOKS
        log("[~] Hooked function %s:%s not imported", dll_name, hook->fun_name);
#endif
    }

    result = ERROR_SUCCESS;

done:
    return result == ERROR_SUCCESS;
}


/**
 * Install hooks.
 */
__declspec(dllexport)
BOOL
install_hooks(
    PCHAR dll_name,
    BOOL warn_if_not_loaded
    )
{
    DWORD result = ERROR_SUCCESS;
    HMODULE dll_imagebase = NULL;
    DWORD i;

    dll_imagebase = (HMODULE)GetModuleHandleA(dll_name);
    if (!dll_imagebase)
    {
        if (warn_if_not_loaded)
        {
            err("Attempted to install hooks in %s, which isn't loaded (yet)!", dll_name);
        }
        return FALSE;
    }

    EnterCriticalSection(&GH_CRIT_SEC);

    log("[*] Looking for APIs to hook in %s @ 0x%p...", dll_name, dll_imagebase);

    for (i = 0; i < HookCount; i++)
    {
        HOOK* hook = &Hooks[i];

        if (hook->is_set || stricmp(hook->dll_name, dll_name))
        {
            continue;
        }

        if (patch_with_hook(hook, dll_name, hook->fun_name, dll_imagebase))
        {
            hook->is_set = 1;
        }
    }

    for (i = 0; i < ForwarderHookCount; i++)
    {
        FORWARDER_HOOK* fwhook = &ForwarderHooks[i];

        if (fwhook->is_set || strnicmp(fwhook->dll_name_prefix, dll_name, strlen(fwhook->dll_name_prefix)))
        {
            continue;
        }

#if CFG_DEBUG_HOOKS
        log("[-] Patching forwarded function %s:%s...", dll_name, fwhook->fun_name);
#endif
        if (patch_with_hook(&Hooks[fwhook->hook_id], dll_name, fwhook->fun_name, dll_imagebase))
        {
            fwhook->is_set = 1;
        }
    }

#if CFG_DEBUG_HOOKS
    log("[*] Done installing hooks in %s.", dll_name);
#endif

    LeaveCriticalSection(&GH_CRIT_SEC);
    return result == ERROR_SUCCESS;
}


__declspec(dllexport)
void
__stdcall
generic_hook(
#ifdef _M_X64
    SIZE_T arg1,
    SIZE_T arg2,
    SIZE_T arg3,
    SIZE_T arg4
#else
    PCHAR fun_name,
    PSIZE_T stack
#endif
    )
{
#ifdef _M_X64
    CONTEXT ctx;
    PSIZE_T stack;
    PCHAR fun_name;
#endif
    SIZE_T ret_addr;
    SIZE_T args[4];

    hook_enter();

#ifdef _M_X64
    RtlCaptureContext(&ctx);
    stack = (PSIZE_T)ctx.Rsp;
    fun_name = (PCHAR)ctx.R10;
#endif
    ret_addr = stack[0];
#ifdef _M_X64
    args[0] = arg1;
    args[1] = arg2;
    args[2] = arg3;
    args[3] = arg4;
#else
    memcpy(args, &stack[1], 16);
#endif

    if (hook_depth() == 1)
    {
        log("APICALL: %s(); args: 0x%p, 0x%p, 0x%p, 0x%p; retaddr: 0x%p",
                fun_name, args[0], args[1], args[2], args[3], ret_addr);

#if CFG_GENHOOK_DUMP_STRING_ARGS_HEUR
        {
            DWORD i;

            for (i = 0; i < 4; i++)
            {
                if (!IsBadReadPtr((PVOID)(args[i]), 8))
                {
                    PBYTE arg = (PVOID)(args[i]);
                    DWORD j, size = IsBadReadPtr(arg, 64) ? (IsBadReadPtr(arg, 32) ? (IsBadReadPtr(arg, 16) ? 8 : 16) : 32) : 64;
                    BYTE buf[3 * 32 + 1] = {0};

                    if (IS_ASCII_CHAR(arg[0]) && IS_ASCII_CHAR(arg[2]) && IS_ASCII_CHAR(arg[4]) && IS_ASCII_CHAR(arg[6]))
                    {
                        BOOL unicode = !((IS_ASCII_CHAR(arg[1]) && IS_ASCII_CHAR(arg[3]) && IS_ASCII_CHAR(arg[5]) && IS_ASCII_CHAR(arg[7])));

                        for (j = 0; j < size && arg[j]; j += unicode ? 2 : 1)
                        {
                            buf[unicode ? (j >> 1) : j] = arg[j];
                        }

                        log("  - arg.%d: %s", i + 1, buf);
                    }
    #if CFG_GENHOOK_DUMP_NONSTRING_ARGS_AS_HEX
                    else
                    {
                        if (size > 32)
                        {
                            size = 32;
                        }
                        for (j = 0; j < size; j++)
                        {
                            ntdll_snprintf(&buf[j * 3], 4, "%02X ", arg[j]);
                        }
                        log("  - arg.%d (hex): %s", i + 1, buf);
                    }
    #endif
                }
            }
        }
#endif
    }

    hook_leave();
}

/**
 * Creates a generic stub and patches the EAT with it.
 *
 * Note: this is an internal function, it performs no checks regarding parameter validity.
 *
 * @param dll_imagebase DLL address
 * @param index Index of function to be patched in the EAT
 * @param hook_addr (optional) Pre-allocated buffer of size GENHOOK_STUB_SIZE
 */
__declspec(dllexport)
DWORD
write_generic_hook(
    PBYTE dll_imagebase,
    DWORD index,
    PBYTE* hook_addr
    )
{
    DWORD result = ERROR_SUCCESS;
	PIMAGE_NT_HEADERS nth = (PIMAGE_NT_HEADERS)(dll_imagebase + ((PIMAGE_DOS_HEADER)dll_imagebase)->e_lfanew);
    PIMAGE_DATA_DIRECTORY pexpdd = (PIMAGE_DATA_DIRECTORY)&nth->OptionalHeader.DataDirectory[0];
	PIMAGE_EXPORT_DIRECTORY expdir = (PIMAGE_EXPORT_DIRECTORY)(dll_imagebase + pexpdd->VirtualAddress);
    char* name = dll_imagebase + *(DWORD*)(dll_imagebase + expdir->AddressOfNames + index * 4);
    char* dll_name = (char*)(dll_imagebase + expdir->Name);
    WORD name_ord = *(WORD*)(dll_imagebase + expdir->AddressOfNameOrdinals + index * 2);
    PDWORD patch_addr = (PDWORD)(dll_imagebase + expdir->AddressOfFunctions + name_ord * 4);
    DWORD delta, page_prot, offset = 0;
    size_t crt_va = (size_t)dll_imagebase + *patch_addr;
    PBYTE stub = NULL;

    if ((*patch_addr >= pexpdd->VirtualAddress) && (*patch_addr <= pexpdd->VirtualAddress + pexpdd->Size))
    {
        // forwarded export
        dbg(2, "Skipping forwarded export: %s:%s -> %s", dll_name, name, dll_imagebase + *patch_addr);
        return ERROR_WRONG_TARGET_NAME;
    }

    if (
            (crt_va >= (size_t)HOOKDLL_ADDR) && (crt_va <= (size_t)HOOKDLL_ADDR + HOOKDLL_SIZE)
            ||
            (CFG_HOOK_GETPROC_EXPORTS && (crt_va >= (size_t)DYN_GEN_HOOK_SLOT) && (crt_va <= (size_t)DYN_GEN_HOOK_SLOT + GENHOOK_SLOT_SIZE))
        )
    {
        // explicitly-hooked or dynamically-hooked export
        dbg(2, "Skipping %s-hooked export: %s:%s -> 0x%p",
                ((crt_va >= (size_t)HOOKDLL_ADDR) && (crt_va <= (size_t)HOOKDLL_ADDR + HOOKDLL_SIZE)) ? "explicitly" : "dynamically",
                dll_name, name, crt_va);
        return ERROR_ALREADY_EXISTS;
    }

    if (hook_addr && *hook_addr)
    {
        stub = *hook_addr;
    }
    else
    {
        if (DYN_GEN_HOOK_CRT >= MAX_DYN_GEN_HOOKS)
        {
            err("Maximum number of dynamic generic hooks (%d) exceeded - won't hook %s!", MAX_DYN_GEN_HOOKS, name);
            return ERROR_INSUFFICIENT_BUFFER;
        }
        stub = &DYN_GEN_HOOK_SLOT[GENHOOK_STUB_SIZE * ++DYN_GEN_HOOK_CRT];
        if (hook_addr)
        {
            *hook_addr = stub;
        }
    }

#ifdef _M_X64
    SHELL_RAW(stub, "\x90\x51\x52\x41\x50\x41\x51\x48\x83\xEC\x48\x49\xBA", offset);
    SHELL_QWORD(stub, name, offset);
    SHELL_RAW(stub, "\x48\xB8", offset);
    SHELL_QWORD(stub, &generic_hook, offset);
    SHELL_RAW(stub, "\xFF\xD0\x48\x83\xC4\x48\x41\x59\x41\x58\x5A\x59\x48\xB8", offset);
    SHELL_QWORD(stub, dll_imagebase + *patch_addr, offset);
    SHELL_RAW(stub, "\xFF\xE0\xCC", offset);
    /*
    Sample disassembly:
    90              nop
    51              push    rcx
    52              push    rdx
    4150            push    r8
    4151            push    r9
    4883ec40        sub     rsp,40h
    49bad22252a6fb7f0000 mov r10, 00007ffba65222d2 ("RtlInitializeCriticalSection")
    48b8f07e5b96fb7f0000 mov rax, offset mwmonhk64!generic_hook (00007ffb`965b7ef0)
    ffd0            call    rax
    4883c440        add     rsp,40h
    4159            pop     r9
    4158            pop     r8
    5a              pop     rdx
    59              pop     rcx
    48b8a0ed43a6fb7f0000 mov rax, offset ntdll!RtlInitializeCriticalSection (00007ffb`a643eda0)
    ffe0            jmp     rax
    cc              int     3
    */
#else
    SHELL_BYTE(stub, 0x90, offset);
    SHELL_BYTE(stub, PUSH_ESP, offset);
    SHELL_BYTE(stub, PUSH_32, offset);
    SHELL_DWORD(stub, name, offset);
    SHELL_BYTE(stub, CALL_REL_32, offset);
    SHELL_DWORD(stub, (DWORD)&generic_hook - (DWORD)stub - offset - 4, offset);
    SHELL_BYTE(stub, JMP_REL_32, offset);
    SHELL_DWORD(stub, (DWORD)(dll_imagebase + *patch_addr) - (DWORD)stub - offset - 4, offset);
    SHELL_BYTE(stub, 0xCC, offset);
    /*
    Sample disassembly:
    90              nop
    54              push    esp
    6884a78477      push    7784A784h
    e824fc786f      call    72075770
    e95fb8e974      jmp     777813b0
    cc              int     3
    */
#endif
    if (offset > GENHOOK_STUB_SIZE)
    {
        err("Generic hook stub too large: %d!", offset);
        return ERROR_INTERNAL_ERROR;
    }

    delta = (DWORD)((SIZE_T)stub - (SIZE_T)dll_imagebase);

#if CFG_DEBUG_HOOKS
    log("[-] Patching generic hook VA 0x%p @ EAT(%s:%s) = 0x%p (new value:0x%08X; old value: 0x%08X -> 0x%p)",
        stub, dll_name, name, patch_addr, delta, *patch_addr, dll_imagebase + *patch_addr);
#endif
    CHECK_CALL_BOOL(VirtualProtect((void*)((size_t)patch_addr & ~0xFFF), 0x1000, PAGE_READWRITE,
            &page_prot));
    *patch_addr = delta;
    CHECK_CALL_BOOL(VirtualProtect((void*)((size_t)patch_addr & ~0xFFF), 0x1000, page_prot,
            &page_prot));
done:
    return result;
}


/**
 * Install generic hook (on all exports in DLL).
 */
__declspec(dllexport)
BOOL
install_generic_hooks(
    PCHAR dll_name,
    BOOL warn_if_not_loaded
    )
{
    DWORD result = 1;
    PBYTE dll_imagebase = NULL;
    PIMAGE_DOS_HEADER dosh;
	PIMAGE_NT_HEADERS nth;
    PIMAGE_DATA_DIRECTORY pexpdd;
	PIMAGE_EXPORT_DIRECTORY expdir;
    DWORD i;
    PBYTE genhook_stubs;
    PVOID ntdll_addr = NULL;

    dll_imagebase = (PBYTE)GetModuleHandleA(dll_name);
    if (!dll_imagebase)
    {
        if (warn_if_not_loaded)
        {
            err("Attempted to install generic hook in %s, which isn't loaded (yet)!", dll_name);
        }
        return FALSE;
    }

    if ((!stricmp(dll_name, "ntdll.dll")))
    {
        return FALSE;
    }

    dosh = (PIMAGE_DOS_HEADER)dll_imagebase;
    nth = (PIMAGE_NT_HEADERS)(dll_imagebase + dosh->e_lfanew);
    pexpdd = (PIMAGE_DATA_DIRECTORY)&nth->OptionalHeader.DataDirectory[0];
    expdir = (PIMAGE_EXPORT_DIRECTORY)(dll_imagebase + pexpdd->VirtualAddress);

    if (!(pexpdd->Size && expdir->NumberOfNames))
    {
        log("[!] Module %s has no exported functions (by name)!", dll_name);
        return FALSE;
    }

    EnterCriticalSection(&GH_CRIT_SEC);

#ifdef _M_X64
    ntdll_addr = GetModuleHandleA("ntdll.dll");
#else
    UNREFERENCED_PARAMETER(ntdll_addr);
#endif

    genhook_stubs = (PBYTE)VirtualAlloc((PVOID)GENHOOK_SLOT_ADDR(ntdll_addr, ++GENHOOK_CRT_IDX),
            GENHOOK_STUB_SIZE * expdir->NumberOfNames, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
    if (!genhook_stubs)
    {
        err("Failed allocating %d bytes for generic hook stubs @ 0x%p: 0x%X!", GENHOOK_STUB_SIZE * expdir->NumberOfNames,
                GENHOOK_SLOT_ADDR(ntdll_addr, GENHOOK_CRT_IDX), GetLastError());
        goto done;
    }

    log("[*] Generic hook stubs @ 0x%p. Walking exported APIs of %s @ 0x%p...", genhook_stubs, dll_name, dll_imagebase);

    for (i = 0; i < expdir->NumberOfNames; i++)
    {
        char *name = dll_imagebase + *(DWORD*)(dll_imagebase + expdir->AddressOfNames + i * 4);
        PBYTE stub = &genhook_stubs[i * GENHOOK_STUB_SIZE];

        if (!stricmp(name, "BaseThreadInitThunk"))
        {
            // todo: find out why this one crashes if hooked
            continue;
        }

        if (!strcmp(name, "RtlEnterCriticalSection")
                || !strcmp(name, "RtlLeaveCriticalSection")
                || !strcmp(name, "RtlSetLastWin32Error")
                || !strcmp(name, "memcpy")
                || !strcmp(name, "memset")
                || !strcmp(name, "RtlAllocateHeap")
                || !strcmp(name, "_vsnprintf")
                || !strcmp(name, "wcsrchr")
                || !strcmp(name, "LdrFindEntryForAddress")
                || !strcmp(name, "GetLastError")
                || !strcmp(name, "CommandLineToArgvW")
                || !strcmp(name, "GetCommandLineW")
                || !strcmp(name, "RtlFreeHeap")
                || !strcmp(name, "RtlDeleteCriticalSection")
                || !strcmp(name, "EtwEventUnregister")
                || !strcmp(name, "EtwUnregisterTraceGuids")
                || !strcmp(name, "RtlAcquireSRWLockShared")
                || !strcmp(name, "RtlReleaseSRWLockShared")
                || !strcmp(name, "SetLastError")
                || !strcmp(name, "GetModuleHandleW")
                || !strcmp(name, "wcschr")
                || !strcmp(name, "_wcsicmp")
                || !strcmp(name, "SE_GetProcAddressForCaller")
            )
        {
            continue;
        }

        result = write_generic_hook(dll_imagebase, i, &stub);
        if (result != ERROR_SUCCESS)
        {
            if ((result != ERROR_WRONG_TARGET_NAME) && (result != ERROR_ALREADY_EXISTS))
            {
                err("Failed installing generic hook for %s: 0x%08X!", name, result);
                goto done;
            }
        }
    }

    result = ERROR_SUCCESS;
done:

    LeaveCriticalSection(&GH_CRIT_SEC);

    return result == ERROR_SUCCESS;
}


#if CFG_HOOK_GETPROC_EXPORTS
/**
 * Installs a single (dynamic) generic hook.
 *
 * @param hook_addr (optional, out) Address of hook code
 * @return GetLastError()-like error code
 */
__declspec(dllexport)
DWORD
install_dynamic_generic_hook(
    PBYTE dll_imagebase,
    char* export_name,
    PVOID* hook_addr
    )
{
    DWORD result = ERROR_SUCCESS;
    PVOID ntdll_addr = NULL;
    DWORD index = (DWORD)-1;
    PBYTE stub = NULL;

#ifdef _M_X64
    ntdll_addr = GetModuleHandleA("ntdll.dll");
#else
    UNREFERENCED_PARAMETER(ntdll_addr);
#endif

    if (find_export(dll_imagebase, export_name, NULL, &index, NULL) != ERROR_SUCCESS)
    {
        err("Failed finding export %s!", export_name);
        return ERROR_NOT_FOUND;
    }

    // ignore spam APIs
    if (!strcmp(export_name, "TlsGetValue")
            || !strcmp(export_name, "TlsSetValue")
            || !strcmp(export_name, "FlsGetValue")
            || !strcmp(export_name, "FlsSetValue")
            || !strcmp(export_name, "GetLastError")
            || !strcmp(export_name, "SetLastError")
            || !strcmp(export_name, "InitializeCriticalSectionEx")
            || !strcmp(export_name, "GetCurrentThreadId")
            || !strcmp(export_name, "GetCurrentProcess")
        )
    {
        return ERROR_WRONG_TARGET_NAME;
    }

    result = write_generic_hook(dll_imagebase, index, &stub);
    if (result == ERROR_SUCCESS)
    {
        if (hook_addr)
        {
            *hook_addr = (PVOID)stub;
        }
    }
    else if ((result != ERROR_ALREADY_EXISTS) && (result != ERROR_WRONG_TARGET_NAME))
    {
        err("Failed installing generic hook for %s!", export_name);
    }

    return result;
}
#endif


/**
 * Increments the hook depth.
 */
__declspec(dllexport)
void
hook_enter(
    void
    )
{
    TlsSetValue(TLSID_INHOOK, (PVOID)((DWORD)TlsGetValue(TLSID_INHOOK) + 1));
}


/**
 * Decrements the hook depth.
 */
__declspec(dllexport)
void
hook_leave(
    void
    )
{
    if (!(DWORD)TlsGetValue(TLSID_INHOOK))
    {
        err("Hook depth is 0 set on hook_leave()!");
        return;
    }
    TlsSetValue(TLSID_INHOOK, (PVOID)((DWORD)TlsGetValue(TLSID_INHOOK) - 1));
}


/**
 * Returns the hook depth.
 */
__declspec(dllexport)
int
hook_depth(
    void
    )
{
    return (DWORD)TlsGetValue(TLSID_INHOOK);
}


/**
 * LdrLoadHook notification callback.
 */
__declspec(dllexport)
VOID
CALLBACK
callback_LdrLoadDll(
    ULONG NotificationReason,
    PLDR_DLL_NOTIFICATION_DATA NotificationData,
    PVOID Context
    )
{
    UNREFERENCED_PARAMETER(Context);
    char buffer[MAX_PATH];
    USHORT i;

    if (NotificationReason != LDR_DLL_NOTIFICATION_REASON_LOADED)
    {
        return;
    }

    // extract DLL base name as PCHAR
    for (i = 0; (i < sizeof(buffer) - 1) && (i < NotificationData->Loaded.BaseDllName->Length >> 1); i++)
    {
        buffer[i] = (char)NotificationData->Loaded.BaseDllName->Buffer[i];
    }
    buffer[i] = 0;
#if CFG_DEBUG_HOOKS
    log("[*] <LdrLoadDll-callback> DLL loaded: %s", buffer);
#endif

    install_hooks(buffer, TRUE);
#if CFG_HOOK_ALL_EXPORTS
    install_generic_hooks(buffer, FALSE);
#endif
}


/**
 * Register LdrLoadHook notification.
 */
__declspec(dllexport)
BOOL
register_hook_LdrLoadDll(
    void
    )
{
    NTSTATUS result = 0xC00000E5L; // STATUS_INTERNAL_ERROR

    if (LdrRegisterDllNotification)
    {
        CHECK_CALL_STATUS(LdrRegisterDllNotification(0, callback_LdrLoadDll, NULL, &LLD_COOKIE));
    }

done:;
    SetLastError(result);
    return NT_SUCCESS(result);
}


BOOL
init_hook_gates(
    void
    )
{
    DWORD result = 0;
#ifdef _M_X64
    PVOID ntdll = GetModuleHandleA("ntdll.dll");
    PVOID hook_gate_addr = (PVOID)HOOK_GATE_BASE_ADDR(ntdll);
#endif
    DWORD i;

#ifdef _M_X64
    log("[*] Settting up hook gates @ 0x%p...", hook_gate_addr);
    CHECK_CALL_BOOL(HOOK_GATES = VirtualAlloc(hook_gate_addr, HOOK_GATE_SIZE * HookCount,
            MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE));

    for (i = 0; i < HookCount; i++)
    {
        HOOK* hook = &Hooks[i];
        PBYTE hook_gate = HOOK_GATE_ADDR(i);

        hook->hook_gate_addr = hook_gate;
        *(WORD*)hook_gate = 0xB848;
        *(ULONGLONG*)(hook_gate + 2) = (ULONGLONG)hook->hook_addr;
        *(WORD*)(hook_gate + 10) = 0xE0FF;
    }

done:
#else
    log("[*] No hook gates needed, pointing gates to actual hooks...");

    for (i = 0; i < HookCount; i++)
    {
        HOOK* hook = &Hooks[i];

        hook->hook_gate_addr = hook->hook_addr;
    }
#endif

    return result == ERROR_SUCCESS;
}


/**
 * Initialize the hooking library.
 */
__declspec(dllexport)
BOOL
init_libhook(
    void
    )
{
    DWORD result = ERROR_SUCCESS;
    SYSTEM_INFO si;
    PCHAR *dll = NULL;
    PVOID ntdll = GetModuleHandleA("ntdll.dll");

    GetNativeSystemInfo(&si);
    TLSID_INHOOK = TlsAlloc();

    CHECK_CALL_BOOL(LdrRegisterDllNotification = (FUN_LdrRegisterDllNotification)GetProcAddress(ntdll,
            "LdrRegisterDllNotification"));
    CHECK_CALL_BOOL(register_hook_LdrLoadDll());
    CHECK_CALL_BOOL(init_hook_gates());

    InitializeCriticalSection(&GH_CRIT_SEC);

#if CFG_HOOK_GETPROC_EXPORTS
    DYN_GEN_HOOK_SLOT = (PBYTE)VirtualAlloc((PVOID)GENHOOK_SLOT_ADDR(ntdll, DYN_GEN_HOOK_INDEX),
            GENHOOK_SLOT_SIZE, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
    if (!DYN_GEN_HOOK_SLOT)
    {
        err("Failed allocating %d bytes for generic hook stubs @ 0x%p: 0x%X!", GENHOOK_SLOT_SIZE,
                GENHOOK_SLOT_ADDR(ntdll, GENHOOK_CRT_IDX), GetLastError());
        result = ERROR_NOT_ENOUGH_MEMORY;
    }
#endif

    if (HOOKID_COUNT != HookCount)
    {
        log("[!] Mismatch: hook IDs = %d; hook structures = %d", HOOKID_COUNT, HookCount);
    }
    for (dll = HookedDlls; *dll; dll++)
    {
        install_hooks(*dll, FALSE);
#if CFG_HOOK_ALL_EXPORTS
        install_generic_hooks(*dll, FALSE);
#endif
    }
    // todo: check env vars, current dir, etc.
done:
    return result == ERROR_SUCCESS;
}
