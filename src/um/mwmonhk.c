/*
 * mwmon/dll: Injectable DLL component of mwmon.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#include "mwmonhk.h"


__declspec(dllexport)
void
init_mwmon_dll(
    void
    )
{
    WCHAR buffer[1024];
    DWORD result;

    init_ntdll_wrappers();
    init_miscvars();

#ifdef _M_X64
    //__debugbreak();
#endif

    if (!(GetFileAttributesW(OUT_PATH) & FILE_ATTRIBUTE_DIRECTORY))
    {
        CreateDirectoryW(OUT_PATH, NULL);
    }
    ntdll_swprintf_s(buffer, sizeof(buffer) / 2, L"%s\\%s-" MWM_WBITS "-P%d-api.log", OUT_PATH, WPROCNAME, GetCurrentProcessId());
    init_logging(buffer);
    log("[*] Process [%s] loaded mwmonhk" MWM_BITS ".dll " MWMVER " @ 0x%p", PROCNAME, HOOKDLL_ADDR);
    log("[*] Command line: %s", GetCommandLineA());

#if CFG_SUSPEND_MS_AUTORUN_BLOATWARE
    if (
            (!stricmp(PROCNAME, "windowsupdatebox.exe"))
            || (!stricmp(PROCNAME, "apphostregistrationverifier.exe"))
            || (!stricmp(PROCNAME, "applicationframehost.exe"))
            || (!stricmp(PROCNAME, "devicecensus.exe"))
            || (!stricmp(PROCNAME, "dismhost.exe"))
            || (!stricmp(PROCNAME, "mpcmdrun.exe"))
            || (!stricmp(PROCNAME, "msmpeng.exe"))
            || (!stricmp(PROCNAME, "provtool.exe"))
            || (!stricmp(PROCNAME, "sdiagnhost.exe"))
            || (!stricmp(PROCNAME, "searchfilterhost.exe"))
            || (!stricmp(PROCNAME, "searchprotocolhost.exe"))
            || (!stricmp(PROCNAME, "SecurityHealthService.exe"))
            || (!stricmp(PROCNAME, "slui.exe"))
            || (!stricmp(PROCNAME, "smartscreen.exe"))
            || (!stricmp(PROCNAME, "speechmodeldownload.exe"))
            || (!stricmp(PROCNAME, "SppExtComObj.exe"))
            || (!stricmp(PROCNAME, "tiworker.exe"))
            || (!stricmp(PROCNAME, "trustedinstaller.exe"))
            || (!stricmp(PROCNAME, "usoclient.exe"))
            || (!stricmp(PROCNAME, "waasmedic.exe"))
            || (!stricmp(PROCNAME, "wuauclt.exe"))
        )
    {
        log("[*] Sleeping forever...");
        stop_logging();
        for (;;)
        {
            Sleep(100000);
        }
    }
#endif

    CHECK_CALL_BOOL(init_libhook());
done:;
}


BOOL
WINAPI
DllMain(
    HINSTANCE hinstDLL,
    DWORD fdwReason,
    LPVOID lpvReserved
    )
{
    UNREFERENCED_PARAMETER(hinstDLL);
    UNREFERENCED_PARAMETER(lpvReserved);

    switch (fdwReason)
    {
        case DLL_PROCESS_ATTACH:
            init_mwmon_dll();
            break;
        case DLL_PROCESS_DETACH:
            log("[*] Process %d detaching from hook DLL...", GetCurrentProcessId());
            break;
    }

    return TRUE;
}
