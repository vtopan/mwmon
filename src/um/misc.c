/*
 * mwmon/misc: misc APIs.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#include "misc.h"


WCHAR WPROCNAME[260];
CHAR PROCNAME[260];
PBYTE HOOKDLL_ADDR = NULL;
size_t HOOKDLL_SIZE = 0;

WCHAR _WS_INVALID_PTR[] = L"INVALID-PTR";
UNICODE_STRING _UCS_INVALID_PTR_STRUCT = {sizeof(_WS_INVALID_PTR), sizeof(_WS_INVALID_PTR), _WS_INVALID_PTR};
PUNICODE_STRING _UCS_INVALID_PTR = &_UCS_INVALID_PTR_STRUCT;
WCHAR _WS_NULL[] = L"NULL";
UNICODE_STRING _UCS_NULL_STRUCT = {sizeof(_WS_NULL), sizeof(_WS_NULL), _WS_NULL};
PUNICODE_STRING _UCS_NULL = &_UCS_NULL_STRUCT;


/**
 * Escapes the binary data to make it printable.
 *
 * The `out_buffer` is expected to be three times as large as the `in_buffer`.
 */
__declspec(dllexport)
int
escape_binary(
    char* in_buffer,
    size_t size,
    char* out_buffer
    )
{
    size_t i, pos = 0;

    for (i = 0; i < size; i++)
    {
        char c = in_buffer[i];

        if (0x20 <= c && c < 0x80)
        {
            out_buffer[pos++] = c;
        }
        else if (c == '\t')
        {
            out_buffer[pos++] = '\\';
            out_buffer[pos++] = 't';
        }
        else if (c == '\r')
        {
            out_buffer[pos++] = '\\';
            out_buffer[pos++] = 'r';
        }
        else if (c == '\n')
        {
            out_buffer[pos++] = '\\';
            out_buffer[pos++] = 'n';
        }
        else
        {
            ntdll_snprintf(out_buffer + pos, 4, "%02hhX", c);
            pos += 2;
        }
    }
    out_buffer[pos++] = 0;

    return (int)pos;
}


/**
 * inet_ntop() replacement.
 */
PCSTR
_inet_ntop(
    int Family,
    const void* pAddr,
    char* pStringBuf,
    size_t StringBufSize
    )
{
    int i;

    if ((StringBufSize < 16) || (Family == 23 && StringBufSize < 40))
    {
        SetLastError(ERROR_INVALID_PARAMETER);
    }
    else if (Family == 2)
    {
        PBYTE addr = (PBYTE)pAddr;
        for (i = 0; i < 4; i++)
        {
            ntdll_snprintf(pStringBuf, StringBufSize, "%d", (BYTE)addr[i]);
            pStringBuf += strlen(pStringBuf);
        }
    }
    else if (Family == 23) // AF_INET6
    {
        PWORD addr = (PWORD)pAddr;
        for (i = 0; i < 8; i++)
        {
            ntdll_snprintf(pStringBuf, StringBufSize, "%04hx:", (WORD)addr[i]);
            if (i == 7)
            {
                pStringBuf[4] = 0;
            }
            pStringBuf += 5;
        }
    }
    else
    {
        ntdll_snprintf(pStringBuf, StringBufSize, "ERR:%d!", Family);
    }
    return pStringBuf;
}


/**
 * Save binary buffer (for API parameters which can't be logged as text).
 */
__declspec(dllexport)
void
save_binary_buffer(
    char* api,
    char* tag_string,
    int tag_value,
    BYTE* buffer,
    int buffer_size
    )
{
    WCHAR out_folder[MAX_PATH];

    ntdll_swprintf_s(out_folder, sizeof(out_folder) / 2, L"%s\\%s-" MWM_WBITS L"-P%d-%S", OUT_PATH,
            WPROCNAME, GetCurrentProcessId(), api);

    if (GetFileAttributesW(out_folder) == INVALID_FILE_ATTRIBUTES)
    {
        if (!CreateDirectoryW(out_folder, NULL))
        {
            log("Failed creating binary buffer folder [%s]: %d!", out_folder, GetLastError());
        }
    }

    if (GetFileAttributesW(out_folder) & FILE_ATTRIBUTE_DIRECTORY)
    {
        HANDLE fh;
        WCHAR filename[MAX_PATH];
        char tag[64];
        DWORD i;

        if ((!tag_string) && tag_value != MWM_NO_TAG)
        {
            ntdll_snprintf(tag, sizeof(tag), "%d", tag_value);
            tag_string = tag;
        }
        if (!tag_string)
        {
            for (i = 0; i < 0x1000; i++)
            {
                ntdll_swprintf_s(filename, sizeof(filename) / 2, L"\\??\\%s\\%s-" MWM_WBITS L"-P%d-%S-%04d.raw", out_folder,
                        WPROCNAME, GetCurrentProcessId(), api, i);
                if (GetFileAttributesW(filename) == INVALID_FILE_ATTRIBUTES)
                {
                    break;
                }
            }
        }
        else
        {
            ntdll_swprintf_s(filename, sizeof(filename) / 2, L"\\??\\%s\\%s-" MWM_WBITS L"-P%d-%S-%S.raw", out_folder,
                    WPROCNAME, GetCurrentProcessId(), api, tag_string);
        }
        log("[-] Writing to [%S]...", filename);
        if (fh = CreateFileW(filename, FILE_APPEND_DATA, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL))
        {
            DWORD written;
            SetFilePointer(fh, 0, NULL, FILE_END);
            WriteFile(fh, buffer, buffer_size, &written, NULL);
            CloseHandle(fh);
        }
    }
}


#ifdef NOCRT

#pragma function(memset)
void*
memset(
    void* ptr,
    int value,
    size_t num
    )
{
    size_t i;

    for (i = 0; i < num; i++)
    {
        ((char*)ptr)[i] = (char)value;
    }

    return ptr;
}


#pragma function(memcpy)
void*
memcpy(
    void* destination,
    const void* source,
    size_t num
    )
{
    size_t i;

    for (i = 0; i < num; i++)
    {
        ((char*)destination)[i] =  ((char*)source)[i];
    }

    return destination;
}


#pragma function(strcmp)
int
strcmp(
    const char *string1,
    const char *string2
    )
{
    while (*string1 || *string2)
    {
        if ((*string1) < (*string2))
        {
            return -1;
        }
        else if ((*string1++) > (*string2++))
        {
            return 1;
        }
    }

    return 0;
}


int
stricmp(
    const char *string1,
    const char *string2
    )
{
    while (*string1 || *string2)
    {
        if ((*string1 | 0x20) < (*string2 | 0x20))
        {
            return -1;
        }
        else if ((*string1++ | 0x20) > (*string2++ | 0x20))
        {
            return 1;
        }
    }

    return 0;
}


int
strnicmp(
    const char *string1,
    const char *string2,
    size_t count
    )
{
    size_t i = 0;

    while ((*string1 || *string2) && (i++ < count))
    {
        if ((*string1 | 0x20) < (*string2 | 0x20))
        {
            return -1;
        }
        else if ((*string1++ | 0x20) > (*string2++ | 0x20))
        {
            return 1;
        }
    }

    return 0;
}


#pragma function(strlen)
size_t
strlen(
    const char *string
    )
{
    size_t i;

    for (i = 0; string[i]; i++);

    return i;
}
#endif


/**
 * Initialize the global variables PROCNAME / WPROCNAME and HOOKDLL_ADDR.
 */
__declspec(dllexport)
void
init_miscvars(
    void
    )
{
    PWCHAR fullname = NULL;
    PPEB peb = get_peb();
    SHORT i, j;
    PIMAGE_NT_HEADERS nth;

    // extract the executable filename without the path, lowercase
    fullname = peb->ProcessParameters->ImagePathName.Buffer;
    for (i = j = 0; fullname[i] && (fullname[i] != L'|') && (i < ((SHORT)peb->ProcessParameters->ImagePathName.Length / 2)); i++, j++)
    {
        WPROCNAME[j] = fullname[i] | 0x20;
        PROCNAME[j] = (CHAR)fullname[i] | 0x20;
        if (fullname[i] == L'\\')
        {
            j = -1;
        }
    }
    WPROCNAME[j] = PROCNAME[j] = 0;

    GetModuleHandleExA(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS, (LPCSTR)&init_miscvars, (HMODULE*)&HOOKDLL_ADDR);
    nth = (PIMAGE_NT_HEADERS)(HOOKDLL_ADDR + ((PIMAGE_DOS_HEADER)HOOKDLL_ADDR)->e_lfanew);
    HOOKDLL_SIZE = nth->OptionalHeader.SizeOfImage;
}