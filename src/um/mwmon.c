/*
 * mwmon: Win32 live malware analysis tool.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#include "mwmon.h"


int
main(
    int argc,
    char** argv
    )
{
    int result = 0;
    PWCHAR args;
    WCHAR mw_cmdline[4096], sep;
    WCHAR log_filename[1024];
    HANDLE target_process = NULL, target_thread = NULL, hook_thread = NULL;
    DWORD pid = 0;
    BOOL injected = FALSE;

    UNREFERENCED_PARAMETER(argv);

    init_ntdll_wrappers();
    init_miscvars();

    if (PATH_EXISTS(OUT_PATH))
    {
        CreateDirectoryW(OUT_PATH, NULL);
    }
    wsprintfW(log_filename, OUT_PATH L"\\mwmon-%d.log", GetCurrentProcessId());
    init_logging(log_filename);

    if (argc < 2)
    {
        err("mwmon " MWMVER "\nUsage:\nmwmon <malware_cmdline>");
        result = -1;
        goto done;
    }

    args = GetCommandLineW();
    sep = (*args == L'"') ? L'"' : L' ';
    while (*(++args) && (*args != sep));
    args++;
    while (*args && (*args == L' ')) args++;
    wcscpy(mw_cmdline, args);

    if (args[0] == L'-' && args[1] == L'p')
    {
        // inject in PID
        args += 2;
        while (*args == L' ') args++;
        pid = _wtoi(args);
        log("[*] Injecting into PID [%d]...", pid);
        CHECK_CALL_BOOL(target_process = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid));
        injected = TRUE;
    }
    else
    {
        STARTUPINFOW si = {0};
        PROCESS_INFORMATION pi = {0};

        si.cb = sizeof(si);
        log("[*] Preparing command line [%S]...", mw_cmdline);
        CHECK_CALL_BOOL(CreateProcessW(NULL, mw_cmdline, NULL, NULL, FALSE, CREATE_SUSPENDED | CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi));
        target_process = pi.hProcess;
        target_thread = pi.hThread;
        pid = pi.dwProcessId;
        log("[*] Spawned command line [%S] => PID [%d]", mw_cmdline, pid);
    }

    CHECK_CALL_BOOL(hook_thread = inject_mwmon_dll(target_process));

    log("[*] Resuming %s thread...", injected ? "hook" : "main");
    CHECK_CALL_BOOL(ResumeThread(injected ? hook_thread : target_thread) != (DWORD)-1);

done:
    return result;
}