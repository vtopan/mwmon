/*
 * mwmon/logging: output + logging support.
 *
 * Author: Vlad Topan (vtopan/gmail).
 */

#include "logging.h"


HANDLE OUT_ERR = INVALID_HANDLE_VALUE;
HANDLE OUT_LOG = INVALID_HANDLE_VALUE;
HANDLE LOG_FILE = INVALID_HANDLE_VALUE;
int DEBUG = CFG_DEBUG;
int TIMESTAMP = 0;


BOOL
init_logging(
    WCHAR* file_name
)
{
    DWORD result = ERROR_SUCCESS;

    OUT_ERR = GetStdHandle(STD_ERROR_HANDLE);
    OUT_LOG = GetStdHandle(STD_OUTPUT_HANDLE);

    if (file_name)
    {
        CHECK_CALL_BOOL(log_to_file(file_name));
    }

done:
    return result == ERROR_SUCCESS;
}


void
out(
    BOOL is_err,
    const char* format,
    ...
)
{
    char message[2048];
    va_list args;
    DWORD msglen; //, written;
    HANDLE out_file;
    IO_STATUS_BLOCK isb;

    va_start(args, format);
    ntdll_vsprintf_s(message, sizeof(message), format, args);
    msglen = (DWORD)strlen(message);
    out_file = is_err ? OUT_ERR : OUT_LOG;
    if (out_file != INVALID_HANDLE_VALUE)
    {
        NtWriteFile(out_file, NULL, NULL, NULL, &isb, message, msglen, 0, NULL);
    }
    if (LOG_FILE != INVALID_HANDLE_VALUE)
    {
        NtWriteFile(LOG_FILE, NULL, NULL, NULL, &isb, message, msglen, 0, NULL);
    }
    va_end(args);
}


BOOL
log_to_file(
    WCHAR* file_name
)
{
    DWORD result = ERROR_SUCCESS;

    CHECK_CALL_HANDLE(LOG_FILE = CreateFileW(file_name, GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_ALWAYS,
            FILE_ATTRIBUTE_NORMAL, NULL));

done:
    return result == ERROR_SUCCESS;
}


void
stop_logging(
    void
)
{
    if (LOG_FILE != INVALID_HANDLE_VALUE)
    {
        CloseHandle(LOG_FILE);
    }
}