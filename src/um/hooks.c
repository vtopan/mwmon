/*
 * mwmon/hooks: API hooks.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#include "hooks.h"


HOOKFUN(NTSTATUS)
hook_LdrLoadDll(
    PULONG DllCharacteristics OPTIONAL,
    PWSTR SearchPath OPTIONAL,
    PUNICODE_STRING DllName,
    PVOID *BaseAddress
    )
{
    NTSTATUS result = 0;
    //WCHAR wdllname[MAX_PATH] = {0};
    //CHAR dllname[MAX_PATH] = {0};

    HOOK_ENTER(LdrLoadDll);

    result = (BOOL)((fun4arg)(Hooks[HOOKID_LdrLoadDll].orig_addr))(DllCharacteristics, SearchPath, DllName, BaseAddress);
    //memcpy(wdllname, DllName->Buffer, min(DllName->Length, sizeof(wdllname) - 2));
    log("APICALL: LdrLoadDll(0x%p, \"%S\", \"%wZ\", 0x%p) => 0x%p", DllCharacteristics, PRINT_WSTR(SearchPath), PRINT_USTR(DllName), //wdllname
            BaseAddress ? *BaseAddress : 0, result);

    /*
    if (NT_SUCCESS(result) && WideCharToMultiByte(CP_ACP, 0, wdllname, (int)wcslen(wdllname), dllname, (int)sizeof(dllname), NULL, NULL))
    {
        install_hooks(dllname, TRUE);
    }
    */

    HOOK_LEAVE();

    return result;
}


HOOKFUN(NTSTATUS)
hook_NtQueryValueKey(
    HANDLE KeyHandle,
    PUNICODE_STRING ValueName,
    DWORD KeyValueInformationClass,
    PVOID KeyValueInformation,
    ULONG Length,
    PULONG ResultLength
    )
{
    NTSTATUS result = 0;

    HOOK_ENTER(NtQueryValueKey);

    result = (NTSTATUS)((fun6arg)(Hooks[HOOKID_NtQueryValueKey].orig_addr))(KeyHandle,
            ValueName, (PVOID)KeyValueInformationClass, KeyValueInformation, (PVOID)Length,
            ResultLength);
    log("APICALL: NtQueryValueKey(0x%p, \"%wZ\", 0x%p, 0x%p, 0x%p, 0x%p) => 0x%p", KeyHandle,
            ValueName, KeyValueInformationClass, KeyValueInformation, Length, ResultLength,
            result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(NTSTATUS)
hook_NtOpenKey(
    PHANDLE KeyHandle,
    ACCESS_MASK DesiredAccess,
    POBJECT_ATTRIBUTES ObjectAttributes
    )
{
    NTSTATUS result = 0;

    HOOK_ENTER(NtOpenKey);

    result = (NTSTATUS)((fun3arg)(Hooks[HOOKID_NtOpenKey].orig_addr))(KeyHandle,
            (PVOID)DesiredAccess, ObjectAttributes);
    log("APICALL: NtOpenKey(0x%p, 0x%p, 0x%p <%wZ>) => 0x%p", KeyHandle, DesiredAccess,
            ObjectAttributes, ObjectAttributes->ObjectName, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(void)
hook_RtlInitUnicodeString(
    PUNICODE_STRING DestinationString,
    LPWSTR SourceString
    )
{
    HOOK_ENTER(RtlInitUnicodeString);

    ((fun2arg)(Hooks[HOOKID_RtlInitUnicodeString].orig_addr))(DestinationString, SourceString);
    log("APICALL: RtlInitUnicodeString(0x%p, \"%S\")", DestinationString, PRINT_WSTR(SourceString));

    HOOK_LEAVE();
}


HOOKFUN(DWORD)
hook_LdrGetProcedureAddress(
    HMODULE ModuleHandle,
    PANSI_STRING FunctionName,
    WORD Ordinal,
    PVOID *FunctionAddress
    )
{
    DWORD result = 0;

    HOOK_ENTER(LdrGetProcedureAddress);

    result = (DWORD)((fun4arg)(Hooks[HOOKID_LdrGetProcedureAddress].orig_addr))((PVOID)ModuleHandle,
            FunctionName, (PVOID)Ordinal, FunctionAddress);
    log("APICALL: LdrGetProcedureAddress(0x%p, \"%s\", 0x%p, 0x%p) => 0x%p", ModuleHandle,
            FunctionName ? PRINT_STR(FunctionName->Buffer) : "NULL", Ordinal, FunctionAddress, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(DWORD)
hook_LdrGetProcedureAddressForCaller(
    HMODULE ModuleHandle,
    PANSI_STRING FunctionName,
    WORD Ordinal,
    PVOID *FunctionAddress,
    BOOL Value,
    PVOID *CallbackAddress
    )
{
    DWORD result = 0;

    HOOK_ENTER(LdrGetProcedureAddressForCaller);

    result = (DWORD)((fun6arg)(Hooks[HOOKID_LdrGetProcedureAddressForCaller].orig_addr))((PVOID)ModuleHandle,
            FunctionName, (PVOID)Ordinal, FunctionAddress, (PVOID)Value, CallbackAddress);
    log("APICALL: LdrGetProcedureAddressForCaller(0x%p, \"%s\", 0x%p, 0x%p, 0x%p, 0x%p) => 0x%p", ModuleHandle,
            FunctionName ? PRINT_STR(FunctionName->Buffer) : "NULL", Ordinal, FunctionAddress, Value, CallbackAddress,
            result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(NTSTATUS)
hook_NtOpenFile(
    PHANDLE FileHandle,
    ACCESS_MASK DesiredAccess,
    POBJECT_ATTRIBUTES ObjectAttributes,
    PIO_STATUS_BLOCK IoStatusBlock,
    ULONG ShareAccess,
    ULONG OpenOptions
    )
{
    NTSTATUS result = 0;

    HOOK_ENTER(NtOpenFile);

    result = (NTSTATUS)((fun6arg)(Hooks[HOOKID_NtOpenFile].orig_addr))(FileHandle, (PVOID)DesiredAccess, ObjectAttributes,
            IoStatusBlock, (PVOID)ShareAccess, (PVOID)OpenOptions);
    log("APICALL: NtOpenFile(0x%p, 0x%p, 0x%p <%wZ>, 0x%p, 0x%p, 0x%p) => 0x%p", FileHandle, DesiredAccess, ObjectAttributes,
            ObjectAttributes ? PRINT_USTR(ObjectAttributes->ObjectName) : _UCS_NULL,
            IoStatusBlock, ShareAccess, OpenOptions, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(void)
hook_Sleep(
    DWORD dwMilliseconds
    )
{
    HOOK_ENTER(Sleep);

    (void)((fun1arg)(Hooks[HOOKID_Sleep].orig_addr))((PVOID)dwMilliseconds);
    if (dwMilliseconds > 1000)
    {
        log("APICALL: Sleep(0x%X)", dwMilliseconds);
    }

    HOOK_LEAVE();
}


HOOKFUN(BOOL)
hook_DeviceIoControl(
    HANDLE hDevice,
    DWORD dwIoControlCode,
    PVOID lpInBuffer,
    DWORD nInBufferSize,
    PVOID lpOutBuffer,
    DWORD nOutBufferSize,
    LPDWORD lpBytesReturned,
    LPOVERLAPPED lpOverlapped
    )
{
    BOOL result = 0;

    HOOK_ENTER(DeviceIoControl);

    result = (BOOL)((fun8arg)(Hooks[HOOKID_DeviceIoControl].orig_addr))(hDevice, (PVOID)dwIoControlCode, lpInBuffer,
            (PVOID)nInBufferSize, lpOutBuffer, (PVOID)nOutBufferSize, lpBytesReturned, lpOverlapped);
    log("APICALL: DeviceIoControl(0x%p, 0x%X, 0x%p, 0x%X, 0x%p, 0x%X, 0x%p, 0x%p) => 0x%p", hDevice, dwIoControlCode,
            lpInBuffer, nInBufferSize, lpOutBuffer, nOutBufferSize, lpBytesReturned, lpOverlapped, result);

    HOOK_LEAVE();

    return result;
}

/*
HOOKFUN(BOOL)
hook_CloseHandle(
    HANDLE hObject
    )
{
    BOOL result = 0;

    HOOK_ENTER(CloseHandle);

    result = (BOOL)((fun1arg)(Hooks[HOOKID_CloseHandle].orig_addr))(hObject);
    log("APICALL: CloseHandle(0x%p) => %d", hObject, result);

    HOOK_LEAVE();

    return result;
}
*/

HOOKFUN(HANDLE)
hook_GetStdHandle(
    DWORD nStdHandle
    )
{
    HANDLE result = NULL;

    HOOK_ENTER(GetStdHandle);

    result = (HANDLE)((fun1arg)(Hooks[HOOKID_GetStdHandle].orig_addr))((void*)nStdHandle);
    log("APICALL: GetStdHandle(0x%08X) => 0x%p", nStdHandle, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(size_t)
hook_FlsAlloc(
    void* lpCallback
    )
{
    size_t result = 0;

    HOOK_ENTER(FlsAlloc);

    log("APICALL: FlsAlloc(0x%p) (PRE)", lpCallback);
    result = (size_t)((fun1arg)(Hooks[HOOKID_FlsAlloc].orig_addr))(lpCallback);
    log("APICALL: FlsAlloc(0x%p) => 0x%p", lpCallback, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(PVOID)
hook_GetProcAddress(
    HMODULE hModule,
    LPSTR lpProcName
    )
{
    PVOID result = NULL;
    char buf_procname[512], buf_dllname[256];

    HOOK_ENTER(GetProcAddress);

    result = (PVOID)((fun2arg)(Hooks[HOOKID_GetProcAddress].orig_addr))(hModule, lpProcName);
    if ((long long)lpProcName < 0x10000L)
    {
        ntdll_snprintf(buf_procname, sizeof(buf_procname), "ordinal:%d", (int)lpProcName);
        lpProcName = buf_procname;
    }
    GetModuleFileNameA(hModule, buf_dllname, sizeof(buf_dllname));

#if CFG_HOOK_GETPROC_EXPORTS
    {
        PVOID hook = NULL;
        DWORD hresult = 1;

        if (result && ((long long)lpProcName > 0x10000L))
        {
            hresult = install_dynamic_generic_hook((PBYTE)hModule, lpProcName, &hook);

            if (hresult == ERROR_SUCCESS)
            {
                result = hook;
            }
            else if ((hresult != ERROR_ALREADY_EXISTS) && (hresult != ERROR_WRONG_TARGET_NAME))
            {
                err("Failed installing dynamic generic hook on GPA'd %s: 0x%p!", lpProcName, hresult);
            }
        }

        log("APICALL: GetProcAddress(0x%p <%s>, \"%s\") => 0x%p (dyn-hook: %s/0x%p)", hModule, buf_dllname,
                PRINT_STR(lpProcName), result, hresult == ERROR_SUCCESS ? "yes" : "no", hook);
    }
#else
    log("APICALL: GetProcAddress(0x%p <%s>, \"%s\") => 0x%p", hModule, buf_dllname,
            PRINT_STR(lpProcName), result);
#endif


    HOOK_LEAVE();

    return result;
}


HOOKFUN(HMODULE)
hook_LoadLibraryA(
    LPSTR lpLibFileName
    )
{
    HANDLE result = NULL;

    HOOK_ENTER(LoadLibraryA);

    result = (HANDLE)((fun1arg)(Hooks[HOOKID_LoadLibraryA].orig_addr))(lpLibFileName);
    log("APICALL: LoadLibraryA(\"%s\") => 0x%p", PRINT_STR(lpLibFileName), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HMODULE)
hook_LoadLibraryW(
    LPWSTR lpLibFileName
    )
{
    HANDLE result = NULL;

    HOOK_ENTER(LoadLibraryW);

    result = (HANDLE)((fun1arg)(Hooks[HOOKID_LoadLibraryW].orig_addr))(lpLibFileName);
    log("APICALL: LoadLibraryW(\"%S\") => 0x%p", PRINT_WSTR(lpLibFileName), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HMODULE)
hook_LoadLibraryExA(
    LPSTR lpLibFileName,
    HANDLE hFile,
    DWORD dwFlags
    )
{
    HANDLE result = NULL;

    HOOK_ENTER(LoadLibraryExA);

    result = (HANDLE)((fun3arg)(Hooks[HOOKID_LoadLibraryExA].orig_addr))(lpLibFileName, hFile, (PVOID)dwFlags);
    log("APICALL: LoadLibraryExA(\"%s\", 0x%p, 0x%X) => 0x%p", PRINT_STR(lpLibFileName), hFile, dwFlags, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HMODULE)
hook_LoadLibraryExW(
    LPWSTR lpLibFileName,
    HANDLE hFile,
    DWORD dwFlags
    )
{
    HANDLE result = NULL;

    HOOK_ENTER(LoadLibraryExW);

    result = (HANDLE)((fun3arg)(Hooks[HOOKID_LoadLibraryExW].orig_addr))(lpLibFileName, hFile, (PVOID)dwFlags);
    log("APICALL: LoadLibraryExW(\"%S\", 0x%p, 0x%X) => 0x%p", PRINT_WSTR(lpLibFileName), hFile, dwFlags, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(int)
hook_connect(
    SOCKET s,
    struct sockaddr* name,
    int namelen
    )
{
    int result = 0;
    char addr[256];

    HOOK_ENTER(connect);

    _inet_ntop(name->sa_family, name, addr, sizeof(addr));
    result = (int)((fun3arg)(Hooks[HOOKID_connect].orig_addr))((void*)s, name, (void*)namelen);
    log("APICALL: connect(%d, 0x%p:\"%s\", %d) => 0x%p", s, name, addr, namelen, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(int)
hook_send(
    SOCKET s,
    char* buf,
    int len,
    int flags
    )
{
    int result = 0;
    char* hex_buf = {0};

    HOOK_ENTER(send);

    if (len)
    {
        save_binary_buffer("send", NULL, (int)s, buf, len);
    }

    hex_buf = LocalAlloc(LPTR, 3 * (len + 1));
    if (hex_buf)
    {
        escape_binary(buf, len, hex_buf);
    }
    else
    {
        hex_buf = "[Failed allocating buffer!]";
    }
    result = (int)((fun4arg)(Hooks[HOOKID_send].orig_addr))((void*)s, buf, (void*)len, (void*)flags);
    log("APICALL: send(%d, 0x%p:\"%s\", %d, 0x%08X) => %d", s, buf, hex_buf, len, flags, result);
    if (hex_buf)
    {
        LocalFree(hex_buf);
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(int)
hook_sendto(
    SOCKET s,
    char *buf,
    int len,
    int flags,
    struct sockaddr *to,
    int tolen
    )
{
    int result = 0;
    char* hex_buf = NULL;
    char addr[256];

    HOOK_ENTER(send);

    if (len)
    {
        save_binary_buffer("sendto", NULL, (int)s, buf, len);
    }

    hex_buf = LocalAlloc(LPTR, 3 * (len + 1));
    if (hex_buf)
    {
        escape_binary(buf, len, hex_buf);
    }
    else
    {
        hex_buf = "[Failed allocating buffer!]";
    }
    _inet_ntop(to->sa_family, to, addr, sizeof(addr));
    result = (int)((fun6arg)(Hooks[HOOKID_sendto].orig_addr))((void*)s, buf, (void*)len, (void*)flags, to, (void*)tolen);
    log("APICALL: sendto(%d, 0x%p:\"%s\", %d, 0x%08X, \"%s\", %d) => %d", s, buf, hex_buf, len, flags, addr, tolen, result);
    if (hex_buf)
    {
        LocalFree(hex_buf);
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(int)
hook_recv(
    SOCKET s,
    char *buf,
    int len,
    int flags
    )
{
    int result = 0;

    HOOK_ENTER(recv);

    result = (int)((fun4arg)(Hooks[HOOKID_recv].orig_addr))((void*)s, buf, (void*)len, (void*)flags);
    log("APICALL: recv(%d, 0x%p, %d, 0x%08X) => %d", s, buf, len, flags, result);

    if (result)
    {
        save_binary_buffer("recv", NULL, (int)s, buf, result);
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(size_t)
hook_CreateProcessInternalW(
    PVOID unknown1,
    LPWSTR lpApplicationName,
    LPWSTR lpCommandLine,
    LPSECURITY_ATTRIBUTES lpProcessAttributes,
    LPSECURITY_ATTRIBUTES lpThreadAttributes,
    BOOL bInheritHandles,
    DWORD dwCreationFlags,
    LPVOID lpEnvironment,
    LPWSTR lpCurrentDirectory,
    LPSTARTUPINFOW lpStartupInfo,
    LPPROCESS_INFORMATION lpProcessInformation,
    PVOID unknown2
    )
{
    size_t result = 0;
    // DWORD orig_flags = dwCreationFlags;

    HOOK_ENTER(CreateProcessInternalW);

    result = (size_t)((fun12arg)(Hooks[HOOKID_CreateProcessInternalW].orig_addr))(unknown1, lpApplicationName, lpCommandLine,
            lpProcessAttributes, lpThreadAttributes, (void*)bInheritHandles, (void*)(dwCreationFlags), // | CREATE_SUSPENDED),
            lpEnvironment, lpCurrentDirectory, lpStartupInfo, lpProcessInformation, unknown2);
    log("APICALL: CreateProcessInternalW(0x%p, \"%S\", \"%S\", 0x%p, 0x%p, %d, 0x%08X, 0x%p, \"%S\", "
            "0x%p, 0x%p, 0x%p) => 0x%p (PID: %d)", unknown1, PRINT_WSTR(lpApplicationName), PRINT_WSTR(lpCommandLine),
            lpProcessAttributes, lpThreadAttributes, bInheritHandles, dwCreationFlags, lpEnvironment,
            PRINT_WSTR(lpCurrentDirectory), lpStartupInfo, lpProcessInformation, unknown2, result,
            (result && !IsBadReadPtr(lpProcessInformation, sizeof(void*))) ? lpProcessInformation->dwProcessId : -1);
    if (result)
    {
        /*
        if (!inject_mwmon_dll(lpProcessInformation->hProcess))
        {
            err("inject_mwmon_dll(0x%p) failed! Last error: 0x%08X", lpProcessInformation->hProcess, result);
        }
        if ((!(orig_flags & CREATE_SUSPENDED)) & !ResumeThread(lpProcessInformation->hThread))
        {
            err("ResumeThread(0x%p) failed! Last error: 0x%08X", lpProcessInformation->hThread, result);
        }
        */
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(size_t)
hook_CreateProcessA(
    LPSTR lpApplicationName,
    LPSTR lpCommandLine,
    LPSECURITY_ATTRIBUTES lpProcessAttributes,
    LPSECURITY_ATTRIBUTES lpThreadAttributes,
    BOOL bInheritHandles,
    DWORD dwCreationFlags,
    LPVOID lpEnvironment,
    LPSTR lpCurrentDirectory,
    LPSTARTUPINFO lpStartupInfo,
    LPPROCESS_INFORMATION lpProcessInformation
    )
{
    size_t result = 0;
    // DWORD orig_flags = dwCreationFlags;

    HOOK_ENTER(CreateProcessA);

    result = (size_t)((fun10arg)(Hooks[HOOKID_CreateProcessA].orig_addr))(lpApplicationName, lpCommandLine,
            lpProcessAttributes, lpThreadAttributes, (void*)bInheritHandles, (void*)(dwCreationFlags),//) | CREATE_SUSPENDED),
            lpEnvironment, lpCurrentDirectory, lpStartupInfo, lpProcessInformation);
    log("APICALL: CreateProcessA(\"%s\", \"%s\", 0x%p, 0x%p, %d, 0x%08X, 0x%p, \"%s\", "
            "0x%p, 0x%p) => 0x%p (PID: %d)", PRINT_STR(lpApplicationName), PRINT_STR(lpCommandLine),
            lpProcessAttributes, lpThreadAttributes, bInheritHandles, dwCreationFlags, lpEnvironment,
            PRINT_STR(lpCurrentDirectory), lpStartupInfo, lpProcessInformation, result,
            (result && !IsBadReadPtr(lpProcessInformation, sizeof(void*))) ? lpProcessInformation->dwProcessId : -1);
    if (result)
    {
        /*
        if (!inject_mwmon_dll(lpProcessInformation->hProcess))
        {
            err("inject_mwmon_dll(0x%p) failed! Last error: 0x%08X", lpProcessInformation->hProcess, result);
        }
        if ((!(orig_flags & CREATE_SUSPENDED)) & !ResumeThread(lpProcessInformation->hThread))
        {
            err("ResumeThread(0x%p) failed! Last error: 0x%08X", lpProcessInformation->hThread, result);
        }
        */
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(size_t)
hook_CreateProcessW(
    LPWSTR lpApplicationName,
    LPWSTR lpCommandLine,
    LPSECURITY_ATTRIBUTES lpProcessAttributes,
    LPSECURITY_ATTRIBUTES lpThreadAttributes,
    BOOL bInheritHandles,
    DWORD dwCreationFlags,
    LPVOID lpEnvironment,
    LPWSTR lpCurrentDirectory,
    LPSTARTUPINFOW lpStartupInfo,
    LPPROCESS_INFORMATION lpProcessInformation
    )
{
    size_t result = 0;
    // DWORD orig_flags = dwCreationFlags;

    HOOK_ENTER(CreateProcessW);

    result = (size_t)((fun10arg)(Hooks[HOOKID_CreateProcessW].orig_addr))(lpApplicationName, lpCommandLine,
            lpProcessAttributes, lpThreadAttributes, (void*)bInheritHandles, (void*)(dwCreationFlags), // | CREATE_SUSPENDED),
            lpEnvironment, lpCurrentDirectory, lpStartupInfo, lpProcessInformation);
    log("APICALL: CreateProcessW(\"%S\", \"%S\", 0x%p, 0x%p, %d, 0x%08X, 0x%p, \"%S\", "
            "0x%p, 0x%p) => 0x%p (PID: %d)", PRINT_WSTR(lpApplicationName), PRINT_WSTR(lpCommandLine),
            lpProcessAttributes, lpThreadAttributes, bInheritHandles, dwCreationFlags, lpEnvironment,
            PRINT_WSTR(lpCurrentDirectory), lpStartupInfo, lpProcessInformation, result,
            (result && !IsBadReadPtr(lpProcessInformation, sizeof(void*))) ? lpProcessInformation->dwProcessId : -1);
    if (result)
    {
        /*
        if (!inject_mwmon_dll(lpProcessInformation->hProcess))
        {
            err("inject_mwmon_dll(0x%p) failed! Last error: 0x%08X", lpProcessInformation->hProcess, result);
        }
        if ((!(orig_flags & CREATE_SUSPENDED)) & !ResumeThread(lpProcessInformation->hThread))
        {
            err("ResumeThread(0x%p) failed! Last error: 0x%08X", lpProcessInformation->hThread, result);
        }
        */
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(size_t)
hook_CreateFileA(
    LPSTR lpFileName,
    DWORD dwDesiredAccess,
    DWORD dwShareMode,
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    DWORD dwCreationDisposition,
    DWORD dwFlagsAndAttributes,
    HANDLE hTemplateFile
    )
{
    size_t result = 0;

    HOOK_ENTER(CreateFileA);

    result = (size_t)((fun7arg)(Hooks[HOOKID_CreateFileA].orig_addr))(lpFileName, (void*)dwDesiredAccess,
            (void*)dwShareMode, lpSecurityAttributes, (void*)dwCreationDisposition, (void*)dwFlagsAndAttributes,
            hTemplateFile);

    if (HOOK_DEPTH() == 1)
    {
        log("APICALL: CreateFileA(\"%s\", 0x%x, 0x%x, 0x%p, 0x%x, 0x%x, 0x%p) => 0x%p",
                PRINT_STR(lpFileName), dwDesiredAccess, dwShareMode, lpSecurityAttributes,
                dwCreationDisposition, dwFlagsAndAttributes, hTemplateFile, result);
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(int)
hook_CreateFileW(
    LPWSTR lpFileName,
    DWORD dwDesiredAccess,
    DWORD dwShareMode,
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    DWORD dwCreationDisposition,
    DWORD dwFlagsAndAttributes,
    HANDLE hTemplateFile
    )
{
    int result = 0;

    HOOK_ENTER(CreateFileW);

    result = (int)((fun7arg)(Hooks[HOOKID_CreateFileW].orig_addr))(lpFileName, (void*)dwDesiredAccess,
            (void*)dwShareMode, lpSecurityAttributes, (void*)dwCreationDisposition, (void*)dwFlagsAndAttributes,
            hTemplateFile);

    if (HOOK_DEPTH() == 1)
    {
        log("APICALL: CreateFileW(\"%S\", 0x%x, 0x%x, 0x%p, 0x%x, 0x%x, 0x%p) => %d",
                PRINT_WSTR(lpFileName), dwDesiredAccess, dwShareMode, lpSecurityAttributes,
                dwCreationDisposition, dwFlagsAndAttributes, hTemplateFile, result);
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(int)
hook_WriteFile(
    HANDLE hFile,
    LPVOID lpBuffer,
    DWORD nNumberOfBytesToWrite,
    LPDWORD lpNumberOfBytesWritten,
    LPOVERLAPPED lpOverlapped
    )
{
    int result = 0;

    HOOK_ENTER(WriteFile);

    result = (int)((fun5arg)(Hooks[HOOKID_WriteFile].orig_addr))(hFile, lpBuffer,
            (void*)nNumberOfBytesToWrite, lpNumberOfBytesWritten, (void*)lpOverlapped);

    if (HOOK_DEPTH() == 1)
    {
        log("APICALL: WriteFile(0x%08X, 0x%p, %d, 0x%p, 0x%p) => %d", hFile, lpBuffer, nNumberOfBytesToWrite,
            lpNumberOfBytesWritten, lpOverlapped, result);

        save_binary_buffer("WriteFile", NULL, (int)hFile, lpBuffer, nNumberOfBytesToWrite);
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_ReadProcessMemory(
    HANDLE hProcess,
    PVOID lpBaseAddress,
    PVOID lpBuffer,
    SIZE_T nSize,
    SIZE_T *lpNumberOfBytesRead
    )
{
    BOOL result = 0;

    HOOK_ENTER(ReadProcessMemory);

    result = (BOOL)((fun5arg)(Hooks[HOOKID_ReadProcessMemory].orig_addr))(hProcess, lpBaseAddress,
            lpBuffer, (PVOID)nSize, lpNumberOfBytesRead);
    log("APICALL: ReadProcessMemory(0x%p, 0x%p, 0x%p, 0x%p, 0x%p) => 0x%p", hProcess, lpBaseAddress,
            lpBuffer, nSize, lpNumberOfBytesRead);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_WriteProcessMemory(
    HANDLE hProcess,
    PVOID lpBaseAddress,
    PVOID lpBuffer,
    SIZE_T nSize,
    SIZE_T *lpNumberOfBytesWritten
    )
{
    BOOL result = 0;

    HOOK_ENTER(WriteProcessMemory);

    result = (BOOL)((fun5arg)(Hooks[HOOKID_WriteProcessMemory].orig_addr))(hProcess, lpBaseAddress,
            lpBuffer, (PVOID)nSize, lpNumberOfBytesWritten);
    log("APICALL: WriteProcessMemory(0x%p, 0x%p, 0x%p, 0x%p, 0x%p) => 0x%p", hProcess, lpBaseAddress,
            lpBuffer, nSize, lpNumberOfBytesWritten);

    save_binary_buffer("WriteProcessMemory", NULL, MWM_NO_TAG, lpBuffer, (int)nSize);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CopyFileA(
    LPSTR lpExistingFileName,
    LPSTR lpNewFileName,
    BOOL bFailIfExists
    )
{
    BOOL result = 0;

    HOOK_ENTER(CopyFileA);

    result = (BOOL)((fun3arg)(Hooks[HOOKID_CopyFileA].orig_addr))(lpExistingFileName, lpNewFileName,
            (PVOID)bFailIfExists);
    log("APICALL: CopyFileA(\"%s\", \"%s\", 0x%p) => 0x%p", PRINT_STR(lpExistingFileName),
            PRINT_STR(lpNewFileName), bFailIfExists, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CopyFileW(
    LPWSTR lpExistingFileName,
    LPWSTR lpNewFileName,
    BOOL bFailIfExists
    )
{
    BOOL result = 0;

    HOOK_ENTER(CopyFileW);

    result = (BOOL)((fun3arg)(Hooks[HOOKID_CopyFileW].orig_addr))(lpExistingFileName, lpNewFileName,
            (PVOID)bFailIfExists);
    log("APICALL: CopyFileW(\"%S\", \"%S\", 0x%p) => 0x%p", PRINT_WSTR(lpExistingFileName),
            PRINT_WSTR(lpNewFileName), bFailIfExists, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CopyFileExA(
    LPSTR lpExistingFileName,
    LPSTR lpNewFileName,
    LPPROGRESS_ROUTINE lpProgressRoutine,
    PVOID lpData,
    LPBOOL pbCancel,
    DWORD dwCopyFlags
    )
{
    BOOL result = 0;

    HOOK_ENTER(CopyFileExA);

    result = (BOOL)((fun6arg)(Hooks[HOOKID_CopyFileExA].orig_addr))(lpExistingFileName, lpNewFileName,
            lpProgressRoutine, lpData, pbCancel, (PVOID)dwCopyFlags);
    log("APICALL: CopyFileExA(\"%s\", \"%s\", 0x%p, 0x%p, 0x%p, 0x%X) => 0x%p", PRINT_STR(lpExistingFileName),
            PRINT_STR(lpNewFileName), lpProgressRoutine, lpData, pbCancel, dwCopyFlags, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CopyFileExW(
    LPWSTR lpExistingFileName,
    LPWSTR lpNewFileName,
    LPPROGRESS_ROUTINE lpProgressRoutine,
    PVOID lpData,
    LPBOOL pbCancel,
    DWORD dwCopyFlags
    )
{
    BOOL result = 0;

    HOOK_ENTER(CopyFileExW);

    result = (BOOL)((fun6arg)(Hooks[HOOKID_CopyFileExW].orig_addr))(lpExistingFileName, lpNewFileName,
            lpProgressRoutine, lpData, pbCancel, (PVOID)dwCopyFlags);
    log("APICALL: CopyFileExW(\"%S\", \"%S\", 0x%p, 0x%p, 0x%p, 0x%X) => 0x%p",
            PRINT_WSTR(lpExistingFileName), PRINT_WSTR(lpNewFileName), lpProgressRoutine, lpData, pbCancel, dwCopyFlags, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_MoveFileA(
    LPSTR lpExistingFileName,
    LPSTR lpNewFileName
    )
{
    BOOL result = 0;

    HOOK_ENTER(MoveFileA);

    result = (BOOL)((fun2arg)(Hooks[HOOKID_MoveFileA].orig_addr))(lpExistingFileName, lpNewFileName);
    log("APICALL: MoveFileA(\"%s\", \"%s\") => 0x%p", PRINT_STR(lpExistingFileName), PRINT_STR(lpNewFileName), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_MoveFileW(
    LPWSTR lpExistingFileName,
    LPWSTR lpNewFileName
    )
{
    BOOL result = 0;

    HOOK_ENTER(MoveFileW);

    result = (BOOL)((fun2arg)(Hooks[HOOKID_MoveFileW].orig_addr))(lpExistingFileName, lpNewFileName);
    log("APICALL: MoveFileW(\"%S\", \"%S\") => 0x%p", PRINT_WSTR(lpExistingFileName), PRINT_WSTR(lpNewFileName), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_CreateRemoteThread(
    HANDLE hProcess,
    LPSECURITY_ATTRIBUTES lpThreadAttributes,
    SIZE_T dwStackSize,
    LPTHREAD_START_ROUTINE lpStartAddress,
    PVOID lpParameter,
    DWORD dwCreationFlags,
    LPDWORD lpThreadId
    )
{
    HANDLE result = 0;

    HOOK_ENTER(CreateRemoteThread);

    result = (HANDLE)((fun7arg)(Hooks[HOOKID_CreateRemoteThread].orig_addr))(hProcess, lpThreadAttributes, (PVOID)dwStackSize, lpStartAddress, lpParameter, (PVOID)dwCreationFlags, lpThreadId);
    log("APICALL: CreateRemoteThread(0x%p, 0x%p, 0x%p, 0x%p, 0x%p, 0x%X, 0x%p) => 0x%p", hProcess, lpThreadAttributes, dwStackSize, lpStartAddress, lpParameter, dwCreationFlags, lpThreadId);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_VirtualProtectEx(
    HANDLE hProcess,
    PVOID lpAddress,
    SIZE_T dwSize,
    DWORD flNewProtect,
    PDWORD lpflOldProtect
    )
{
    BOOL result = 0;

    HOOK_ENTER(VirtualProtectEx);

    result = (BOOL)((fun5arg)(Hooks[HOOKID_VirtualProtectEx].orig_addr))(hProcess, lpAddress, (PVOID)dwSize, (PVOID)flNewProtect, lpflOldProtect);
    log("APICALL: VirtualProtectEx(0x%p, 0x%p, 0x%p, 0x%X, 0x%p) => 0x%p", hProcess, lpAddress, dwSize, flNewProtect, lpflOldProtect);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(LPVOID)
hook_VirtualAlloc(
    PVOID lpAddress,
    SIZE_T dwSize,
    DWORD flAllocationType,
    DWORD flProtect
    )
{
    LPVOID result = 0;

    HOOK_ENTER(VirtualAlloc);

    result = (LPVOID)((fun4arg)(Hooks[HOOKID_VirtualAlloc].orig_addr))(lpAddress, (PVOID)dwSize, (PVOID)flAllocationType, (PVOID)flProtect);
    log("APICALL: VirtualAlloc(0x%p, 0x%p, 0x%X, 0x%X) => 0x%p", lpAddress, dwSize, flAllocationType, flProtect);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_VirtualFree(
    PVOID lpAddress,
    SIZE_T dwSize,
    DWORD dwFreeType
    )
{
    BOOL result = 0;

    HOOK_ENTER(VirtualFree);

    if (dwSize)
    {
        save_binary_buffer("VirtualFree", NULL, MWM_NO_TAG, lpAddress, (int)dwSize);
    }

    result = (BOOL)((fun3arg)(Hooks[HOOKID_VirtualFree].orig_addr))(lpAddress, (PVOID)dwSize, (PVOID)dwFreeType);
    log("APICALL: VirtualFree(0x%p, 0x%p, 0x%X) => 0x%p", lpAddress, dwSize, dwFreeType);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_IsProcessorFeaturePresent(
    DWORD ProcessorFeature
    )
{
    BOOL result = FALSE;

    HOOK_ENTER(IsProcessorFeaturePresent);

    result = (BOOL)((fun1arg)(Hooks[HOOKID_IsProcessorFeaturePresent].orig_addr))((PVOID)ProcessorFeature);

    log("APICALL: IsProcessorFeaturePresent(0x%X) => %d", ProcessorFeature, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(DWORD)
hook_GetFileAttributesA(
    LPSTR lpFileName
    )
{
    DWORD result = 0;

    HOOK_ENTER(GetFileAttributesA);

    result = (DWORD)((fun1arg)(Hooks[HOOKID_GetFileAttributesA].orig_addr))(lpFileName);
    log("APICALL: GetFileAttributesA(\"%s\") => 0x%p", PRINT_STR(lpFileName), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(DWORD)
hook_GetFileAttributesW(
    LPWSTR lpFileName
    )
{
    DWORD result = 0;

    HOOK_ENTER(GetFileAttributesW);

    result = (DWORD)((fun1arg)(Hooks[HOOKID_GetFileAttributesW].orig_addr))(lpFileName);
    log("APICALL: GetFileAttributesW(\"%S\") => 0x%p", PRINT_WSTR(lpFileName), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_GetFileAttributesExA(
    LPSTR lpFileName,
    GET_FILEEX_INFO_LEVELS fInfoLevelId,
    PVOID lpFileInformation
    )
{
    BOOL result = 0;

    HOOK_ENTER(GetFileAttributesExA);

    result = (BOOL)((fun3arg)(Hooks[HOOKID_GetFileAttributesExA].orig_addr))(lpFileName, (PVOID)fInfoLevelId, lpFileInformation);
    log("APICALL: GetFileAttributesExA(\"%s\", 0x%p, 0x%p) => 0x%p", PRINT_STR(lpFileName), fInfoLevelId, lpFileInformation, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_GetFileAttributesExW(
    LPWSTR lpFileName,
    GET_FILEEX_INFO_LEVELS fInfoLevelId,
    PVOID lpFileInformation
    )
{
    BOOL result = 0;

    HOOK_ENTER(GetFileAttributesExW);

    result = (BOOL)((fun3arg)(Hooks[HOOKID_GetFileAttributesExW].orig_addr))(lpFileName, (PVOID)fInfoLevelId, lpFileInformation);
    log("APICALL: GetFileAttributesExW(\"%S\", 0x%p, 0x%p) => 0x%p", PRINT_WSTR(lpFileName), fInfoLevelId, lpFileInformation, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_FindFirstFileA(
    LPSTR lpFileName,
    LPWIN32_FIND_DATAA lpFindFileData
    )
{
    HANDLE result = 0;

    HOOK_ENTER(FindFirstFileA);

    result = (HANDLE)((fun2arg)(Hooks[HOOKID_FindFirstFileA].orig_addr))(lpFileName, lpFindFileData);
    log("APICALL: FindFirstFileA(\"%s\", 0x%p) => 0x%p", PRINT_STR(lpFileName), lpFindFileData, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_FindFirstFileW(
    LPWSTR lpFileName,
    LPWIN32_FIND_DATAW lpFindFileData
    )
{
    HANDLE result = 0;

    HOOK_ENTER(FindFirstFileW);

    result = (HANDLE)((fun2arg)(Hooks[HOOKID_FindFirstFileW].orig_addr))(lpFileName, lpFindFileData);
    log("APICALL: FindFirstFileW(\"%S\", 0x%p) => 0x%p", PRINT_WSTR(lpFileName), lpFindFileData, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_FindFirstFileExA(
    LPSTR lpFileName,
    FINDEX_INFO_LEVELS fInfoLevelId,
    PVOID lpFindFileData,
    FINDEX_SEARCH_OPS fSearchOp,
    PVOID lpSearchFilter,
    DWORD dwAdditionalFlags
    )
{
    HANDLE result = 0;

    HOOK_ENTER(FindFirstFileExA);

    result = (HANDLE)((fun6arg)(Hooks[HOOKID_FindFirstFileExA].orig_addr))(lpFileName, (PVOID)fInfoLevelId, lpFindFileData, (PVOID)fSearchOp, lpSearchFilter, (PVOID)dwAdditionalFlags);
    log("APICALL: FindFirstFileExA(\"%s\", 0x%p, 0x%p, 0x%p, 0x%p, 0x%X) => 0x%p", PRINT_STR(lpFileName), fInfoLevelId, lpFindFileData, fSearchOp, lpSearchFilter, dwAdditionalFlags, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_FindFirstFileExW(
    LPWSTR lpFileName,
    FINDEX_INFO_LEVELS fInfoLevelId,
    PVOID lpFindFileData,
    FINDEX_SEARCH_OPS fSearchOp,
    PVOID lpSearchFilter,
    DWORD dwAdditionalFlags
    )
{
    HANDLE result = 0;

    HOOK_ENTER(FindFirstFileExW);

    result = (HANDLE)((fun6arg)(Hooks[HOOKID_FindFirstFileExW].orig_addr))(lpFileName, (PVOID)fInfoLevelId, lpFindFileData, (PVOID)fSearchOp, lpSearchFilter, (PVOID)dwAdditionalFlags);
    log("APICALL: FindFirstFileExW(\"%S\", 0x%p, 0x%p, 0x%p, 0x%p, 0x%X) => 0x%p", PRINT_WSTR(lpFileName), fInfoLevelId, lpFindFileData, fSearchOp, lpSearchFilter, dwAdditionalFlags, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_SetFileAttributesA(
    LPSTR lpFileName,
    DWORD dwFileAttributes
    )
{
    BOOL result = 0;

    HOOK_ENTER(SetFileAttributesA);

    result = (BOOL)((fun2arg)(Hooks[HOOKID_SetFileAttributesA].orig_addr))(lpFileName, (PVOID)dwFileAttributes);
    log("APICALL: SetFileAttributesA(\"%s\", 0x%X) => 0x%p", PRINT_STR(lpFileName), dwFileAttributes, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_SetFileAttributesW(
    LPWSTR lpFileName,
    DWORD dwFileAttributes
    )
{
    BOOL result = 0;

    HOOK_ENTER(SetFileAttributesW);

    result = (BOOL)((fun2arg)(Hooks[HOOKID_SetFileAttributesW].orig_addr))(lpFileName, (PVOID)dwFileAttributes);
    log("APICALL: SetFileAttributesW(\"%S\", 0x%X) => 0x%p", PRINT_WSTR(lpFileName), dwFileAttributes, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(size_t)
hook_BCryptDecrypt(
    PVOID hKey,
    PUCHAR pbInput,
    ULONG cbInput,
    VOID *pPaddingInfo,
    PUCHAR pbIV,
    ULONG cbIV,
    PUCHAR pbOutput,
    ULONG cbOutput,
    ULONG *pcbResult,
    ULONG dwFlags
    )
{
    size_t result = 0;

    HOOK_ENTER(BCryptDecrypt);

    if (pbInput && cbInput)
    {
        // with symmetrical encryption, BCryptDecrypt() could be used for encryption
        save_binary_buffer("BCryptDecrypt-PRE", NULL, MWM_NO_TAG, pbOutput, pcbResult ? *pcbResult : cbOutput);
    }

    result = (size_t)((fun10arg)(Hooks[HOOKID_BCryptDecrypt].orig_addr))(hKey, pbInput, (PVOID)cbInput,
            pPaddingInfo, pbIV, (PVOID)cbIV, pbOutput, (PVOID)cbOutput, pcbResult, (PVOID)dwFlags);

    log("APICALL: BCryptDecrypt(hKey=0x%p, in=0x%p[%d], out=0x%p[%d/%d], flags=0x%X) => 0x%p", hKey, pbInput, cbInput,
            pbOutput, cbOutput, pcbResult ? *pcbResult : -1, dwFlags, result);

    if (NT_SUCCESS(result))
    {
        save_binary_buffer("BCryptDecrypt", NULL, MWM_NO_TAG, pbOutput, pcbResult ? *pcbResult : cbOutput);
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(size_t)
hook_BCryptEncrypt(
    PVOID hKey,
    PUCHAR pbInput,
    ULONG cbInput,
    VOID *pPaddingInfo,
    PUCHAR pbIV,
    ULONG cbIV,
    PUCHAR pbOutput,
    ULONG cbOutput,
    ULONG *pcbResult,
    ULONG dwFlags
    )
{
    size_t result = 0;

    HOOK_ENTER(BCryptEncrypt);

    if (pbInput && cbInput)
    {
        save_binary_buffer("BCryptEncrypt", NULL, MWM_NO_TAG, pbInput, cbInput);
    }

    result = (size_t)((fun10arg)(Hooks[HOOKID_BCryptEncrypt].orig_addr))(hKey, pbInput, (PVOID)cbInput,
            pPaddingInfo, pbIV, (PVOID)cbIV, pbOutput, (PVOID)cbOutput, pcbResult, (PVOID)dwFlags);

    log("APICALL: BCryptEncrypt(hKey=0x%p, in=0x%p[%d], out=0x%p[%d/%d], flags=0x%X) => 0x%p", hKey, pbInput, cbInput,
            pbOutput, cbOutput, pcbResult ? *pcbResult : -1, dwFlags, result);

    if (result && pbOutput && pcbResult && *pcbResult)
    {
        // with symmetrical encryption, BCryptEncrypt() could be used for decryption
        save_binary_buffer("BCryptEncrypt-POST", NULL, MWM_NO_TAG, pbOutput, *pcbResult);
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(size_t)
hook_URLDownloadToFileA(
    LPVOID pCaller,
    LPSTR szURL,
    LPSTR szFileName,
    DWORD dwReserved,
    PVOID lpfnCB
    )
{
    size_t result = 0;

    HOOK_ENTER(URLDownloadToFileA);

    result = (size_t)((fun5arg)(Hooks[HOOKID_URLDownloadToFileA].orig_addr))(pCaller, szURL, szFileName,
            (PVOID)dwReserved, lpfnCB);

    log("APICALL: URLDownloadToFileA(szURL=%s, szFileName=%s) => 0x%p", PRINT_STR(szURL), PRINT_STR(szFileName), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(size_t)
hook_URLDownloadToFileW(
    LPVOID pCaller,
    LPWSTR szURL,
    LPWSTR szFileName,
    DWORD dwReserved,
    PVOID lpfnCB
    )
{
    size_t result = 0;

    HOOK_ENTER(URLDownloadToFileW);

    result = (size_t)((fun5arg)(Hooks[HOOKID_URLDownloadToFileW].orig_addr))(pCaller, szURL, szFileName,
            (PVOID)dwReserved, lpfnCB);

    log("APICALL: URLDownloadToFileW(szURL=%S, szFileName=%S) => 0x%p", PRINT_WSTR(szURL), PRINT_WSTR(szFileName), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptHashData(
    PVOID hHash,
    BYTE *pbData,
    DWORD dwDataLen,
    DWORD dwFlags
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptHashData);

    result = (BOOL)((fun4arg)(Hooks[HOOKID_CryptHashData].orig_addr))(hHash, pbData, (PVOID)dwDataLen,
            (PVOID)dwFlags);

    log("APICALL: CryptHashData(0x%p, 0x%p, %d, 0x%X) => 0x%p", hHash, pbData, dwDataLen, dwFlags, result);

    if (result)
    {
        save_binary_buffer("CryptHashData", NULL, MWM_NO_TAG, pbData, dwDataLen);
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptDecrypt(
    HANDLE hKey,
    HANDLE hHash,
    BOOL Final,
    DWORD dwFlags,
    BYTE *pbData,
    DWORD *pdwDataLen
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptDecrypt);

    if (result && pbData && pdwDataLen)
    {
        // with symmetrical encryption, CryptDecrypt() could be used for encryption
        save_binary_buffer("CryptDecrypt-PRE", NULL, MWM_NO_TAG, pbData, *pdwDataLen);
    }

    result = (BOOL)((fun6arg)(Hooks[HOOKID_CryptDecrypt].orig_addr))(hKey, hHash, (PVOID)Final,
    (PVOID)dwFlags, pbData, pdwDataLen);

    // todo: CryptExportKey
    log("APICALL: CryptDecrypt(0x%p, 0x%p, %d, 0x%X, 0x%p, %d) => 0x%p", hKey, hHash, Final,
            dwFlags, pbData, pdwDataLen ? pdwDataLen : 0, result);

    if (result && pbData && pdwDataLen)
    {
        save_binary_buffer("CryptDecrypt", NULL, MWM_NO_TAG, pbData, *pdwDataLen);
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptEncrypt(
    HANDLE hKey,
    HANDLE hHash,
    BOOL Final,
    DWORD dwFlags,
    BYTE *pbData,
    DWORD *pdwDataLen,
    DWORD dwBufLen
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptEncrypt);

    if (pbData && pdwDataLen)
    {
        save_binary_buffer("CryptEncrypt", NULL, MWM_NO_TAG, pbData, *pdwDataLen);
    }

    result = (BOOL)((fun7arg)(Hooks[HOOKID_CryptEncrypt].orig_addr))(hKey, hHash, (PVOID)Final,
    (PVOID)dwFlags, pbData, pdwDataLen, (PVOID)dwBufLen);

    // todo: CryptExportKey
    log("APICALL: CryptEncrypt(0x%p, 0x%p, %d, 0x%X, 0x%p, %d, %d) => 0x%p", hKey, hHash, Final,
            dwFlags, pbData, pdwDataLen ? *pdwDataLen : 0, dwBufLen, result);

    if (result && pbData && pdwDataLen)
    {
        // with symmetrical encryption, CryptEncrypt() could be used for decryption
        save_binary_buffer("CryptEncrypt-POST", NULL, MWM_NO_TAG, pbData, *pdwDataLen);
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptBinaryToStringA(
    BYTE *pbBinary,
    DWORD cbBinary,
    DWORD dwFlags,
    LPSTR pszString,
    DWORD *pcchString
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptBinaryToStringA);

    result = (BOOL)((fun5arg)(Hooks[HOOKID_CryptBinaryToStringA].orig_addr))(pbBinary,
            (PVOID)cbBinary, (PVOID)dwFlags, pszString, pcchString);
    log("APICALL: CryptBinaryToStringA(0x%p, 0x%X, 0x%X, \"%s\", 0x%X) => 0x%p", pbBinary,
            cbBinary, dwFlags, PRINT_STR(pszString), pcchString, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptBinaryToStringW(
    BYTE *pbBinary,
    DWORD cbBinary,
    DWORD dwFlags,
    LPWSTR pszString,
    DWORD *pcchString
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptBinaryToStringW);

    result = (BOOL)((fun5arg)(Hooks[HOOKID_CryptBinaryToStringW].orig_addr))(pbBinary,
            (PVOID)cbBinary, (PVOID)dwFlags, pszString, pcchString);
    log("APICALL: CryptBinaryToStringW(0x%p, 0x%X, 0x%X, \"%S\", 0x%X) => 0x%p", pbBinary,
            cbBinary, dwFlags, PRINT_WSTR(pszString), pcchString, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptStringToBinaryA(
    LPSTR pszString,
    DWORD cchString,
    DWORD dwFlags,
    BYTE *pbBinary,
    DWORD *pcbBinary,
    DWORD *pdwSkip,
    DWORD *pdwFlags
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptStringToBinaryA);

    result = (BOOL)((fun7arg)(Hooks[HOOKID_CryptStringToBinaryA].orig_addr))(pszString,
            (PVOID)cchString, (PVOID)dwFlags, pbBinary, pcbBinary, pdwSkip, pdwFlags);
    log("APICALL: CryptStringToBinaryA(\"%s\", 0x%X, 0x%X, 0x%p, 0x%X, 0x%X, 0x%X) => 0x%p",
            PRINT_STR(pszString), cchString, dwFlags, pbBinary, pcbBinary, pdwSkip, pdwFlags, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptStringToBinaryW(
    LPWSTR pszString,
    DWORD cchString,
    DWORD dwFlags,
    BYTE *pbBinary,
    DWORD *pcbBinary,
    DWORD *pdwSkip,
    DWORD *pdwFlags
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptStringToBinaryW);

    result = (BOOL)((fun7arg)(Hooks[HOOKID_CryptStringToBinaryW].orig_addr))(pszString,
            (PVOID)cchString, (PVOID)dwFlags, pbBinary, pcbBinary, pdwSkip, pdwFlags);
    log("APICALL: CryptStringToBinaryW(\"%S\", 0x%X, 0x%X, 0x%p, 0x%X, 0x%X, 0x%X) => 0x%p",
            PRINT_WSTR(pszString), cchString, dwFlags, pbBinary, pcbBinary, pdwSkip, pdwFlags, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptProtectMemory(
    PVOID pDataIn,
    DWORD cbDataIn,
    DWORD dwFlags
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptProtectMemory);

    result = (BOOL)((fun3arg)(Hooks[HOOKID_CryptProtectMemory].orig_addr))(pDataIn,
            (PVOID)cbDataIn, (PVOID)dwFlags);
    log("APICALL: CryptProtectMemory(0x%p, 0x%X, 0x%X) => 0x%p", pDataIn, cbDataIn, dwFlags, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptUnprotectMemory(
    PVOID pDataIn,
    DWORD cbDataIn,
    DWORD dwFlags
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptUnprotectMemory);

    result = (BOOL)((fun3arg)(Hooks[HOOKID_CryptUnprotectMemory].orig_addr))(pDataIn,
            (PVOID)cbDataIn, (PVOID)dwFlags);
    log("APICALL: CryptUnprotectMemory(0x%p, 0x%X, 0x%X) => 0x%p", pDataIn, cbDataIn, dwFlags, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptProtectData(
    DATA_BLOB *pDataIn,
    LPWSTR szDataDescr,
    DATA_BLOB *pOptionalEntropy,
    PVOID pvReserved,
    PVOID pPromptStruct,
    DWORD dwFlags,
    DATA_BLOB *pDataOut
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptProtectData);

    if (pDataIn && pDataIn->cbData)
    {
        save_binary_buffer("CryptProtectData", NULL, MWM_NO_TAG, pDataIn->pbData, pDataIn->cbData);
    }

    result = (BOOL)((fun7arg)(Hooks[HOOKID_CryptProtectData].orig_addr))(pDataIn,
            szDataDescr, pOptionalEntropy, pvReserved, pPromptStruct, (PVOID)dwFlags, pDataOut);
    log("APICALL: CryptProtectData(0x%p, \"%S\", 0x%p, 0x%p, 0x%p, 0x%X, 0x%p) => 0x%p",
            pDataIn, PRINT_WSTR(szDataDescr), pOptionalEntropy, pvReserved, pPromptStruct, dwFlags, pDataOut,
            result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptUnprotectData(
    DATA_BLOB *pDataIn,
    LPWSTR *ppszDataDescr,
    DATA_BLOB *pOptionalEntropy,
    PVOID pvReserved,
    PVOID pPromptStruct,
    DWORD dwFlags,
    DATA_BLOB *pDataOut
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptUnprotectData);

    result = (BOOL)((fun7arg)(Hooks[HOOKID_CryptUnprotectData].orig_addr))(pDataIn,
            ppszDataDescr, pOptionalEntropy, pvReserved, pPromptStruct, (PVOID)dwFlags, pDataOut);
    log("APICALL: CryptUnprotectData(0x%p, \"%S\", 0x%p, 0x%p, 0x%p, 0x%X, 0x%p) => 0x%p",
            pDataIn, ppszDataDescr ? PRINT_WSTR(*ppszDataDescr) : L"NULL", pOptionalEntropy, pvReserved,
            pPromptStruct, dwFlags, pDataOut, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptEncodeObject(
    DWORD dwCertEncodingType,
    LPSTR lpszStructType,
    void *pvStructInfo,
    BYTE *pbEncoded,
    DWORD *pcbEncoded
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptEncodeObject);

    result = (BOOL)((fun5arg)(Hooks[HOOKID_CryptEncodeObject].orig_addr))((PVOID)dwCertEncodingType,
            lpszStructType, pvStructInfo, pbEncoded, pcbEncoded);
    log("APICALL: CryptEncodeObject(0x%X, \"%s\", 0x%p, 0x%p, 0x%X) => 0x%p", dwCertEncodingType,
            PRINT_STR(lpszStructType), pvStructInfo, pbEncoded, pcbEncoded, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptDecodeObject(
    DWORD dwCertEncodingType,
    LPSTR lpszStructType,
    BYTE *pbEncoded,
    DWORD cbEncoded,
    DWORD dwFlags,
    void *pvStructInfo,
    DWORD *pcbStructInfo
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptDecodeObject);

    result = (BOOL)((fun7arg)(Hooks[HOOKID_CryptDecodeObject].orig_addr))((PVOID)dwCertEncodingType,
            lpszStructType, pbEncoded, (PVOID)cbEncoded, (PVOID)dwFlags, pvStructInfo, pcbStructInfo);
    log("APICALL: CryptDecodeObject(0x%X, \"%s\", 0x%p, 0x%X, 0x%X, 0x%p, 0x%X) => 0x%p",
            dwCertEncodingType, PRINT_STR(lpszStructType), pbEncoded, cbEncoded, dwFlags, pvStructInfo,
            pcbStructInfo, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptEncodeObjectEx(
    DWORD dwCertEncodingType,
    LPSTR lpszStructType,
    void *pvStructInfo,
    DWORD dwFlags,
    PCRYPT_ENCODE_PARA pEncodePara,
    void *pvEncoded,
    DWORD *pcbEncoded
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptEncodeObjectEx);

    result = (BOOL)((fun7arg)(Hooks[HOOKID_CryptEncodeObjectEx].orig_addr))((PVOID)dwCertEncodingType,
            lpszStructType, pvStructInfo, (PVOID)dwFlags, pEncodePara, pvEncoded, pcbEncoded);
    log("APICALL: CryptEncodeObjectEx(0x%X, \"%s\", 0x%p, 0x%X, 0x%p, 0x%p, 0x%X) => 0x%p",
            dwCertEncodingType, PRINT_STR(lpszStructType), pvStructInfo, dwFlags, pEncodePara,
            pvEncoded, pcbEncoded, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptDecodeObjectEx(
    DWORD dwCertEncodingType,
    LPSTR lpszStructType,
    BYTE *pbEncoded,
    DWORD cbEncoded,
    DWORD dwFlags,
    PCRYPT_DECODE_PARA pDecodePara,
    void *pvStructInfo,
    DWORD *pcbStructInfo
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptDecodeObjectEx);

    if (pbEncoded && cbEncoded)
    {
        save_binary_buffer("CryptDecodeObjectEx", NULL, MWM_NO_TAG, pbEncoded, cbEncoded);
    }

    result = (BOOL)((fun8arg)(Hooks[HOOKID_CryptDecodeObjectEx].orig_addr))((PVOID)dwCertEncodingType,
            lpszStructType, pbEncoded, (PVOID)cbEncoded, (PVOID)dwFlags, pDecodePara, pvStructInfo,
            pcbStructInfo);
    log("APICALL: CryptDecodeObjectEx(0x%X, \"%s\", 0x%p, 0x%X, 0x%X, 0x%p, 0x%p, 0x%X) => 0x%p",
            dwCertEncodingType, PRINT_STR(lpszStructType), pbEncoded, cbEncoded, dwFlags, pDecodePara,
            pvStructInfo, pcbStructInfo, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(DWORD)
hook_CertGetNameStringA(
    PCERT_CONTEXT pCertContext,
    DWORD dwType,
    DWORD dwFlags,
    void *pvTypePara,
    LPSTR pszNameString,
    DWORD cchNameString
    )
{
    DWORD result = 0;

    HOOK_ENTER(CertGetNameStringA);

    result = (DWORD)((fun6arg)(Hooks[HOOKID_CertGetNameStringA].orig_addr))(pCertContext,
            (PVOID)dwType, (PVOID)dwFlags, pvTypePara, pszNameString, (PVOID)cchNameString);
    log("APICALL: CertGetNameStringA(0x%p, 0x%X, 0x%X, 0x%p, \"%s\", 0x%X) => 0x%p",
            pCertContext, dwType, dwFlags, pvTypePara, PRINT_STR(pszNameString), cchNameString,
            result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptImportKey(
    HCRYPTPROV hProv,
    BYTE *pbData,
    DWORD dwDataLen,
    HCRYPTKEY hPubKey,
    DWORD dwFlags,
    HCRYPTKEY *phKey
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptImportKey);

    result = (BOOL)((fun6arg)(Hooks[HOOKID_CryptImportKey].orig_addr))((PVOID)hProv, pbData,
            (PVOID)dwDataLen, (PVOID)hPubKey, (PVOID)dwFlags, phKey);
    log("APICALL: CryptImportKey(0x%p, 0x%p, 0x%X, 0x%p, 0x%X, 0x%p) => 0x%p", hProv, pbData,
            dwDataLen, hPubKey, dwFlags, phKey, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptGenKey(
    HCRYPTPROV hProv,
    ALG_ID Algid,
    DWORD dwFlags,
    HCRYPTKEY *phKey
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptGenKey);

    result = (BOOL)((fun4arg)(Hooks[HOOKID_CryptGenKey].orig_addr))((PVOID)hProv, (PVOID)Algid, (PVOID)dwFlags, phKey);
    log("APICALL: CryptGenKey(0x%p, 0x%p, 0x%X, 0x%p) => 0x%p", hProv, Algid, dwFlags, phKey, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptDestroyKey(
    HCRYPTKEY hKey
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptDestroyKey);

    result = (BOOL)((fun1arg)(Hooks[HOOKID_CryptDestroyKey].orig_addr))((PVOID)hKey);
    log("APICALL: CryptDestroyKey(0x%p) => 0x%p", hKey);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HCRYPTMSG)
hook_CryptMsgOpenToEncode(
    DWORD dwMsgEncodingType,
    DWORD dwFlags,
    DWORD dwMsgType,
    void *pvMsgEncodeInfo,
    LPSTR pszInnerContentObjID,
    PCMSG_STREAM_INFO pStreamInfo
    )
{
    HCRYPTMSG result = 0;

    HOOK_ENTER(CryptMsgOpenToEncode);

    result = (HCRYPTMSG)((fun6arg)(Hooks[HOOKID_CryptMsgOpenToEncode].orig_addr))((PVOID)dwMsgEncodingType,
            (PVOID)dwFlags, (PVOID)dwMsgType, pvMsgEncodeInfo, pszInnerContentObjID, pStreamInfo);
    log("APICALL: CryptMsgOpenToEncode(0x%X, 0x%X, 0x%X, 0x%p, \"%s\", 0x%p) => 0x%p", dwMsgEncodingType,
            dwFlags, dwMsgType, pvMsgEncodeInfo, PRINT_STR(pszInnerContentObjID), pStreamInfo, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HCRYPTMSG)
hook_CryptMsgOpenToDecode(
    DWORD dwMsgEncodingType,
    DWORD dwFlags,
    DWORD dwMsgType,
    HCRYPTPROV_LEGACY hCryptProv,
    PCERT_INFO pRecipientInfo,
    PCMSG_STREAM_INFO pStreamInfo
    )
{
    HCRYPTMSG result = 0;

    HOOK_ENTER(CryptMsgOpenToDecode);

    result = (HCRYPTMSG)((fun6arg)(Hooks[HOOKID_CryptMsgOpenToDecode].orig_addr))((PVOID)dwMsgEncodingType,
            (PVOID)dwFlags, (PVOID)dwMsgType, (PVOID)hCryptProv, pRecipientInfo, pStreamInfo);
    log("APICALL: CryptMsgOpenToDecode(0x%X, 0x%X, 0x%X, 0x%p, 0x%p, 0x%p) => 0x%p", dwMsgEncodingType,
            dwFlags, dwMsgType, hCryptProv, pRecipientInfo, pStreamInfo, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CryptMsgUpdate(
    HCRYPTMSG hCryptMsg,
    BYTE *pbData,
    DWORD cbData,
    BOOL fFinal
    )
{
    BOOL result = 0;

    HOOK_ENTER(CryptMsgUpdate);

    result = (BOOL)((fun4arg)(Hooks[HOOKID_CryptMsgUpdate].orig_addr))((PVOID)hCryptMsg, pbData,
            (PVOID)cbData, (PVOID)fFinal);
    log("APICALL: CryptMsgUpdate(0x%p, 0x%p, 0x%X, 0x%p) => 0x%p", hCryptMsg, pbData, cbData,
            fFinal, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(SECURITY_STATUS)
hook_NCryptProtectSecret(
    NCRYPT_DESCRIPTOR_HANDLE hDescriptor,
    DWORD dwFlags,
    BYTE *pbData,
    ULONG cbData,
    NCRYPT_ALLOC_PARA *pMemPara,
    HWND hWnd,
    BYTE **ppbProtectedBlob,
    ULONG *pcbProtectedBlob
    )
{
    SECURITY_STATUS result = 0;

    HOOK_ENTER(NCryptProtectSecret);

    result = (SECURITY_STATUS)((fun8arg)(Hooks[HOOKID_NCryptProtectSecret].orig_addr))((PVOID)hDescriptor,
            (PVOID)dwFlags, pbData, (PVOID)cbData, pMemPara, (PVOID)hWnd, ppbProtectedBlob, pcbProtectedBlob);
    log("APICALL: NCryptProtectSecret(0x%p, 0x%X, 0x%p, 0x%p, 0x%p, 0x%p, 0x%p, 0x%p) => 0x%p", hDescriptor,
            dwFlags, pbData, cbData, pMemPara, hWnd, ppbProtectedBlob, pcbProtectedBlob, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(SECURITY_STATUS)
hook_NCryptUnprotectSecret(
    NCRYPT_DESCRIPTOR_HANDLE *phDescriptor,
    DWORD dwFlags,
    BYTE *pbProtectedBlob,
    ULONG cbProtectedBlob,
    NCRYPT_ALLOC_PARA *pMemPara,
    HWND hWnd,
    BYTE **ppbData,
    ULONG *pcbData
    )
{
    SECURITY_STATUS result = 0;

    HOOK_ENTER(NCryptUnprotectSecret);

    result = (SECURITY_STATUS)((fun8arg)(Hooks[HOOKID_NCryptUnprotectSecret].orig_addr))(phDescriptor,
            (PVOID)dwFlags, pbProtectedBlob, (PVOID)cbProtectedBlob, pMemPara, (PVOID)hWnd, ppbData, pcbData);
    log("APICALL: NCryptUnprotectSecret(0x%p, 0x%X, 0x%p, 0x%p, 0x%p, 0x%p, 0x%p, 0x%p) => 0x%p",
            phDescriptor, dwFlags, pbProtectedBlob, cbProtectedBlob, pMemPara, hWnd, ppbData, pcbData, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HINSTANCE)
hook_ShellExecuteA(
    HWND hwnd,
    LPSTR lpOperation,
    LPSTR lpFile,
    LPSTR lpParameters,
    LPSTR lpDirectory,
    INT nShowCmd
    )
{
    HINSTANCE result = 0;

    HOOK_ENTER(ShellExecuteA);

    result = (HINSTANCE)((fun6arg)(Hooks[HOOKID_ShellExecuteA].orig_addr))(hwnd, lpOperation, lpFile,
    lpParameters, lpDirectory, (PVOID)nShowCmd);

    log("APICALL: ShellExecuteA(0x%p, \"%s\", \"%s\", \"%s\", \"%s\", %d) => 0x%p", hwnd,
            PRINT_STR(lpOperation), PRINT_STR(lpFile), PRINT_STR(lpParameters), PRINT_STR(lpDirectory),
            nShowCmd, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HINSTANCE)
hook_ShellExecuteW(
    HWND hwnd,
    LPWSTR lpOperation,
    LPWSTR lpFile,
    LPWSTR lpParameters,
    LPWSTR lpDirectory,
    INT nShowCmd
    )
{
    HINSTANCE result = 0;

    HOOK_ENTER(ShellExecuteW);

    result = (HINSTANCE)((fun6arg)(Hooks[HOOKID_ShellExecuteW].orig_addr))(hwnd, lpOperation, lpFile,
    lpParameters, lpDirectory, (PVOID)nShowCmd);

    log("APICALL: ShellExecuteW(0x%p, \"%S\", \"%S\", \"%S\", \"%S\", %d) => 0x%p", hwnd,
            PRINT_WSTR(lpOperation), PRINT_WSTR(lpFile), PRINT_WSTR(lpParameters), PRINT_WSTR(lpDirectory),
            nShowCmd, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_ShellExecuteExA(
    SHELLEXECUTEINFOA *pExecInfo
    )
{
    BOOL result = 0;

    HOOK_ENTER(ShellExecuteExA);

    log("APICALL: ShellExecuteExA(0x%p: lpVerb=\"%s\", lpFile=\"%s\", lpParameters=\"%s\", lpDirectory=\"%s\") => 0x%p",
            pExecInfo, PRINT_STR(pExecInfo->lpVerb), PRINT_STR(pExecInfo->lpFile),
            PRINT_STR(pExecInfo->lpParameters), PRINT_STR(pExecInfo->lpDirectory), result);

    result = (BOOL)((fun1arg)(Hooks[HOOKID_ShellExecuteExA].orig_addr))(pExecInfo);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_ShellExecuteExW(
    SHELLEXECUTEINFOW *pExecInfo
    )
{
    BOOL result = 0;

    HOOK_ENTER(ShellExecuteExW);

    log("APICALL: ShellExecuteExW(0x%p: lpVerb=\"%S\", lpFile=\"%S\", lpParameters=\"%S\", lpDirectory=\"%S\")",
            pExecInfo, PRINT_WSTR(pExecInfo->lpVerb), PRINT_WSTR(pExecInfo->lpFile),
            PRINT_WSTR(pExecInfo->lpParameters), PRINT_WSTR(pExecInfo->lpDirectory));

    result = (BOOL)((fun1arg)(Hooks[HOOKID_ShellExecuteExW].orig_addr))(pExecInfo);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_WinHttpOpen(
    LPWSTR pszAgentW,
    DWORD dwAccessType,
    LPWSTR pszProxyW,
    LPWSTR pszProxyBypassW,
    DWORD dwFlags
    )
{
    HANDLE result = NULL;

    HOOK_ENTER(WinHttpOpen);

    result = (HANDLE)((fun5arg)(Hooks[HOOKID_WinHttpOpen].orig_addr))(pszAgentW, (PVOID)dwAccessType,
            pszProxyW, pszProxyBypassW, (PVOID)dwFlags);

    log("APICALL: WinHttpOpen(\"%S\", 0x%X, \"%S\", 0x%X, 0x%X) => 0x%p", PRINT_WSTR(pszAgentW), dwAccessType,
            PRINT_WSTR(pszProxyW), pszProxyBypassW, dwFlags, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_WinHttpConnect(
    HANDLE hSession,
    LPWSTR pswzServerName,
    DWORD nServerPort,
    DWORD dwReserved
    )
{
    HANDLE result = NULL;

    HOOK_ENTER(WinHttpConnect);

    result = (HANDLE)((fun4arg)(Hooks[HOOKID_WinHttpConnect].orig_addr))(hSession, pswzServerName,
            (PVOID)nServerPort, (PVOID)dwReserved);

    log("APICALL: WinHttpConnect(0x%p, \"%S\", %d, %d) => 0x%p", hSession, PRINT_WSTR(pswzServerName),
            nServerPort, dwReserved, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_WinHttpOpenRequest(
    HANDLE hConnect,
    LPWSTR pwszVerb,
    LPWSTR pwszObjectName,
    LPWSTR pwszVersion,
    LPWSTR pwszReferrer,
    LPWSTR *ppwszAcceptTypes,
    DWORD dwFlags
    )
{
    HANDLE result = NULL;

    HOOK_ENTER(WinHttpOpenRequest);

    result = (HANDLE)((fun7arg)(Hooks[HOOKID_WinHttpOpenRequest].orig_addr))(hConnect, pwszVerb,
            pwszObjectName, pwszVersion, pwszReferrer, ppwszAcceptTypes, (PVOID)dwFlags);

    log("APICALL: WinHttpOpenRequest(0x%p, \"%S\", \"%S\", \"%S\", \"%S\", 0x%p, 0x%p) => 0x%p", hConnect,
            PRINT_WSTR(pwszVerb), PRINT_WSTR(pwszObjectName), PRINT_WSTR(pwszVersion), PRINT_WSTR(pwszReferrer),
            ppwszAcceptTypes, dwFlags, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOLEAN)
hook_WinHttpSendRequest(
    HANDLE hConnect,
    LPWSTR lpszHeaders,
    DWORD dwHeadersLength,
    LPVOID lpOptional,
    DWORD dwOptionalLength,
    DWORD dwTotalLength,
    DWORD_PTR dwContext
    )
{
    BOOLEAN result = 0;

    HOOK_ENTER(WinHttpSendRequest);

    result = (BOOLEAN)((fun7arg)(Hooks[HOOKID_WinHttpSendRequest].orig_addr))(hConnect, lpszHeaders,
            (PVOID)dwHeadersLength, lpOptional, (PVOID)dwOptionalLength, (PVOID)dwTotalLength, (PVOID)dwContext);

    log("APICALL: WinHttpSendRequest(0x%p, \"%S\", %d, 0x%p, %d, %d, 0x%p) => 0x%p", hConnect,
            PRINT_WSTR(lpszHeaders), dwHeadersLength, lpOptional, dwOptionalLength,
            dwTotalLength, dwContext, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOLEAN)
hook_WinHttpReadData(
    HANDLE hRequest,
    LPVOID lpBuffer,
    DWORD dwNumberOfBytesToRead,
    LPDWORD lpdwNumberOfBytesRead
    )
{
    BOOLEAN result = 0;

    HOOK_ENTER(WinHttpReadData);

    result = (BOOLEAN)((fun4arg)(Hooks[HOOKID_WinHttpReadData].orig_addr))(hRequest, lpBuffer,
            (PVOID)dwNumberOfBytesToRead, lpdwNumberOfBytesRead);

    log("APICALL: WinHttpReadData(0x%p, 0x%p, %d, 0x%p, 0x%p) => 0x%p", hRequest,
            lpBuffer, dwNumberOfBytesToRead, lpdwNumberOfBytesRead, result);

    if (result && lpBuffer && lpdwNumberOfBytesRead && *lpdwNumberOfBytesRead)
    {
        save_binary_buffer("WinHttpReadData", NULL, (int)hRequest, lpBuffer, *lpdwNumberOfBytesRead);
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOLEAN)
hook_WinHttpWriteData(
    HANDLE hRequest,
    LPVOID lpBuffer,
    DWORD dwNumberOfBytesToWrite,
    LPDWORD lpdwNumberOfBytesWritten
    )
{
    BOOLEAN result = 0;

    HOOK_ENTER(WinHttpWriteData);

    if (lpBuffer && dwNumberOfBytesToWrite)
    {
        save_binary_buffer("WinHttpWriteData", NULL, (int)hRequest, lpBuffer, dwNumberOfBytesToWrite);
    }

    result = (BOOLEAN)((fun4arg)(Hooks[HOOKID_WinHttpWriteData].orig_addr))(hRequest, lpBuffer,
            (PVOID)dwNumberOfBytesToWrite, lpdwNumberOfBytesWritten);

    log("APICALL: WinHttpWriteData(0x%p, 0x%p, %d, 0x%p, 0x%p) => 0x%p", hRequest,
            lpBuffer, dwNumberOfBytesToWrite, lpdwNumberOfBytesWritten, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOLEAN)
hook_WinHttpSetCredentials(
    HANDLE hRequest,
    DWORD AuthTargets,
    DWORD AuthScheme,
    LPWSTR pwszUserName,
    LPWSTR pwszPassword,
    LPVOID pAuthParams
    )
{
    BOOLEAN result = 0;

    HOOK_ENTER(WinHttpSetCredentials);

    result = (BOOLEAN)((fun6arg)(Hooks[HOOKID_WinHttpSetCredentials].orig_addr))(hRequest, (PVOID)AuthTargets,
            (PVOID)AuthScheme, pwszUserName, pwszPassword, pAuthParams);

    log("APICALL: WinHttpSetCredentials(0x%p, %d, %d, \"%S\", \"%S\", 0x%p) => 0x%p", hRequest,
            AuthTargets, AuthScheme, PRINT_WSTR(pwszUserName), PRINT_WSTR(pwszPassword), pAuthParams, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOLEAN)
hook_WinHttpQueryHeaders(
    HANDLE hRequest,
    DWORD dwInfoLevel,
    LPWSTR pwszName,
    LPVOID lpBuffer,
    LPDWORD lpdwBufferLength,
    LPDWORD lpdwIndex
    )
{
    BOOLEAN result = 0;

    HOOK_ENTER(WinHttpQueryHeaders);

    result = (BOOLEAN)((fun6arg)(Hooks[HOOKID_WinHttpQueryHeaders].orig_addr))(hRequest, (PVOID)dwInfoLevel,
            pwszName, lpBuffer, lpdwBufferLength, lpdwIndex);

    log("APICALL: WinHttpQueryHeaders(0x%p, %d, \"%S\", \"%s\", 0x%p, 0x%p) => 0x%p", hRequest, dwInfoLevel,
            PRINT_WSTR(pwszName), PRINT_STR(lpBuffer), lpdwBufferLength, lpdwIndex, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_HttpOpenRequestA(
    HANDLE hConnect,
    LPSTR pszVerb,
    LPSTR pszObjectName,
    LPSTR pszVersion,
    LPSTR pszReferrer,
    LPSTR *ppszAcceptTypes,
    DWORD dwFlags,
    DWORD_PTR dwContext
    )
{
    HANDLE result = NULL;

    HOOK_ENTER(HttpOpenRequestA);

    result = (HANDLE)((fun8arg)(Hooks[HOOKID_HttpOpenRequestA].orig_addr))(hConnect, pszVerb,
            pszObjectName, pszVersion, pszReferrer, ppszAcceptTypes, (PVOID)dwFlags,
            (PVOID)dwContext);

    log("APICALL: HttpOpenRequestA(0x%p, \"%s\", \"%s\", \"%s\", \"%s\", 0x%p, 0x%p, 0x%p) => 0x%p", hConnect,
            PRINT_STR(pszVerb), PRINT_STR(pszObjectName), PRINT_STR(pszVersion), PRINT_STR(pszReferrer),
            ppszAcceptTypes, dwFlags, dwContext, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_HttpOpenRequestW(
    HANDLE hConnect,
    LPWSTR pwszVerb,
    LPWSTR pwszObjectName,
    LPWSTR pwszVersion,
    LPWSTR pwszReferrer,
    LPWSTR *ppwszAcceptTypes,
    DWORD dwFlags,
    DWORD_PTR dwContext
    )
{
    HANDLE result = NULL;

    HOOK_ENTER(HttpOpenRequestW);

    result = (HANDLE)((fun8arg)(Hooks[HOOKID_HttpOpenRequestW].orig_addr))(hConnect, pwszVerb,
            pwszObjectName, pwszVersion, pwszReferrer, ppwszAcceptTypes, (PVOID)dwFlags,
            (PVOID)dwContext);

    log("APICALL: HttpOpenRequestW(0x%p, \"%S\", \"%S\", \"%S\", \"%S\", 0x%p, 0x%p, 0x%p) => 0x%p", hConnect,
            PRINT_WSTR(pwszVerb), PRINT_WSTR(pwszObjectName), PRINT_WSTR(pwszVersion), PRINT_WSTR(pwszReferrer),
            ppwszAcceptTypes, dwFlags, dwContext, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_FtpOpenFileA(
    HANDLE hInternet,
    LPSTR lpszFileName,
    DWORD dwAccess,
    DWORD dwFlags,
    DWORD_PTR dwContext
    )
{
    HANDLE result = 0;

    HOOK_ENTER(FtpOpenFileA);

    result = (HANDLE)((fun5arg)(Hooks[HOOKID_FtpOpenFileA].orig_addr))(hInternet, lpszFileName,
            (PVOID)dwAccess, (PVOID)dwFlags, (PVOID)dwContext);

    log("APICALL: FtpOpenFileA(0x%p, \"%s\", %d, 0x%X, 0x%p) => 0x%p", hInternet, PRINT_STR(lpszFileName),
            dwAccess, dwFlags, dwContext, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_FtpOpenFileW(
    HANDLE hInternet,
    LPWSTR lpszFileName,
    DWORD dwAccess,
    DWORD dwFlags,
    DWORD_PTR dwContext
    )
{
    HANDLE result = 0;

    HOOK_ENTER(FtpOpenFileW);

    result = (HANDLE)((fun5arg)(Hooks[HOOKID_FtpOpenFileW].orig_addr))(hInternet, lpszFileName,
            (PVOID)dwAccess, (PVOID)dwFlags, (PVOID)dwContext);

    log("APICALL: FtpOpenFileW(0x%p, \"%S\", %d, 0x%X, 0x%p) => 0x%p", hInternet, PRINT_WSTR(lpszFileName),
            dwAccess, dwFlags, dwContext, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_InternetConnectA(
    HINTERNET hInternet,
    LPSTR lpszServerName,
    INTERNET_PORT nServerPort,
    LPSTR lpszUserName,
    LPSTR lpszPassword,
    DWORD dwService,
    DWORD dwFlags,
    DWORD_PTR dwContext
    )
{
    HANDLE result = 0;

    HOOK_ENTER(InternetConnectA);

    result = (HANDLE)((fun8arg)(Hooks[HOOKID_InternetConnectA].orig_addr))((PVOID)hInternet,
            lpszServerName, (PVOID)nServerPort, lpszUserName, lpszPassword, (PVOID)dwService,
            (PVOID)dwFlags, (PVOID)dwContext);
    log("APICALL: InternetConnectA(0x%p, \"%s\", %d, \"%s\", \"%s\", 0x%X, 0x%X, 0x%p) => 0x%p",
            hInternet, PRINT_STR(lpszServerName), nServerPort, PRINT_STR(lpszUserName),
            PRINT_STR(lpszPassword), dwService, dwFlags, dwContext, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_InternetConnectW(
    HINTERNET hInternet,
    LPWSTR lpszServerName,
    INTERNET_PORT nServerPort,
    LPWSTR lpszUserName,
    LPWSTR lpszPassword,
    DWORD dwService,
    DWORD dwFlags,
    DWORD_PTR dwContext
    )
{
    HANDLE result = 0;

    HOOK_ENTER(InternetConnectW);

    result = (HANDLE)((fun8arg)(Hooks[HOOKID_InternetConnectW].orig_addr))((PVOID)hInternet,
            lpszServerName, (PVOID)nServerPort, lpszUserName, lpszPassword, (PVOID)dwService,
            (PVOID)dwFlags, (PVOID)dwContext);
    log("APICALL: InternetConnectW(0x%p, \"%S\", %d, \"%S\", \"%S\", 0x%X, 0x%X, 0x%p) => 0x%p",
            hInternet, PRINT_WSTR(lpszServerName), nServerPort, PRINT_WSTR(lpszUserName),
            PRINT_WSTR(lpszPassword), dwService, dwFlags, dwContext, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_InternetOpenA(
    LPSTR lpszAgent,
    DWORD dwAccessType,
    LPSTR lpszProxy,
    LPSTR lpszProxyBypass,
    DWORD dwFlags
    )
{
    HANDLE result = 0;

    HOOK_ENTER(InternetOpenA);

    result = (HANDLE)((fun5arg)(Hooks[HOOKID_InternetOpenA].orig_addr))(lpszAgent, (PVOID)dwAccessType,
            lpszProxy, lpszProxyBypass, (PVOID)dwFlags);
    log("APICALL: InternetOpenA(\"%s\", 0x%X, \"%s\", \"%s\", 0x%X) => 0x%p", PRINT_STR(lpszAgent),
            dwAccessType, PRINT_STR(lpszProxy), PRINT_STR(lpszProxyBypass), dwFlags, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_InternetOpenW(
    LPSTR lpszAgent,
    DWORD dwAccessType,
    LPSTR lpszProxy,
    LPSTR lpszProxyBypass,
    DWORD dwFlags
    )
{
    HANDLE result = 0;

    HOOK_ENTER(InternetOpenW);

    result = (HANDLE)((fun5arg)(Hooks[HOOKID_InternetOpenW].orig_addr))(lpszAgent, (PVOID)dwAccessType,
            lpszProxy, lpszProxyBypass, (PVOID)dwFlags);
    log("APICALL: InternetOpenW(\"%s\", 0x%X, \"%s\", \"%s\", 0x%X) => 0x%p", PRINT_STR(lpszAgent),
            dwAccessType, PRINT_STR(lpszProxy), PRINT_STR(lpszProxyBypass), dwFlags, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_InternetOpenUrlA(
    HANDLE hInternet,
    LPSTR lpszUrl,
    LPSTR lpszHeaders,
    DWORD dwHeadersLength,
    DWORD dwFlags,
    DWORD_PTR dwContext
    )
{
    HANDLE result = 0;

    HOOK_ENTER(InternetOpenUrlA);

    result = (HANDLE)((fun6arg)(Hooks[HOOKID_InternetOpenUrlA].orig_addr))(hInternet, lpszUrl,
            lpszHeaders, (PVOID)dwHeadersLength, (PVOID)dwFlags, (PVOID)dwContext);

    log("APICALL: InternetOpenUrlA(0x%p, \"%s\", \"%s\", %d, 0x%X, 0x%p) => 0x%p", hInternet, PRINT_STR(lpszUrl),
            PRINT_STR(lpszHeaders), dwHeadersLength, dwFlags, dwContext, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HANDLE)
hook_InternetOpenUrlW(
    HANDLE hInternet,
    LPWSTR lpszUrl,
    LPWSTR lpszHeaders,
    DWORD dwHeadersLength,
    DWORD dwFlags,
    DWORD_PTR dwContext
    )
{
    HANDLE result = 0;

    HOOK_ENTER(InternetOpenUrlW);

    result = (HANDLE)((fun6arg)(Hooks[HOOKID_InternetOpenUrlW].orig_addr))(hInternet, lpszUrl,
            lpszHeaders, (PVOID)dwHeadersLength, (PVOID)dwFlags, (PVOID)dwContext);

    log("APICALL: InternetOpenUrlW(0x%p, \"%S\", \"%S\", %d, 0x%X, 0x%p) => 0x%p", hInternet, PRINT_WSTR(lpszUrl),
            PRINT_WSTR(lpszHeaders), dwHeadersLength, dwFlags, dwContext, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOLEAN)
hook_InternetReadFile(
    HANDLE hFile,
    LPVOID lpBuffer,
    DWORD dwNumberOfBytesToRead,
    LPDWORD lpdwNumberOfBytesRead
    )
{
    BOOLEAN result = 0;

    HOOK_ENTER(InternetReadFile);

    result = (BOOLEAN)((fun4arg)(Hooks[HOOKID_InternetReadFile].orig_addr))(hFile, lpBuffer,
            (PVOID)dwNumberOfBytesToRead, lpdwNumberOfBytesRead);

    log("APICALL: InternetReadFile(0x%p, 0x%p, %d, 0x%p) => 0x%p", hFile, lpBuffer,
            dwNumberOfBytesToRead, lpdwNumberOfBytesRead, result);

    if (result && lpBuffer && lpdwNumberOfBytesRead && *lpdwNumberOfBytesRead)
    {
        save_binary_buffer("InternetReadFile", NULL, (int)hFile, lpBuffer, *lpdwNumberOfBytesRead);
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HWND)
hook_FindWindowExA(
    HWND hWndParent,
    HWND hWndChildAfter,
    LPSTR lpszClass,
    LPSTR lpszWindow
    )
{
    HWND result = 0;

    HOOK_ENTER(FindWindowExA);

    result = (HWND)((fun4arg)(Hooks[HOOKID_FindWindowExA].orig_addr))((PVOID)hWndParent, (PVOID)hWndChildAfter, lpszClass, lpszWindow);
    log("APICALL: FindWindowExA(0x%p, 0x%p, \"%s\", \"%s\") => 0x%p", hWndParent, hWndChildAfter, PRINT_STR(lpszClass), PRINT_STR(lpszWindow), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HWND)
hook_FindWindowExW(
    HWND hWndParent,
    HWND hWndChildAfter,
    LPWSTR lpszClass,
    LPWSTR lpszWindow
    )
{
    HWND result = 0;

    HOOK_ENTER(FindWindowExW);

    result = (HWND)((fun4arg)(Hooks[HOOKID_FindWindowExW].orig_addr))((PVOID)hWndParent, (PVOID)hWndChildAfter, lpszClass, lpszWindow);
    log("APICALL: FindWindowExW(0x%p, 0x%p, \"%S\", \"%S\") => 0x%p", hWndParent, hWndChildAfter, PRINT_WSTR(lpszClass), PRINT_WSTR(lpszWindow), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HWND)
hook_FindWindowA(
    LPSTR lpClassName,
    LPSTR lpWindowName
    )
{
    HWND result = 0;

    HOOK_ENTER(FindWindowA);

    result = (HWND)((fun2arg)(Hooks[HOOKID_FindWindowA].orig_addr))(lpClassName, lpWindowName);
    log("APICALL: FindWindowA(\"%s\", \"%s\") => 0x%p", PRINT_STR(lpClassName), PRINT_STR(lpWindowName), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HWND)
hook_FindWindowW(
    LPWSTR lpClassName,
    LPWSTR lpWindowName
    )
{
    HWND result = 0;

    HOOK_ENTER(FindWindowW);

    result = (HWND)((fun2arg)(Hooks[HOOKID_FindWindowW].orig_addr))(lpClassName, lpWindowName);
    log("APICALL: FindWindowW(\"%S\", \"%S\") => 0x%p", PRINT_WSTR(lpClassName), PRINT_WSTR(lpWindowName), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_EnumWindows(
    WNDENUMPROC lpEnumFunc,
    LPARAM lParam
    )
{
    BOOL result = 0;

    HOOK_ENTER(EnumWindows);

    result = (BOOL)((fun2arg)(Hooks[HOOKID_EnumWindows].orig_addr))((PVOID)lpEnumFunc, (PVOID)lParam);
    log("APICALL: EnumWindows(0x%p, 0x%p) => 0x%p", lpEnumFunc, lParam, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_SetDlgItemTextA(
    HWND hDlg,
    int nIDDlgItem,
    LPSTR lpString
    )
{
    BOOL result = 0;

    HOOK_ENTER(SetDlgItemTextA);

    result = (BOOL)((fun3arg)(Hooks[HOOKID_SetDlgItemTextA].orig_addr))((PVOID)hDlg, (PVOID)nIDDlgItem, lpString);
    log("APICALL: SetDlgItemTextA(0x%p, 0x%X, \"%s\") => 0x%p", hDlg, nIDDlgItem, PRINT_STR(lpString), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_SetDlgItemTextW(
    HWND hDlg,
    int nIDDlgItem,
    LPWSTR lpString
    )
{
    BOOL result = 0;

    HOOK_ENTER(SetDlgItemTextW);

    result = (BOOL)((fun3arg)(Hooks[HOOKID_SetDlgItemTextW].orig_addr))((PVOID)hDlg, (PVOID)nIDDlgItem, lpString);
    log("APICALL: SetDlgItemTextW(0x%p, 0x%X, \"%S\") => 0x%p", hDlg, nIDDlgItem, PRINT_WSTR(lpString), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(UINT)
hook_GetDlgItemTextA(
    HWND hDlg,
    int nIDDlgItem,
    LPSTR lpString,
    int cchMax
    )
{
    UINT result = 0;

    HOOK_ENTER(GetDlgItemTextA);

    result = (UINT)((fun4arg)(Hooks[HOOKID_GetDlgItemTextA].orig_addr))((PVOID)hDlg, (PVOID)nIDDlgItem, lpString, (PVOID)cchMax);
    log("APICALL: GetDlgItemTextA(0x%p, 0x%X, \"%s\", 0x%X) => 0x%p", hDlg, nIDDlgItem, PRINT_STR(lpString), cchMax, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(UINT)
hook_GetDlgItemTextW(
    HWND hDlg,
    int nIDDlgItem,
    LPWSTR lpString,
    int cchMax
    )
{
    UINT result = 0;

    HOOK_ENTER(GetDlgItemTextW);

    result = (UINT)((fun4arg)(Hooks[HOOKID_GetDlgItemTextW].orig_addr))((PVOID)hDlg, (PVOID)nIDDlgItem, lpString, (PVOID)cchMax);
    log("APICALL: GetDlgItemTextW(0x%p, 0x%X, \"%S\", 0x%X) => 0x%p", hDlg, nIDDlgItem, PRINT_WSTR(lpString), cchMax, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_HeapFree(
    HANDLE hHeap,
    DWORD dwFlags,
    PVOID lpMem
    )
{
    BOOL result = 0;
    size_t size = HeapSize(hHeap, dwFlags, lpMem);

    if (size >= 128)
    {
        save_binary_buffer("HeapFree", NULL, MWM_NO_TAG, lpMem, (int)size);
    }

    HOOK_ENTER(HeapFree);

    result = (BOOL)((fun3arg)(Hooks[HOOKID_HeapFree].orig_addr))(hHeap, (PVOID)dwFlags, lpMem);
    if (size >= 128)
    {
        log("APICALL: HeapFree(0x%p, 0x%X, 0x%p) => 0x%p (size:%p)", hHeap, dwFlags, lpMem, result, size);
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(int)
hook_WideCharToMultiByte(
    UINT CodePage,
    DWORD dwFlags,
    LPWSTR lpWideCharStr,
    int cchWideChar,
    LPSTR lpMultiByteStr,
    int cbMultiByte,
    LPCH lpDefaultChar,
    LPBOOL lpUsedDefaultChar
    )
{
    int result = 0;

    HOOK_ENTER(WideCharToMultiByte);

    result = (int)((fun8arg)(Hooks[HOOKID_WideCharToMultiByte].orig_addr))((PVOID)CodePage, (PVOID)dwFlags, (PVOID)lpWideCharStr,
            (PVOID)cchWideChar, lpMultiByteStr, (PVOID)cbMultiByte, lpDefaultChar, lpUsedDefaultChar);
    log("APICALL: WideCharToMultiByte(0x%p, 0x%X, \"%S\", 0x%X, 0x%p, 0x%X, 0x%p, 0x%p) => 0x%p", CodePage, dwFlags, PRINT_WSTR(lpWideCharStr),
            cchWideChar, lpMultiByteStr, cbMultiByte, lpDefaultChar, lpUsedDefaultChar, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(int)
hook_MultiByteToWideChar(
    UINT CodePage,
    DWORD dwFlags,
    LPCCH lpMultiByteStr,
    int cbMultiByte,
    LPWSTR lpWideCharStr,
    int cchWideChar
    )
{
    int result = 0;

    HOOK_ENTER(MultiByteToWideChar);

    result = (int)((fun6arg)(Hooks[HOOKID_MultiByteToWideChar].orig_addr))((PVOID)CodePage, (PVOID)dwFlags,
            (PVOID)lpMultiByteStr, (PVOID)cbMultiByte, lpWideCharStr, (PVOID)cchWideChar);
    log("APICALL: MultiByteToWideChar(0x%p, 0x%X, 0x%p, 0x%X, \"%S\", 0x%X) => 0x%p", CodePage, dwFlags, lpMultiByteStr,
            cbMultiByte, PRINT_WSTR(lpWideCharStr), cchWideChar, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HLOCAL)
hook_LocalFree(
    PVOID hMem
    )
{
    HLOCAL result = 0;

    HOOK_ENTER(LocalFree);

    {
        SIZE_T size = LocalSize(hMem);
        if (size)
        {
            save_binary_buffer("LocalFree", NULL, MWM_NO_TAG, hMem, (int)size);
        }
    }

    result = (HLOCAL)((fun1arg)(Hooks[HOOKID_LocalFree].orig_addr))(hMem);
    log("APICALL: LocalFree(0x%p) => 0x%p", hMem, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HGLOBAL)
hook_GlobalFree(
    PVOID hMem
    )
{
    HGLOBAL result = 0;

    HOOK_ENTER(GlobalFree);

    {
        SIZE_T size = GlobalSize(hMem);
        if (size)
        {
            save_binary_buffer("GlobalFree", NULL, MWM_NO_TAG, hMem, (int)size);
        }
    }

    result = (HGLOBAL)((fun1arg)(Hooks[HOOKID_GlobalFree].orig_addr))(hMem);
    log("APICALL: GlobalFree(0x%p) => 0x%p", hMem, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HRSRC)
hook_FindResourceA(
    HMODULE hModule,
    LPSTR lpName,
    LPSTR lpType
    )
{
    HRSRC result = 0;

    HOOK_ENTER(FindResourceA);

    result = (HRSRC)((fun3arg)(Hooks[HOOKID_FindResourceA].orig_addr))((PVOID)hModule, lpName, lpType);
    log("APICALL: FindResourceA(0x%p, \"%s\", \"%s\") => 0x%p", hModule, PRINT_STR(lpName), PRINT_STR(lpType), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HRSRC)
hook_FindResourceW(
    HMODULE hModule,
    LPWSTR lpName,
    LPWSTR lpType
    )
{
    HRSRC result = 0;

    HOOK_ENTER(FindResourceW);

    result = (HRSRC)((fun3arg)(Hooks[HOOKID_FindResourceW].orig_addr))((PVOID)hModule, lpName, lpType);
    log("APICALL: FindResourceW(0x%p, \"%S\", \"%S\") => 0x%p", hModule, PRINT_WSTR(lpName), PRINT_WSTR(lpType), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HRSRC)
hook_FindResourceExA(
    HMODULE hModule,
    LPSTR lpType,
    LPSTR lpName,
    WORD wLanguage
    )
{
    HRSRC result = 0;

    HOOK_ENTER(FindResourceExA);

    result = (HRSRC)((fun4arg)(Hooks[HOOKID_FindResourceExA].orig_addr))((PVOID)hModule, lpType, lpName, (PVOID)wLanguage);
    log("APICALL: FindResourceExA(0x%p, \"%s\", \"%s\", 0x%p) => 0x%p", hModule, PRINT_STR(lpType), PRINT_STR(lpName), wLanguage, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HRSRC)
hook_FindResourceExW(
    HMODULE hModule,
    LPWSTR lpType,
    LPWSTR lpName,
    WORD wLanguage
    )
{
    HRSRC result = 0;

    HOOK_ENTER(FindResourceExW);

    result = (HRSRC)((fun4arg)(Hooks[HOOKID_FindResourceExW].orig_addr))((PVOID)hModule, lpType, lpName, (PVOID)wLanguage);
    log("APICALL: FindResourceExW(0x%p, \"%S\", \"%S\", 0x%p) => 0x%p", hModule, PRINT_WSTR(lpType), PRINT_WSTR(lpName), wLanguage, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HMODULE)
hook_GetModuleHandleA(
    LPSTR lpModuleName
    )
{
    HMODULE result = 0;

    HOOK_ENTER(GetModuleHandleA);

    result = (HMODULE)((fun1arg)(Hooks[HOOKID_GetModuleHandleA].orig_addr))(lpModuleName);
    log("APICALL: GetModuleHandleA(\"%s\") => 0x%p", PRINT_STR(lpModuleName), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(HMODULE)
hook_GetModuleHandleW(
    LPWSTR lpModuleName
    )
{
    HMODULE result = 0;

    HOOK_ENTER(GetModuleHandleW);

    result = (HMODULE)((fun1arg)(Hooks[HOOKID_GetModuleHandleW].orig_addr))(lpModuleName);
    log("APICALL: GetModuleHandleW(\"%S\") => 0x%p", PRINT_WSTR(lpModuleName), result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(DWORD)
hook_GetModuleFileNameA(
    HMODULE hModule,
    LPSTR lpFilename,
    DWORD nSize
    )
{
    DWORD result = 0;

    HOOK_ENTER(GetModuleFileNameA);

    result = (DWORD)((fun3arg)(Hooks[HOOKID_GetModuleFileNameA].orig_addr))((PVOID)hModule, lpFilename, (PVOID)nSize);
    log("APICALL: GetModuleFileNameA(0x%p, \"%s\", 0x%X) => 0x%p", hModule, PRINT_STR(lpFilename), nSize, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(DWORD)
hook_GetModuleFileNameW(
    HMODULE hModule,
    LPWSTR lpFilename,
    DWORD nSize
    )
{
    DWORD result = 0;

    HOOK_ENTER(GetModuleFileNameW);

    result = (DWORD)((fun3arg)(Hooks[HOOKID_GetModuleFileNameW].orig_addr))((PVOID)hModule, lpFilename, (PVOID)nSize);
    log("APICALL: GetModuleFileNameW(0x%p, \"%S\", 0x%X) => 0x%p", hModule, PRINT_WSTR(lpFilename), nSize, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(RPC_STATUS)
hook_RpcStringBindingComposeA(
    LPSTR ObjUuid,
    LPSTR ProtSeq,
    LPSTR NetworkAddr,
    LPSTR Endpoint,
    LPSTR Options,
    LPSTR *StringBinding
    )
{
    RPC_STATUS result = 0;

    HOOK_ENTER(RpcStringBindingComposeA);

    result = (RPC_STATUS)((fun6arg)(Hooks[HOOKID_RpcStringBindingComposeA].orig_addr))(ObjUuid, ProtSeq, NetworkAddr,
            Endpoint, Options, StringBinding);
    log("APICALL: RpcStringBindingComposeA(\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", 0x%p) => 0x%p", PRINT_STR(ObjUuid),
            PRINT_STR(ProtSeq), PRINT_STR(NetworkAddr), PRINT_STR(Endpoint), PRINT_STR(Options), StringBinding, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(RPC_STATUS)
hook_RpcStringBindingComposeW(
    LPWSTR ObjUuid,
    LPWSTR ProtSeq,
    LPWSTR NetworkAddr,
    LPWSTR Endpoint,
    LPWSTR Options,
    LPWSTR *StringBinding
    )
{
    RPC_STATUS result = 0;

    HOOK_ENTER(RpcStringBindingComposeW);

    result = (RPC_STATUS)((fun6arg)(Hooks[HOOKID_RpcStringBindingComposeW].orig_addr))(ObjUuid, ProtSeq, NetworkAddr,
            Endpoint, Options, StringBinding);
    log("APICALL: RpcStringBindingComposeW(\"%S\", \"%S\", \"%S\", \"%S\", \"%S\", 0x%p) => 0x%p", PRINT_WSTR(ObjUuid),
            PRINT_WSTR(ProtSeq), PRINT_WSTR(NetworkAddr), PRINT_WSTR(Endpoint), PRINT_WSTR(Options), StringBinding, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_EqualSid(
    PSID pSid1,
    PSID pSid2
    )
{
    BOOL result = 0;

    HOOK_ENTER(EqualSid);

    result = (BOOL)((fun2arg)(Hooks[HOOKID_EqualSid].orig_addr))(pSid1, pSid2);
    {
        LPSTR sid1 = NULL, sid2 = NULL;

        ConvertSidToStringSidA(pSid1, &sid1);
        ConvertSidToStringSidA(pSid2, &sid2);

        log("APICALL: EqualSid(0x%p <%s>, 0x%p <%s>) => 0x%p", pSid1, PRINT_STR(sid1), pSid2, PRINT_STR(sid2), result);

        if (sid1)
        {
            LocalFree(sid1);
        }
        if (sid2)
        {
            LocalFree(sid2);
        }
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_GetTokenInformation(
    HANDLE TokenHandle,
    TOKEN_INFORMATION_CLASS TokenInformationClass,
    PVOID TokenInformation,
    DWORD TokenInformationLength,
    PDWORD ReturnLength
    )
{
    BOOL result = 0;

    HOOK_ENTER(GetTokenInformation);

    result = (BOOL)((fun5arg)(Hooks[HOOKID_GetTokenInformation].orig_addr))(TokenHandle, (PVOID)TokenInformationClass, TokenInformation, (PVOID)TokenInformationLength, ReturnLength);
    log("APICALL: GetTokenInformation(0x%p, 0x%p, 0x%p, 0x%X, 0x%p) => 0x%p", TokenHandle, TokenInformationClass, TokenInformation, TokenInformationLength, ReturnLength, result);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_CheckTokenMembership(
    HANDLE TokenHandle,
    PSID SidToCheck,
    PBOOL IsMember
    )
{
    BOOL result = 0;

    HOOK_ENTER(CheckTokenMembership);

    result = (BOOL)((fun3arg)(Hooks[HOOKID_CheckTokenMembership].orig_addr))(TokenHandle, SidToCheck, IsMember);
    {
        LPSTR sid = NULL;

        ConvertSidToStringSidA(SidToCheck, &sid);

        log("APICALL: CheckTokenMembership(0x%p, 0x%p <%s>, 0x%p) => 0x%p", TokenHandle, SidToCheck, PRINT_STR(sid),
                IsMember, result);

        if (sid)
        {
            LocalFree(sid);
        }
    }

    HOOK_LEAVE();

    return result;
}


HOOKFUN(BOOL)
hook_OpenProcessToken(
    HANDLE ProcessHandle,
    DWORD DesiredAccess,
    PHANDLE TokenHandle
    )
{
    BOOL result = 0;

    HOOK_ENTER(OpenProcessToken);

    result = (BOOL)((fun3arg)(Hooks[HOOKID_OpenProcessToken].orig_addr))(ProcessHandle, (PVOID)DesiredAccess, TokenHandle);
    log("APICALL: OpenProcessToken(0x%p, 0x%X, 0x%p)", ProcessHandle, DesiredAccess, TokenHandle);

    HOOK_LEAVE();

    return result;
}


HOOKFUN(PWSTR)
hook_StrStrIW(
    LPWSTR pszFirst,
    LPWSTR pszSrch
    )
{
    PWSTR result = 0;

    HOOK_ENTER(StrStrIW);

    result = (PWSTR)((fun2arg)(Hooks[HOOKID_StrStrIW].orig_addr))(pszFirst, pszSrch);
    log("APICALL: StrStrIW(\"%S\", \"%S\") => 0x%p", PRINT_WSTR(pszFirst), PRINT_WSTR(pszSrch), result);

    HOOK_LEAVE();

    return result;
}


// todo: extract this automatically
PCHAR HookedDlls[] = {
    "advapi32.dll",
    "bcrypt.dll",
    "crypt32.dll",
    "dpapi.dll",
    "kernel32.dll",
    "ncrypt.dll",
    "ntdll.dll",
    "rpcrt4.dll",
    "shell32.dll",
    "shlwapi.dll",
    "urlmon.dll",
    "user32.dll",
    "winhttp.dll",
    "wininet.dll",
    "ws2_32.dll",
    //"api-ms-win-core-processthreads-l1-1-0.dll",
    NULL
};
DWORD HookedDllCount = (sizeof(HookedDlls) / sizeof(HookedDlls[0])) - 1;


__declspec(dllexport)
HOOK Hooks[] = {
    // ntdll.dll
    {hook_LdrGetProcedureAddress, "ntdll.dll", "LdrGetProcedureAddress"},
    {hook_LdrGetProcedureAddressForCaller, "ntdll.dll", "LdrGetProcedureAddressForCaller"},
    {hook_LdrLoadDll, "ntdll.dll", "LdrLoadDll"},
    {hook_NtOpenFile, "ntdll.dll", "NtOpenFile"},
    {hook_NtOpenKey, "ntdll.dll", "NtOpenKey"},
    {hook_NtQueryValueKey, "ntdll.dll", "NtQueryValueKey"},
    {hook_RtlInitUnicodeString, "ntdll.dll", "RtlInitUnicodeString"},
    // kernel32.dll
//    {hook_CloseHandle, "kernel32.dll", "CloseHandle"},
    {hook_CopyFileA, "kernel32.dll", "CopyFileA"},
    {hook_CopyFileExA, "kernel32.dll", "CopyFileExA"},
    {hook_CopyFileExW, "kernel32.dll", "CopyFileExW"},
    {hook_CopyFileW, "kernel32.dll", "CopyFileW"},
    {hook_CreateFileA, "kernel32.dll", "CreateFileA"},
    {hook_CreateFileW, "kernel32.dll", "CreateFileW"},
    {hook_CreateProcessA, "kernel32.dll", "CreateProcessA"},
    {hook_CreateProcessInternalW, "kernel32.dll", "CreateProcessInternalW"},
    {hook_CreateProcessW, "kernel32.dll", "CreateProcessW"},
    {hook_CreateRemoteThread, "kernel32.dll", "CreateRemoteThread"},
    {hook_DeviceIoControl, "kernel32.dll", "DeviceIoControl"},
    {hook_FindFirstFileA, "kernel32.dll", "FindFirstFileA"},
    {hook_FindFirstFileExA, "kernel32.dll", "FindFirstFileExA"},
    {hook_FindFirstFileExW, "kernel32.dll", "FindFirstFileExW"},
    {hook_FindFirstFileW, "kernel32.dll", "FindFirstFileW"},
    {hook_FindResourceA, "kernel32.dll", "FindResourceA"},
    {hook_FindResourceExA, "kernel32.dll", "FindResourceExA"},
    {hook_FindResourceExW, "kernel32.dll", "FindResourceExW"},
    {hook_FindResourceW, "kernel32.dll", "FindResourceW"},
    {hook_FlsAlloc, "kernel32.dll", "FlsAlloc"},
    {hook_GetFileAttributesA, "kernel32.dll", "GetFileAttributesA"},
    {hook_GetFileAttributesExA, "kernel32.dll", "GetFileAttributesExA"},
    {hook_GetFileAttributesExW, "kernel32.dll", "GetFileAttributesExW"},
    {hook_GetFileAttributesW, "kernel32.dll", "GetFileAttributesW"},
    {hook_GetModuleFileNameA, "kernel32.dll", "GetModuleFileNameA"},
    {hook_GetModuleFileNameW, "kernel32.dll", "GetModuleFileNameW"},
    {hook_GetModuleHandleA, "kernel32.dll", "GetModuleHandleA"},
    {hook_GetModuleHandleW, "kernel32.dll", "GetModuleHandleW"},
    {hook_GetProcAddress, "kernel32.dll", "GetProcAddress"},
    {hook_GetStdHandle, "kernel32.dll", "GetStdHandle"},
    {hook_GlobalFree, "kernel32.dll", "GlobalFree"},
    {hook_HeapFree, "kernel32.dll", "HeapFree"},
    {hook_IsProcessorFeaturePresent, "kernel32.dll", "IsProcessorFeaturePresent"},
    {hook_LoadLibraryA, "kernel32.dll", "LoadLibraryA"},
    {hook_LoadLibraryExA, "kernel32.dll", "LoadLibraryExA"},
    {hook_LoadLibraryExW, "kernel32.dll", "LoadLibraryExW"},
    {hook_LoadLibraryW, "kernel32.dll", "LoadLibraryW"},
    {hook_LocalFree, "kernel32.dll", "LocalFree"},
    {hook_MoveFileA, "kernel32.dll", "MoveFileA"},
    {hook_MoveFileW, "kernel32.dll", "MoveFileW"},
    {hook_MultiByteToWideChar, "kernel32.dll", "MultiByteToWideChar"},
    {hook_ReadProcessMemory, "kernel32.dll", "ReadProcessMemory"},
    {hook_SetFileAttributesA, "kernel32.dll", "SetFileAttributesA"},
    {hook_SetFileAttributesW, "kernel32.dll", "SetFileAttributesW"},
    {hook_Sleep, "kernel32.dll", "Sleep"},
    {hook_VirtualAlloc, "kernel32.dll", "VirtualAlloc"},
    {hook_VirtualFree, "kernel32.dll", "VirtualFree"},
    {hook_VirtualProtectEx, "kernel32.dll", "VirtualProtectEx"},
    {hook_WideCharToMultiByte, "kernel32.dll", "WideCharToMultiByte"},
    {hook_WriteFile, "kernel32.dll", "WriteFile"},
    {hook_WriteProcessMemory, "kernel32.dll", "WriteProcessMemory"},
    // advapi32.dll
    {hook_CheckTokenMembership, "advapi32.dll", "CheckTokenMembership"},
    {hook_CryptDecrypt, "advapi32.dll", "CryptDecrypt"},
    {hook_CryptDestroyKey, "advapi32.dll", "CryptDestroyKey"},
    {hook_CryptEncrypt, "advapi32.dll", "CryptEncrypt"},
    {hook_CryptGenKey, "advapi32.dll", "CryptGenKey"},
    {hook_CryptHashData, "advapi32.dll", "CryptHashData"},
    {hook_CryptImportKey, "advapi32.dll", "CryptImportKey"},
    {hook_EqualSid, "advapi32.dll", "EqualSid"},
    {hook_GetTokenInformation, "advapi32.dll", "GetTokenInformation"},
    {hook_OpenProcessToken, "advapi32.dll", "OpenProcessToken"},
    // dpapi.dll
    {hook_CryptProtectMemory, "dpapi.dll", "CryptProtectMemory"},
    {hook_CryptUnprotectMemory, "dpapi.dll", "CryptUnprotectMemory"},
    // crypt32.dll
    {hook_CertGetNameStringA, "crypt32.dll", "CertGetNameStringA"},
    {hook_CryptBinaryToStringA, "crypt32.dll", "CryptBinaryToStringA"},
    {hook_CryptBinaryToStringW, "crypt32.dll", "CryptBinaryToStringW"},
    {hook_CryptDecodeObject, "crypt32.dll", "CryptDecodeObject"},
    {hook_CryptDecodeObjectEx, "crypt32.dll", "CryptDecodeObjectEx"},
    {hook_CryptEncodeObject, "crypt32.dll", "CryptEncodeObject"},
    {hook_CryptEncodeObjectEx, "crypt32.dll", "CryptEncodeObjectEx"},
    {hook_CryptMsgOpenToDecode, "crypt32.dll", "CryptMsgOpenToDecode"},
    {hook_CryptMsgOpenToEncode, "crypt32.dll", "CryptMsgOpenToEncode"},
    {hook_CryptMsgUpdate, "crypt32.dll", "CryptMsgUpdate"},
    {hook_CryptProtectData, "crypt32.dll", "CryptProtectData"},
    {hook_CryptStringToBinaryA, "crypt32.dll", "CryptStringToBinaryA"},
    {hook_CryptStringToBinaryW, "crypt32.dll", "CryptStringToBinaryW"},
    {hook_CryptUnprotectData, "crypt32.dll", "CryptUnprotectData"},
    // ncrypt.dll
    {hook_NCryptProtectSecret, "ncrypt.dll", "NCryptProtectSecret"},
    {hook_NCryptUnprotectSecret, "ncrypt.dll", "NCryptUnprotectSecret"},
    // shell32.dll
    {hook_ShellExecuteA, "shell32.dll", "ShellExecuteA"},
    {hook_ShellExecuteW, "shell32.dll", "ShellExecuteW"},
    {hook_ShellExecuteExA, "shell32.dll", "ShellExecuteExA"},
    {hook_ShellExecuteExW, "shell32.dll", "ShellExecuteExW"},
    // ws2_32.dll
    {hook_connect, "ws2_32.dll", "connect"},
    {hook_recv, "ws2_32.dll", "recv"},
    {hook_send, "ws2_32.dll", "send"},
    {hook_sendto, "ws2_32.dll", "sendto"},
    // urlmon.dll
    {hook_URLDownloadToFileA, "Urlmon.dll", "URLDownloadToFileA"},
    {hook_URLDownloadToFileW, "Urlmon.dll", "URLDownloadToFileW"},
    // bcrypt.dll
    {hook_BCryptDecrypt, "Bcrypt.dll", "BCryptDecrypt"},
    {hook_BCryptEncrypt, "Bcrypt.dll", "BCryptEncrypt"},
    // winhttp.dll
    {hook_WinHttpConnect, "Winhttp.dll", "WinHttpConnect"},
    {hook_WinHttpOpen, "Winhttp.dll", "WinHttpOpen"},
    {hook_WinHttpOpenRequest, "Winhttp.dll", "WinHttpOpenRequest"},
    {hook_WinHttpQueryHeaders, "Winhttp.dll", "WinHttpQueryHeaders"},
    {hook_WinHttpReadData, "Winhttp.dll", "WinHttpReadData"},
    {hook_WinHttpSendRequest, "Winhttp.dll", "WinHttpSendRequest"},
    {hook_WinHttpSetCredentials, "Winhttp.dll", "WinHttpSetCredentials"},
    {hook_WinHttpWriteData, "Winhttp.dll", "WinHttpWriteData"},
    // wininet.dll
    {hook_FtpOpenFileA, "wininet.dll", "FtpOpenFileA"},
    {hook_FtpOpenFileW, "wininet.dll", "FtpOpenFileW"},
    {hook_HttpOpenRequestA, "wininet.dll", "HttpOpenRequestA"},
    {hook_HttpOpenRequestW, "wininet.dll", "HttpOpenRequestW"},
    {hook_InternetConnectA, "wininet.dll", "InternetConnectA"},
    {hook_InternetConnectW, "wininet.dll", "InternetConnectW"},
    {hook_InternetOpenA, "wininet.dll", "InternetOpenA"},
    {hook_InternetOpenUrlA, "wininet.dll", "InternetOpenUrlA"},
    {hook_InternetOpenUrlW, "wininet.dll", "InternetOpenUrlW"},
    {hook_InternetOpenW, "wininet.dll", "InternetOpenW"},
    {hook_InternetReadFile, "wininet.dll", "InternetReadFile"},
    // user32.dll
    {hook_EnumWindows, "user32.dll", "EnumWindows"},
    {hook_FindWindowA, "user32.dll", "FindWindowA"},
    {hook_FindWindowExA, "user32.dll", "FindWindowExA"},
    {hook_FindWindowExW, "user32.dll", "FindWindowExW"},
    {hook_FindWindowW, "user32.dll", "FindWindowW"},
    {hook_GetDlgItemTextA, "user32.dll", "GetDlgItemTextA"},
    {hook_GetDlgItemTextW, "user32.dll", "GetDlgItemTextW"},
    {hook_SetDlgItemTextA, "user32.dll", "SetDlgItemTextA"},
    {hook_SetDlgItemTextW, "user32.dll", "SetDlgItemTextW"},
    // rpcrt4.dll
    {hook_RpcStringBindingComposeA, "rpcrt4.dll", "RpcStringBindingComposeA"},
    {hook_RpcStringBindingComposeW, "rpcrt4.dll", "RpcStringBindingComposeW"},
    // shlwapi.dll
    {hook_StrStrIW, "shlwapi.dll", "StrStrIW"}
};
DWORD HookCount = (sizeof(Hooks) / sizeof(Hooks[0]));


FORWARDER_HOOK ForwarderHooks[] = {
//    {"api-ms-win-core-handle-l1", "CloseHandle", HOOKID_CloseHandle, 0},
    {"api-ms-win-core-kernel32-legacy-l1", "LoadLibraryA", HOOKID_LoadLibraryA, 0},
    {"api-ms-win-core-kernel32-legacy-l1", "LoadLibraryW", HOOKID_LoadLibraryW, 0}
};
DWORD ForwarderHookCount = (sizeof(ForwarderHooks) / sizeof(ForwarderHooks[0]));
