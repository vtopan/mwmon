/**
 * mwmon/mwmondrv.c: Minifilter which injects the LoadLibrary(mwmonhk*.dll) into new processes.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#include "mwmondrv.h"


CONST FLT_OPERATION_REGISTRATION Callbacks[] = {
    // {IRP_MJ_CREATE, 0, PreOperation, PostOperation, NULL},   // todo
    {IRP_MJ_OPERATION_END}
};

CONST FLT_REGISTRATION FilterRegistration = {
    sizeof(FLT_REGISTRATION),           //  Size
    FLT_REGISTRATION_VERSION,           //  Version
    0,                                  //  Flags
    NULL,                               //  Context
    Callbacks,                          //  Operation callbacks
    FilterUnloadCallback,               //  PFLT_FILTER_UNLOAD_CALLBACK
    NULL,                               //  InstanceSetup
    NULL,                               //  InstanceQueryTeardown
    NULL,                               //  InstanceTeardownStart
    NULL,                               //  InstanceTeardownComplete
    NULL,                               //  GenerateFileName
    NULL,                               //  GenerateDestinationFileName
    NULL                                //  NormalizeNameComponent
};

G_MWMON G = {0};


NTSTATUS
DriverEntry(
    IN PDRIVER_OBJECT DriverObject,
    IN PUNICODE_STRING RegistryPath
    )
{
    NTSTATUS status = STATUS_SUCCESS;
    PSECURITY_DESCRIPTOR secDesc = NULL;
    OBJECT_ATTRIBUTES objAttr;
    UNICODE_STRING portName;

    UNREFERENCED_PARAMETER(RegistryPath);

    MWM_DBG("MwMon " MWMVER ": DriverEntry() started...");

    CHECK_CALL_STATUS(FltRegisterFilter(DriverObject, &FilterRegistration, &G.Filter));

    CHECK_CALL_STATUS(PsSetLoadImageNotifyRoutine(ImageLoaded));
    G.ImgLoadNotifyEnabled = TRUE;

    RtlInitUnicodeString(&portName, COMM_PORT_NAME);
    CHECK_CALL_STATUS(FltBuildDefaultSecurityDescriptor(&secDesc, FLT_PORT_ALL_ACCESS));
    InitializeObjectAttributes(&objAttr, &portName, OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, NULL, secDesc);
    CHECK_CALL_STATUS(FltCreateCommunicationPort(G.Filter, &G.CommPort, &objAttr, NULL,
            ClientConnectedCallback, ClientDisconnectedCallback, MessageCallback, 1));

done:
    if (secDesc)
    {
        FltFreeSecurityDescriptor(secDesc);
    }
    if (!NT_SUCCESS(status))
    {
        if (G.Filter)
        {
            FltUnregisterFilter(G.Filter);
        }
    }

    MWM_DBG("DriverEntry() completed.");

    return status;
}


NTSTATUS
FilterUnloadCallback(
    IN FLT_FILTER_UNLOAD_FLAGS Flags
    )
{
    UNREFERENCED_PARAMETER(Flags);

    MWM_DBG("FilterUnloadCallback() started...");

    if (G.ImgLoadNotifyEnabled)
    {
        PsRemoveLoadImageNotifyRoutine(ImageLoaded);
        G.ImgLoadNotifyEnabled = FALSE;
    }

    if (G.CommPort)
    {
        FltCloseCommunicationPort(G.CommPort);
    }
    if (G.ClientPort)
    {
        FltCloseClientPort(G.Filter, &G.ClientPort);
    }
    FltUnregisterFilter(G.Filter);

    MWM_DBG("MwMon " MWMVER ": FilterUnloadCallback() completed.");

    return STATUS_SUCCESS;
}


/**
 * PsRemoveLoadImageNotifyRoutine callback.
 */
void
ImageLoaded(
    IN PUNICODE_STRING FullImageName,
    IN HANDLE ProcessId,
    IN PIMAGE_INFO ImageInfo
    )
{
    NTSTATUS status = STATUS_UNSUCCESSFUL;
    PEPROCESS crtProcess = PsGetCurrentProcess();
    PPEB peb32 = PsGetProcessWow64Process(crtProcess);
    BOOLEAN is32 = MwmPeImageIs32Bit(ImageInfo->ImageBase);

    UNREFERENCED_PARAMETER(ImageInfo);

    MWM_DBG("<%s:%d> Image %wZ (%s) loaded @ 0x%p (sign: %s/%s)",
            PsGetProcessImageFileName(crtProcess), ProcessId, FullImageName,
            is32 ? "32" : "64", ImageInfo->ImageBase,
            STR_SE_IMAGE_SIGNATURE_TYPE[ImageInfo->ImageSignatureType],
            STR_SE_SIGNING_LEVEL[ImageInfo->ImageSignatureLevel]);

    if (ProcessId && (is32 || !peb32))      // ignore 64-bit DLLs loaded in a 32-bit process (WoW64)
    {
        UNICODE_STRING suffix;
        PVOID* AddrLdrLoadDll = NULL;

        RtlInitUnicodeString(&suffix, L"\\ntdll.dll");
        if (RtlSuffixUnicodeString(&suffix, FullImageName, TRUE))
        {
            PVOID *addr = (is32 ? &G.AddrNtdll32 : &G.AddrNtdll64);
            *addr = ImageInfo->ImageBase;
            AddrLdrLoadDll = is32 ? &G.AddrLdrLoadDll32 : &G.AddrLdrLoadDll64;
            if (!(*AddrLdrLoadDll))
            {
                CHECK_CALL_STATUS(MwmFindPeExport(ImageInfo->ImageBase, "LdrLoadDll", NULL, NULL, AddrLdrLoadDll));
                MWM_DBG("Resolved LdrLoadDll/%s @ 0x%p", is32 ? "32" : "64", *AddrLdrLoadDll);
            }
        }

        RtlInitUnicodeString(&suffix, L"kernel32.dll"); // alternatives: KernelBase kernel32 ntdllB
        if (RtlSuffixUnicodeString(&suffix, FullImageName, TRUE))
        {
            PUCHAR processName = PsGetProcessImageFileName(crtProcess);

            if (
                    // fixme: better whitelisting
                    strcmp(processName, "conhost.exe")
                    && strcmp(processName, "cmd.exe")
                    && strcmp(processName, "x96dbg.exe")
                    && strcmp(processName, "x32dbg.exe")
                    && strcmp(processName, "x64dbg.exe")
                    && strcmp(processName, "sc.exe")
                    && strcmp(processName, "fltMC.exe")
                    && strcmp(processName, "windbg.exe")
                    && strcmp(processName, "xcopy.exe")
                    && strcmp(processName, "WerFault.exe")
                    && strcmp(processName, "mscorsvw.exe")
                    && strcmp(processName, "consent.exe")
                )
            {
                MWM_DBG("Injecting %s (ntdll@0x%p) into %s:%d...", is32 ? MWMON_DLL_NAME_32 : MWMON_DLL_NAME_64,
                        is32 ? G.AddrNtdll32 : G.AddrNtdll64, processName, ProcessId);
                CHECK_CALL_STATUS(MwmInjectMwmonDllInCurrentProcess(is32,
                        is32 ? G.AddrNtdll32 : G.AddrNtdll64,
                        is32 ? G.AddrLdrLoadDll32 : G.AddrLdrLoadDll64));
            }
        }
    }

done:;
}


NTSTATUS
ClientConnectedCallback(
    IN PFLT_PORT ClientPort,
    IN PVOID ServerPortCookie,
    IN PVOID ConnectionContext,
    IN ULONG SizeOfContext,
    OUT PVOID *ConnectionPortCookie
    )
{
    UNREFERENCED_PARAMETER(ServerPortCookie);
    UNREFERENCED_PARAMETER(ConnectionContext);
    UNREFERENCED_PARAMETER(SizeOfContext);
    UNREFERENCED_PARAMETER(ConnectionPortCookie);

    if (!InterlockedCompareExchangePointer((volatile PVOID*)&G.ClientPort, ClientPort, NULL))
    {
        MWM_DBG("Client connected: 0x%p", ClientPort);
    }
    else
    {
        MWM_DBG("Client cannot connect (connection already open): 0x%p", ClientPort);
        FltCloseClientPort(G.Filter, &ClientPort);
        return STATUS_INSUFFICIENT_RESOURCES;
    }

    return STATUS_SUCCESS;
}


void
ClientDisconnectedCallback(
    IN PVOID Cookie
    )
{
    UNREFERENCED_PARAMETER(Cookie);

    MWM_DBG("Client disconnecting: 0x%p", G.ClientPort);

    FltCloseClientPort(G.Filter, &G.ClientPort);
    G.ClientPort = NULL;
}


/**
 * UM-KM communication (placeholder - not currently used).
 */
NTSTATUS
MessageCallback(
    IN PVOID PortCookie,
    IN PVOID InputBuffer,
    IN ULONG InputBufferLength,
    OUT PVOID OutputBuffer OPTIONAL,
    IN ULONG OutputBufferLength,
    OUT PULONG ReturnOutputBufferLength
    )
{
    UNREFERENCED_PARAMETER(OutputBuffer);
    UNREFERENCED_PARAMETER(OutputBufferLength);
    UNREFERENCED_PARAMETER(ReturnOutputBufferLength);
    UNREFERENCED_PARAMETER(PortCookie);

    PMWM_MESSAGE msg = (PMWM_MESSAGE)InputBuffer;

    *ReturnOutputBufferLength = 0;

    if ((InputBufferLength < sizeof(MWM_MESSAGE)) || InputBufferLength < MWM_MESSAGE_SIZE(msg))
    {
        MWM_DBG("Invalid message received (too small): %d\n", InputBufferLength);
        return STATUS_BUFFER_TOO_SMALL;
    }

    MWM_DBG("Got message %d (%d bytes)", msg->Type, InputBufferLength);

    switch (msg->Type)
    {
        case MwmMtPlaceholder:
            break;
        default:
            return STATUS_INVALID_PARAMETER;
    }

    return STATUS_SUCCESS;
}
