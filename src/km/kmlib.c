/**
 * mwmon/kmlib.c: Various kernel-mode utility functions.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */
#include "kmlib.h"


PCHAR STR_SE_IMAGE_SIGNATURE_TYPE[] = {
    "None",
    "Embedded",
    "Cache",
    "CatalogCached",
    "CatalogNotCached",
    "CatalogHint",
    "PackageCatalog",
    "INVALID"
    };

PCHAR STR_SE_SIGNING_LEVEL[] = {
    "UNCHECKED",
    "UNSIGNED",
    "ENTERPRISE",
    "DEVELOPER",
    "AUTHENTICODE",
    "CUSTOM_2",
    "STORE",
    "ANTIMALWARE",
    "MICROSOFT",
    "CUSTOM_4",
    "CUSTOM_5",
    "DYNAMIC_CODEGEN",
    "WINDOWS",
    "CUSTOM_7",
    "WINDOWS_TCB",
    "CUSTOM_6",
    };


/**
 * Retrieves the address of the system routine given by name.
 *
 * Example:
 *      typedef NTSYSAPI PPEB(NTAPI *Fun_PsGetProcessWow64Process)(PEPROCESS Process);
 *      ...
 *      Fun_PsGetProcessWow64Process PsGetProcessWow64Process;
 *      ...
 *      CHECK_CALL_STATUS(MwmResolveDynamicImport((PVOID*)&PsGetProcessWow64Process,
 *          L"PsGetProcessWow64Process"));
 */
NTSTATUS
MwmResolveDynamicImport(
    OUT PVOID *Import,
    IN PWSTR Name
    )
{
    UNICODE_STRING usName;

    RtlInitUnicodeString(&usName, Name);
    *Import = MmGetSystemRoutineAddress(&usName);

    return (*Import) ? STATUS_SUCCESS : STATUS_NOT_FOUND;
}


// Shellcode layout:
// - size: 2K
#define SHLCD_SIZE 2048
// - code @ offset 0
// - original return address @ offset 196..199 (x86-only)
#define SHLCD_OFFS_ORIG_RET_ADDR 196
// - UNICODE_STRING of DLL name @ offset 200..207/215
#define SHLCD_OFFS_US_DLL_NAME 200
// - LLD arg HMODULE @ 216..219 (returned by LdrLoadDll)
#define SHLCD_OFFS_ARG_HMODULE 216
// - WCHAR DLL name (40 WCHARs) @ offset 220..299
#define SHLCD_OFFS_W_DLL_NAME 220
// - saved stolen bytes from LdrLoadDll (16 bytes) @ offset 300..315 (x64-only)
#define SHLCD_OFFS_STOLEN_LLD_BYTES 300
// - WCHAR path @ 320..839 (MAX_PATH WCHAR chars = 520 bytes)
#define SHLCD_OFFS_W_MWMON_PATH 320

/**
 * Creates the shellcode used by MwmInjectMwmonDllInCurrentProcess().
 */
NTSTATUS
_MwmPrepareShellcode(
    IN PVOID AddrLdrLoadDll,
    BOOLEAN Is32,
    OUT PVOID *Shellcode,
    OUT SIZE_T *ShellcodeSize
    )
{
    NTSTATUS status = STATUS_SUCCESS;
    PCHAR shellcode = NULL;
    SIZE_T shellcodeSize = SHLCD_SIZE;
    CHAR* asciiDllName = Is32 ? MWMON_DLL_NAME_32 : MWMON_DLL_NAME_64;
    WCHAR* wcharDllName = NULL;
    PUNICODE_STRING usDllName;
    WCHAR* wcharPath = NULL;
    DWORD* hModule = NULL;
    DWORD offset = 0;

    CHECK_CALL_STATUS(ZwAllocateVirtualMemory(ZwCurrentProcess(), (PVOID*)&shellcode, (ULONG_PTR)NULL,
            &shellcodeSize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE));

    // create shellcode data
    usDllName = (PUNICODE_STRING)&shellcode[SHLCD_OFFS_US_DLL_NAME];
    hModule = (DWORD*)&shellcode[SHLCD_OFFS_ARG_HMODULE];
    wcharDllName = (WCHAR*)&shellcode[SHLCD_OFFS_W_DLL_NAME];
    wcharPath = (WCHAR*)&shellcode[SHLCD_OFFS_W_MWMON_PATH];

    CHECK_CALL_STATUS(MwmAsciiToWchar(MWMON_REL_PATH, 260, wcharPath));
    CHECK_CALL_STATUS(MwmAsciiToWchar(asciiDllName, 40, wcharDllName));
    if (Is32)
    {
        PUNICODE_STRING32 usDllName32 = (PUNICODE_STRING32)usDllName;
        // can't use RtlInitUnicodeString - 32 vs. 64 bits UNICODE_STRING
        usDllName32->Length = (USHORT)wcslen(wcharDllName) * 2;
        usDllName32->MaximumLength = usDllName32->Length;
        usDllName32->Buffer = (ULONG)(ULONG_PTR)wcharDllName;
    }
    else
    {
        RtlInitUnicodeString(usDllName, wcharDllName);
    }

    // note: LdrLoadDll changed prototype at some point, Flags is the first parameter now

    if (Is32)
    {
        SHELL_BYTE(shellcode, 0x90, offset);                // nop - placeholder for an int3 (0xCC)

        // save original return address
        SHELL_BYTE(shellcode, 0x58, offset);                // pop EAX
        SHELL_BYTE(shellcode, 0xA3, offset);                // mov [mem], EAX
        SHELL_DWORD(shellcode, shellcode + SHLCD_OFFS_ORIG_RET_ADDR, offset);  // original ret.addr. location

        // remove patch in LdrLoadDll (restore standard prologue)
        SHELL_BYTE(shellcode, 0xB8, offset);                // mov EAX, <dword>
        SHELL_DWORD(shellcode, AddrLdrLoadDll, offset);     // EAX := &LdrLoadDll
        SHELL_DWORD(shellcode, 0x408B00C6, offset);         // mov byte [EAX], 0x8B; inc EAX
        SHELL_WORD(shellcode, 0x00C7, offset);              // mov dword [EAX], ...
        SHELL_DWORD(shellcode, 0xEC8B55FF, offset);         // rest of the "mov edi, edi; push ebp; mov ebp, esp" prologue

        // complete the original call to LdrLoadDll("kernel32.dll")
        SHELL_BYTE(shellcode, 0xB8, offset);                // mov EAX, <dword>
        SHELL_DWORD(shellcode, AddrLdrLoadDll, offset);     // EAX := &LdrLoadDll
        SHELL_WORD(shellcode, 0xD0FF, offset);              // call EAX
        SHELL_BYTE(shellcode, 0x50, offset);                // push EAX (save LdrLoadDll("kernel32.dll") result)

        // LdrLoadDll("kernel32.dll") completed; load mwmonhk*.dll
        SHELL_BYTE(shellcode, 0x68, offset);                // push <dword>
        SHELL_DWORD(shellcode, hModule, offset);            // PHANDLE ModuleHandle
        SHELL_BYTE(shellcode, 0x68, offset);                // push <dword>
        SHELL_DWORD(shellcode, usDllName, offset);          // PUNICODE_STRING ModuleFileName
        SHELL_WORD(shellcode, 0x006A, offset);              // push 0 (PathToFile)
        SHELL_WORD(shellcode, 0x006A, offset);              // push 0 (Flags)
        SHELL_BYTE(shellcode, 0xB8, offset);                // mov EAX, <dword>
        SHELL_DWORD(shellcode, AddrLdrLoadDll, offset);     // EAX := &LdrLoadDll
        SHELL_WORD(shellcode, 0xD0FF, offset);              // call EAX - todo: check for 0xC0000135 (STATUS_DLL_NOT_FOUND)
        SHELL_BYTE(shellcode, 0x90, offset);                // nop - placeholder for an int3 (0xCC)

        // return
        SHELL_BYTE(shellcode, 0x58, offset);                // pop EAX (recover original call result)
        SHELL_WORD(shellcode, 0x25FF, offset);              // jmp [<dword>]
        SHELL_DWORD(shellcode, shellcode + SHLCD_OFFS_ORIG_RET_ADDR, offset);  // saved original ret.addr. location
        // todo: restore memory protection flags (RX)

        /*
        Sample x86 shellcode disassembly:

            0:000> u 00280000 00280100
            00280000 90              nop
            00280001 58              pop     eax
            00280002 a3c4002800      mov     dword ptr ds:[002800C4h],eax
            00280007 b860e87877      mov     eax,offset ntdll!LdrLoadDll (7778e860)
            0028000c c6008b          mov     byte ptr [eax],8Bh
            0028000f 40              inc     eax
            00280010 c700ff558bec    mov     dword ptr [eax],0EC8B55FFh
            00280016 b860e87877      mov     eax,offset ntdll!LdrLoadDll (7778e860)
            0028001b ffd0            call    eax
            0028001d 50              push    eax
            0028001e 68d8002800      push    2800D8h
            00280023 68c8002800      push    2800C8h
            00280028 6a00            push    0
            0028002a 6a00            push    0
            0028002c b860e87877      mov     eax,offset ntdll!LdrLoadDll (7778e860)
            00280031 ffd0            call    eax
            00280033 90              nop
            00280034 58              pop     eax
            00280035 ff25c4002800    jmp     dword ptr ds:[2800C4h]
        */

        if (offset >= SHLCD_OFFS_ORIG_RET_ADDR)
        {
            MWM_DBG("ERROR: shellcode has overflown (%d > %d bytes)!", offset, SHLCD_OFFS_ORIG_RET_ADDR);
        }
        MWM_DBG("Prepared the 32-bit shellcode @ 0x%p (%d bytes)", shellcode, offset);
    }
    else
    {
        SHELL_BYTE(shellcode, 0x90, offset);                           // nop - placeholder for an int3 (0xCC)

        // reserve space on stack - this is not required if initially hooking LdrLoadDll
        SHELL_BYTE(shellcode, 0x48, offset);                           // prefix
        SHELL_WORD(shellcode, 0xEC81, offset);                         // sub RSP, <dword>
        SHELL_DWORD(shellcode, 0x00000400, offset);                    // 0x400 (1KB)

        // restore stolen bytes
        SHELL_WORD(shellcode, 0xA148, offset);                         // mov RAX, [<qword-addr>]
        SHELL_QWORD(shellcode, shellcode + SHLCD_OFFS_STOLEN_LLD_BYTES, offset);
        SHELL_WORD(shellcode, 0xA348, offset);                         // mov [<qword-addr>], RAX
        SHELL_QWORD(shellcode, AddrLdrLoadDll, offset);
        SHELL_WORD(shellcode, 0xA148, offset);                         // mov RAX, [<qword-addr>]
        SHELL_QWORD(shellcode, shellcode + SHLCD_OFFS_STOLEN_LLD_BYTES + 8, offset);
        SHELL_WORD(shellcode, 0xA348, offset);                         // mov [<qword-addr>], RAX
        SHELL_QWORD(shellcode, (PCHAR)AddrLdrLoadDll + 8, offset);

        // execute original LdrLoadDll & save result
        SHELL_WORD(shellcode, 0xB848, offset);                         // mov RAX, <qword>
        SHELL_QWORD(shellcode, AddrLdrLoadDll, offset);
        SHELL_WORD(shellcode, 0xD0FF, offset);                         // call RAX
        SHELL_BYTE(shellcode, 0x50, offset);                           // push RAX

        // call LdrLoadDll("mwmonhk64.dll")
        SHELL_DWORD(shellcode, 0xC9334890, offset);                    // xor RCX, RCX; nop
        SHELL_DWORD(shellcode, 0xD2334890, offset);                    // xor RDX, RDX; nop
        SHELL_WORD(shellcode, 0xB849, offset);                         // mov R8, <qword>
        SHELL_QWORD(shellcode, usDllName, offset);
        SHELL_WORD(shellcode, 0xB949, offset);                         // mov R9, <qword>
        SHELL_QWORD(shellcode, hModule, offset);
        SHELL_WORD(shellcode, 0xB848, offset);                         // mov RAX, <qword>
        SHELL_QWORD(shellcode, AddrLdrLoadDll, offset);
        SHELL_WORD(shellcode, 0xD0FF, offset);

        SHELL_BYTE(shellcode, 0x58, offset);                           // pop RAX (recover original call result)

        // restore stack pointer
        SHELL_BYTE(shellcode, 0x48, offset);                           // prefix
        SHELL_WORD(shellcode, 0xC481, offset);                         // add RSP, <dword>
        SHELL_DWORD(shellcode, 0x00000400, offset);                    // 0x400 (1KB)

        SHELL_BYTE(shellcode, 0xC3, offset);                           // ret

        /*
        Sample x64 shellcode disassembly:

            0:000> u 0000024e`7e2e0000 0000024e`7e2e0100
            0000024e`7e2e0000 90              nop
            0000024e`7e2e0001 4881ec00040000  sub     rsp,400h
            0000024e`7e2e0008 48a12c012e7e4e020000 mov rax,qword ptr [0000024E7E2E012Ch]
            0000024e`7e2e0012 48a3d05a3ea6fb7f0000 mov qword ptr [ntdll!LdrLoadDll (00007ffb`a63e5ad0)],rax
            0000024e`7e2e001c 48a134012e7e4e020000 mov rax,qword ptr [0000024E7E2E0134h]
            0000024e`7e2e0026 48a3d85a3ea6fb7f0000 mov qword ptr [ntdll!LdrLoadDll+0x8 (00007ffb`a63e5ad8)],rax
            0000024e`7e2e0030 48b8d05a3ea6fb7f0000 mov rax,offset ntdll!LdrLoadDll (00007ffb`a63e5ad0)
            0000024e`7e2e003a ffd0            call    rax
            0000024e`7e2e003c 50              push    rax
            0000024e`7e2e003d 90              nop
            0000024e`7e2e003e 4833c9          xor     rcx,rcx
            0000024e`7e2e0041 90              nop
            0000024e`7e2e0042 4833d2          xor     rdx,rdx
            0000024e`7e2e0045 49b8c8002e7e4e020000 mov r8,24E7E2E00C8h
            0000024e`7e2e004f 49b9d8002e7e4e020000 mov r9,24E7E2E00D8h
            0000024e`7e2e0059 48b8d05a3ea6fb7f0000 mov rax,offset ntdll!LdrLoadDll (00007ffb`a63e5ad0)
            0000024e`7e2e0063 ffd0            call    rax
            0000024e`7e2e0065 58              pop     rax
            0000024e`7e2e0066 4881c400040000  add     rsp,400h
            0000024e`7e2e006d c3              ret
        */

        if (offset >= SHLCD_OFFS_US_DLL_NAME)
        {
            MWM_DBG("ERROR: shellcode has overflown (%d > %d bytes)!", offset, SHLCD_OFFS_US_DLL_NAME);
        }
        MWM_DBG("Prepared the 64-bit shellcode @ 0x%p (%d bytes)", shellcode, offset);
    }

done:
    if (NT_SUCCESS(status))
    {
        *Shellcode = shellcode;
        *ShellcodeSize = shellcodeSize;
    }

    return status;
}


/**
 * Injects the appropriate mwmonhk*.dll into the current process by temporarily hooking LdrLoadDll().
 */
NTSTATUS
MwmInjectMwmonDllInCurrentProcess(
    IN BOOLEAN Is32,
    IN PVOID AddrNtdll,
    IN PVOID AddrLdrLoadDll
    )
{
    NTSTATUS status = STATUS_SUCCESS;
    PKAPC_STATE apcState = (PKAPC_STATE)ExAllocatePool(NonPagedPool, sizeof(KAPC_STATE));
    ULONG oldProt;
    PCHAR shellcode = NULL;
    SIZE_T shellcodeSize = 0;

    UNREFERENCED_PARAMETER(AddrNtdll);

    MWM_DBG("Injecting into process %d (%s)...", PsGetCurrentProcessId(),
            PsGetProcessImageFileName(PsGetCurrentProcess()));

    if (!apcState)
    {
        MWM_DBG("Failed allocating memory for APC state (%p)!", apcState);
        status = STATUS_INSUFFICIENT_RESOURCES;
        goto done;
    }

    KeStackAttachProcess(PsGetCurrentProcess(), apcState);

    {
        // patch LdrLoadDll
        SIZE_T protSize = 0x1000;
        PUCHAR patchAddr = AddrLdrLoadDll;
        PVOID patchAddrCopy = patchAddr;

        CHECK_CALL_STATUS(_MwmPrepareShellcode(AddrLdrLoadDll, Is32, &shellcode, &shellcodeSize));

        // warning: ZwProtectVirtualMemory() alters the second and third parameters (rounds them to page addr/size)
        CHECK_CALL_STATUS(ZwProtectVirtualMemory(ZwCurrentProcess(), &patchAddrCopy, &protSize, PAGE_EXECUTE_READWRITE, &oldProt));

        if (Is32)
        {
            patchAddr[0] = 0xE9;                                            // jmp +4-byte-imm
            *(DWORD*)(patchAddr + 1) = (DWORD)(shellcode - patchAddr) - 5;  // relative offset of shellcode
        }
        else
        {
            memcpy(shellcode + SHLCD_OFFS_STOLEN_LLD_BYTES, patchAddr, 16);
            *(WORD*)patchAddr = 0xB848;                                     // mov RAX, ...
            *(ULONGLONG*)(patchAddr + 2) = (ULONGLONG)shellcode;
            *(WORD*)(patchAddr + 10) = 0xE0FF;                              // jmp RAX
        }

        MWM_DBG("Patched LdrLoadDll @ 0x%p (page: 0x%p [0x%X])", patchAddr, patchAddrCopy, protSize);
    }

    KeUnstackDetachProcess(apcState);

done:
    if (apcState)
    {
        ExFreePool(apcState);
    }

    return status;
}


/**
 * Finds a function's address in the given PE image and optionally returns the bits of the image (32/64) and the
 * corresponding EAT address as well.
 */
NTSTATUS
MwmFindPeExport(
    IN PVOID ImageBase,
    IN PCHAR ExportName,
    OUT DWORD* Bits OPTIONAL,
    OUT PVOID* EatAddress OPTIONAL,
    OUT PVOID* FunctionAddress
    )
{
    PIMAGE_DOS_HEADER dosHdr = (PIMAGE_DOS_HEADER)ImageBase;
	PIMAGE_NT_HEADERS64 ntHdr64;
	PIMAGE_EXPORT_DIRECTORY expDir;
    ULONG i;
    PCHAR imagebase = (PCHAR)ImageBase;
    BOOLEAN is32;

    if (dosHdr->e_magic != IMAGE_DOS_SIGNATURE)
    {
        MWM_DBG("Invalid MZ magic: 0x%04hX", dosHdr->e_magic);
        return STATUS_INVALID_PARAMETER;
    }

    ntHdr64 = (PIMAGE_NT_HEADERS64)(imagebase + dosHdr->e_lfanew);
    if (ntHdr64->Signature != IMAGE_NT_SIGNATURE)
    {
        MWM_DBG("Invalid PE magic: 0x%08X", ntHdr64->Signature);
        return STATUS_INVALID_PARAMETER;
    }

    MWM_DBG("Looking for export %s in image @ 0x%p (OH magic: 0x%04hX)",
            ExportName, ImageBase, ntHdr64->OptionalHeader.Magic);
    is32 = ntHdr64->OptionalHeader.Magic == IMAGE_NT_OPTIONAL_HDR32_MAGIC;
    if (is32)
    {
        expDir = (PIMAGE_EXPORT_DIRECTORY)(imagebase +
                ((PIMAGE_NT_HEADERS32)ntHdr64)->OptionalHeader.DataDirectory[0].VirtualAddress);
    }
    else
    {
        expDir = (PIMAGE_EXPORT_DIRECTORY)(imagebase +
                ntHdr64->OptionalHeader.DataDirectory[0].VirtualAddress);
    }
    if (Bits)
    {
        *Bits = is32 ? 32 : 64;
    }

    MWM_DBG("Found export directory @ %p (%d named exports)", expDir, expDir->NumberOfNames);
    for (i = 0; i < expDir->NumberOfNames; i++)
    {
        if (!strcmp(ExportName, imagebase + *(DWORD*)(imagebase + expDir->AddressOfNames + i * 4)))
        {
            WORD nameOrd = *(WORD*)(imagebase + expDir->AddressOfNameOrdinals + i * 2);
            PVOID eatAddress = (PVOID)(imagebase + expDir->AddressOfFunctions + nameOrd * 4);

            if (EatAddress)
            {
                *EatAddress = eatAddress;
            }
            *FunctionAddress = (PVOID)(imagebase + *(DWORD*)eatAddress);

            return STATUS_SUCCESS;
        }
    }

    return STATUS_NOT_FOUND;
}


/**
 * Checks if a (loaded) PE image is 32 bits (WoW64).
 */
BOOLEAN
MwmPeImageIs32Bit(
    IN PVOID ImageBase
    )
{
    PIMAGE_DOS_HEADER dosHdr = (PIMAGE_DOS_HEADER)ImageBase;
	PIMAGE_NT_HEADERS32 ntHdr = (PIMAGE_NT_HEADERS32)((PCHAR)ImageBase + dosHdr->e_lfanew);

    return ntHdr->OptionalHeader.Magic == IMAGE_NT_OPTIONAL_HDR32_MAGIC;
}


/**
 * Converts an ASCII string to WCHAR.
 */
NTSTATUS
MwmAsciiToWchar(
    IN CHAR* AsciiString,
    IN SIZE_T WcharMaxChars,
    OUT WCHAR* WcharString
    )
{
    SIZE_T i;

    for (i = 0; AsciiString[i] && (i < WcharMaxChars); i++)
    {
        WcharString[i] = (WCHAR)AsciiString[i];
    }

    if (i >= WcharMaxChars)
    {
        return STATUS_BUFFER_TOO_SMALL;
    }

    WcharString[i] = 0;

    return STATUS_SUCCESS;
}


/**
 * Debug-prints a buffer as hex dump.
 */
void
MwmHexdump(
    IN PCHAR Title,
    IN PVOID Address,
    IN SIZE_T Count
    )
{
    CHAR buffer[600];
    DWORD i;

    memset(buffer, 0, sizeof(buffer));
    Count = min(Count, sizeof(buffer) / 3);
    MWM_DBG("%s (%d bytes):", Title, Count);
    for (i = 0; i < Count; i++)
    {
        if ((i & 0xF) == 0)
        {
            DbgPrint("    %p:  ", (PUCHAR)Address + ((i >> 4) * 16));
        }
        _snprintf_s(&buffer[(i * 3) % (3 * 16)], sizeof(buffer) / 3, sizeof(buffer) / 3,
                "%02hhX ", (UCHAR)((PUCHAR)Address)[i]);
        if ((i & 0xF) == 0xF)
        {
            DbgPrint("%s\n", buffer);
            memset(buffer, 0, sizeof(buffer));
        }
    }
    if ((i & 0xF) != 0)
    {
        DbgPrint("%s\n", buffer);
        memset(buffer, 0, sizeof(buffer));
    }
}
