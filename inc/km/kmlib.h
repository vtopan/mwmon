/**
 * mwmon/kmlib.h: Various kernel utility functions.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#pragma once

#include <ntifs.h>
#include <wdm.h>
#include <ntimage.h>
#include <ntintsafe.h>
#include <stdio.h>
#include "umkmdefs.h"


#define MWM_DBG(msg, ...) DbgPrint("[mwmondrv] " msg "\n", ##__VA_ARGS__)
#define CHECK_CALL_STATUS(op) if (!NT_SUCCESS(status = (op))) {MWM_DBG("<" __FUNCTION__ "()@%d> " #op " failed! Error: 0x%X", __LINE__, status); goto done;}


extern
NTSYSAPI
CHAR*
NTAPI
PsGetProcessImageFileName(
    PEPROCESS Process
    );

extern
NTSYSAPI
PPEB
NTAPI
PsGetProcessPeb(
    PEPROCESS Process
    );

extern
NTSYSAPI
PPEB
NTAPI
PsGetProcessWow64Process(
    PEPROCESS Process
    );

extern
NTSYSAPI
void
NTAPI
KeInitializeApc(
	PRKAPC Apc,
	PRKTHREAD Thread,
	UINT32 Environment,
	PVOID KernelRoutine,
	PVOID RundownRoutine,
	PVOID NormalRoutine,
	KPROCESSOR_MODE ProcessorMode,
	PVOID NormalContext
	);

extern
NTSYSAPI
NTSTATUS
NTAPI
ZwProtectVirtualMemory(
    IN HANDLE ProcessHandle,
    IN OUT PVOID *BaseAddress,
    IN OUT SIZE_T *NumberOfBytesToProtect,
    IN ULONG NewAccessProtection,
    OUT ULONG *OldAccessProtection
    );


NTSTATUS
MwmResolveDynamicImport(
    OUT PVOID *Import,
    IN PWSTR Name
    );

NTSTATUS
MwmInjectMwmonDllInCurrentProcess(
    IN BOOLEAN Is32,
    IN PVOID AddrNtdll,
    IN PVOID AddrLdrLoadDll
    );

NTSTATUS
MwmFindPeExport(
    IN PVOID ImageBase,
    IN PCHAR ExportName,
    OUT DWORD* Bits OPTIONAL,
    OUT PVOID* EatAddress OPTIONAL,
    OUT PVOID* FunctionAddress
    );

BOOLEAN
MwmPeImageIs32Bit(
    IN PVOID ImageBase
    );

NTSTATUS
MwmAsciiToWchar(
    IN CHAR* AsciiString,
    IN SIZE_T WcharMaxChars,
    OUT WCHAR* WcharString
    );

void
MwmHexdump(
    IN PCHAR Title,
    IN PVOID Address,
    IN SIZE_T Count
    );