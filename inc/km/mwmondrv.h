/**
 * mwmon/mwmondrv.h: Minifilter header.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#pragma once

#include <fltKernel.h>
#include <wdm.h>
#include "umkmdefs.h"
#include "kmlib.h"

extern PCHAR STR_SE_IMAGE_SIGNATURE_TYPE[];
extern PCHAR STR_SE_SIGNING_LEVEL[];


typedef struct _G_MWMON {
    PFLT_FILTER Filter;
    PFLT_PORT CommPort;
    PFLT_PORT ClientPort;
    PVOID AddrLoadLibraryA32;
    PVOID AddrLoadLibraryA64;
    PVOID AddrLdrLoadDll32;
    PVOID AddrLdrLoadDll64;
    PVOID AddrNtdll32;
    PVOID AddrNtdll64;
    BOOLEAN ImgLoadNotifyEnabled;
} G_MWMON;


NTSTATUS
DriverEntry(
    IN PDRIVER_OBJECT DriverObject,
    IN PUNICODE_STRING RegistryPath
    );

void
DriverUnload(
    PDRIVER_OBJECT DriverObject
    );

NTSTATUS
FilterUnloadCallback(
    IN FLT_FILTER_UNLOAD_FLAGS Flags
    );

void
ImageLoaded(
    IN PUNICODE_STRING FullImageName,
    IN HANDLE ProcessId,
    IN PIMAGE_INFO ImageInfo
    );

NTSTATUS
ClientConnectedCallback (
    IN PFLT_PORT ClientPort,
    IN PVOID ServerPortCookie,
    IN PVOID ConnectionContext,
    IN ULONG SizeOfContext,
    OUT PVOID *ConnectionPortCookie
    );

void
ClientDisconnectedCallback(
    IN PVOID Cookie
    );

NTSTATUS
MessageCallback(
    IN PVOID PortCookie,
    IN PVOID InputBuffer,
    IN ULONG InputBufferLength,
    OUT PVOID OutputBuffer OPTIONAL,
    IN ULONG OutputBufferLength,
    OUT PULONG ReturnOutputBufferLength
    );