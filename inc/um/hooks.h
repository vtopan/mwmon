/*
 * API hooks.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winternl.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <wincrypt.h>
#include <ncryptprotect.h>
#include <wininet.h>
#include <sddl.h>
#include <shellapi.h>

#include "common.h"
#include "logging.h"
#include "misc.h"
#include "libhook.h"


extern DWORD HookCount;
extern __declspec(dllexport) HOOK Hooks[];
