/*
 * mwmonhk*.dll header.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <string.h>

#include "common.h"
#include "logging.h"
#include "libhook.h"
#include "ntdll.h"
#include "misc.h"
