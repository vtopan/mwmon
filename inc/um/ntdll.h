/*
 * ntdll APIs & typedefs.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#pragma warning(push)
#pragma warning(disable:4201)
#include <winternl.h>
#pragma warning(pop)

#pragma comment(lib, "ntdll")


#define LDR_DLL_NOTIFICATION_REASON_LOADED 1
#define LDR_DLL_NOTIFICATION_REASON_UNLOADED 2


typedef NTSTATUS (NTAPI *FUN_LdrRegisterDllNotification)(ULONG Flags, PVOID NotificationFunction, PVOID Context, PVOID *Cookie);
typedef int (__cdecl *FUN_snprintf)(char *buffer, size_t count, const char *format, ...);
typedef int (__cdecl *FUN_vsprintf_s)(char *buffer, size_t numberOfElements, const char *format, va_list argptr);
typedef int (__cdecl *FUN_swprintf_s)(wchar_t *buffer, size_t sizeOfBuffer, const wchar_t *format, ...);

typedef struct _LDR_DLL_LOADED_NOTIFICATION_DATA {
    ULONG Flags;                    //Reserved.
    PCUNICODE_STRING FullDllName;   //The full path name of the DLL module.
    PCUNICODE_STRING BaseDllName;   //The base file name of the DLL module.
    PVOID DllBase;                  //A pointer to the base address for the DLL in memory.
    ULONG SizeOfImage;              //The size of the DLL image, in bytes.
} LDR_DLL_LOADED_NOTIFICATION_DATA, *PLDR_DLL_LOADED_NOTIFICATION_DATA;

typedef struct _LDR_DLL_UNLOADED_NOTIFICATION_DATA {
    ULONG Flags;                    //Reserved.
    PCUNICODE_STRING FullDllName;   //The full path name of the DLL module.
    PCUNICODE_STRING BaseDllName;   //The base file name of the DLL module.
    PVOID DllBase;                  //A pointer to the base address for the DLL in memory.
    ULONG SizeOfImage;              //The size of the DLL image, in bytes.
} LDR_DLL_UNLOADED_NOTIFICATION_DATA, *PLDR_DLL_UNLOADED_NOTIFICATION_DATA;

typedef union _LDR_DLL_NOTIFICATION_DATA {
    LDR_DLL_LOADED_NOTIFICATION_DATA Loaded;
    LDR_DLL_UNLOADED_NOTIFICATION_DATA Unloaded;
} LDR_DLL_NOTIFICATION_DATA, *PLDR_DLL_NOTIFICATION_DATA;


extern FUN_swprintf_s ntdll_swprintf_s; // note: sizeOfBuffer is in characters, not bytes
extern FUN_snprintf ntdll_snprintf;
extern FUN_vsprintf_s ntdll_vsprintf_s;


void
init_ntdll_wrappers(
    void
    );

NTSYSAPI
NTSTATUS
NTAPI
NtWriteFile(
    HANDLE FileHandle,
    HANDLE Event,
    FARPROC ApcRoutine,
    PVOID ApcContext,
    PIO_STATUS_BLOCK IoStatusBlock,
    PVOID Buffer,
    ULONG Length,
    PLARGE_INTEGER ByteOffset,
    PULONG Key
    );

NTSYSAPI
NTSTATUS
NTAPI
LdrGetDllFullName(
    PVOID DllHandle,
    PUNICODE_STRING FullDllName
    );


