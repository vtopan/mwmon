/*
 * Misc functions.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdio.h>
#include <winternl.h>

#include "common.h"
#include "ntdll.h"
#include "shlwapi.h"
#include "logging.h"


// get_teb() / get_peb(): Get a pointer to the current process' TEB / PEB
#ifdef _M_X64
#define get_teb() ((PTEB)__readgsqword(0x30))
#define get_peb() ((PPEB)__readgsqword(0x60))
#else
#define get_teb() ((PTEB)__readfsdword(0x18))
#define get_peb() ((PPEB)__readfsdword(0x30))
#endif // 64 bits

#define MWM_NO_TAG -1234554321

#define IS_ASCII_CHAR(x) (((BYTE)(x) < 0x80) && (((x) >= 0x20) || ((x) == 0x09) || ((x) == 0x0A) || ((x) == 0x0D)))

extern WCHAR _WS_INVALID_PTR[];
extern PUNICODE_STRING _UCS_INVALID_PTR;
extern WCHAR _WS_NULL[];
extern PUNICODE_STRING _UCS_NULL;
extern PBYTE HOOKDLL_ADDR;
extern size_t HOOKDLL_SIZE;

#define PRINT_STR(s) (IsBadReadPtr(s, 1) ? ((s) ? "INVALID-PTR" : "NULL") : s)
#define PRINT_WSTR(s) (IsBadReadPtr(s, 2) ? ((s) ? _WS_INVALID_PTR : _WS_NULL) : s)
#define PRINT_USTR(s) (IsBadReadPtr(s, 8) ? ((s) ? _UCS_INVALID_PTR : _UCS_NULL) : s)


__declspec(dllexport)
void
init_miscvars(
    void
    );

__declspec(dllexport)
int
escape_binary(
    char* in_buffer,
    size_t size,
    char* out_buffer
    );

PCSTR
_inet_ntop(
    int Family,
    const void* pAddr,
    char* pStringBuf,
    size_t StringBufSize
    );

__declspec(dllexport)
void
save_binary_buffer(
    char* api,
    char* tag_string,
    int tag_value,
    BYTE* buffer,
    int buffer_size
    );