/**
 * libhook: hooking structures, constants & APIs.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdlib.h>

#include "common.h"
#include "logging.h"
#include "umkmdefs.h"
#include "ntdll.h"
#include "misc.h"


#define HOOK_GATE_BASE_ADDR(ntdll_addr) (((size_t)(ntdll_addr) & (size_t)~0xFFFFFF) + (size_t)(512 * 1024 * 1024))
#define GENHOOK_SLOT_SIZE (512 * 1024)
#ifdef _M_X64
// ~10k genhook entries per index, after ntdll, leave one 512K slot for hook gates and one for dynamic genric hooks
#define GENHOOK_SLOT_ADDR(ntdll_addr, index) (size_t)(HOOK_GATE_BASE_ADDR(ntdll_addr) + (index + 2) * GENHOOK_SLOT_SIZE)
#else
// random allocation
#define GENHOOK_SLOT_ADDR(ntdll_addr, index) 0
#endif
#define DYN_GEN_HOOK_INDEX 0

// opcodes
#define PUSH_32 0x68
#define CALL_REL_32 0xE8
#define JMP_REL_32 0xE9
#define POP_EAX 0x58
#define PUSH_ESP 0x54

#ifdef _M_X64
#define GENHOOK_STUB_SIZE 64 // (3 * 8 + 32)
#else
#define GENHOOK_STUB_SIZE 32 // (3 * 4 + 6)
#endif

#define SELF64 (sizeof(size_t) == 8)
#define INJECT_SIZE 4096
#define GATE_SIZE 128
#define HOOK_GATE_ADDR(i) ((PBYTE)HOOK_GATES + (i) * HOOK_GATE_SIZE)

#define HOOKFUN(rettype) __declspec(dllexport) rettype WINAPI

#if CFG_DEBUG_HOOKS
#define HOOK_ENTER(fun) if (CFG_DEBUG_HOOKS) log("[#] Entering hook for %s() (original addr: 0x%p)...", #fun, Hooks[HOOKID_ ## fun].orig_addr); hook_enter(); //__try {
#define HOOK_LEAVE() hook_leave(); // } __except(1) {err("EXCEPTION: 0x%08X!", GetExceptionCode());}
#define HOOK_DEPTH() hook_depth()
#else
#define HOOK_ENTER(fun) if (CFG_DEBUG_HOOKS) if (!(Hooks[HOOKID_ ## fun].orig_addr)) err("Original address for %s() is null!", #fun); hook_enter(); //__try {
#define HOOK_LEAVE() hook_leave(); // } __except(1) {err("EXCEPTION: 0x%08X!", GetExceptionCode());}
#define HOOK_DEPTH() hook_depth()
#endif

#ifdef _M_X64
#define HOOK_GATE_SIZE 16   // 48 B8 <8-byte-address> FF EO (mov rax, <qword>; jmp rax) + 4 bytes padding
#else
#define HOOK_GATE_SIZE 8    // B8 <4-byte-address> FF EO (mov eax, <dword>; jmp eax) + 1 byte padding
#endif


// these should be in hooks.h, but this makes it easier to avoid cross-includes
enum {
    // ntdll
    HOOKID_LdrGetProcedureAddress = 0,
    HOOKID_LdrGetProcedureAddressForCaller,
    HOOKID_LdrLoadDll,
    HOOKID_NtOpenFile,
    HOOKID_NtOpenKey,
    HOOKID_NtQueryValueKey,
    HOOKID_RtlInitUnicodeString,
    // kernel32
//    HOOKID_CloseHandle,
    HOOKID_CopyFileA,
    HOOKID_CopyFileExA,
    HOOKID_CopyFileExW,
    HOOKID_CopyFileW,
    HOOKID_CreateFileA,
    HOOKID_CreateFileW,
    HOOKID_CreateProcessA,
    HOOKID_CreateProcessInternalW,
    HOOKID_CreateProcessW,
    HOOKID_CreateRemoteThread,
    HOOKID_DeviceIoControl,
    HOOKID_FindFirstFileA,
    HOOKID_FindFirstFileExA,
    HOOKID_FindFirstFileExW,
    HOOKID_FindFirstFileW,
    HOOKID_FindResourceA,
    HOOKID_FindResourceExA,
    HOOKID_FindResourceExW,
    HOOKID_FindResourceW,
    HOOKID_FlsAlloc,
    HOOKID_GetFileAttributesA,
    HOOKID_GetFileAttributesExA,
    HOOKID_GetFileAttributesExW,
    HOOKID_GetFileAttributesW,
    HOOKID_GetModuleFileNameA,
    HOOKID_GetModuleFileNameW,
    HOOKID_GetModuleHandleA,
    HOOKID_GetModuleHandleW,
    HOOKID_GetProcAddress,
    HOOKID_GetStdHandle,
    HOOKID_GlobalFree,
    HOOKID_HeapFree,
    HOOKID_IsProcessorFeaturePresent,
    HOOKID_LoadLibraryA,
    HOOKID_LoadLibraryExA,
    HOOKID_LoadLibraryExW,
    HOOKID_LoadLibraryW,
    HOOKID_LocalFree,
    HOOKID_MoveFileA,
    HOOKID_MoveFileW,
    HOOKID_MultiByteToWideChar,
    HOOKID_ReadProcessMemory,
    HOOKID_SetFileAttributesA,
    HOOKID_SetFileAttributesW,
    HOOKID_Sleep,
    HOOKID_VirtualAlloc,
    HOOKID_VirtualFree,
    HOOKID_VirtualProtectEx,
    HOOKID_WideCharToMultiByte,
    HOOKID_WriteFile,
    HOOKID_WriteProcessMemory,
    // advapi32
    HOOKID_CheckTokenMembership,
    HOOKID_CryptDecrypt,
    HOOKID_CryptDestroyKey,
    HOOKID_CryptEncrypt,
    HOOKID_CryptGenKey,
    HOOKID_CryptHashData,
    HOOKID_CryptImportKey,
    HOOKID_EqualSid,
    HOOKID_GetTokenInformation,
    HOOKID_OpenProcessToken,
    // dpapi
    HOOKID_CryptProtectMemory,
    HOOKID_CryptUnprotectMemory,
    // crypt32
    HOOKID_CertGetNameStringA,
    HOOKID_CryptBinaryToStringA,
    HOOKID_CryptBinaryToStringW,
    HOOKID_CryptDecodeObject,
    HOOKID_CryptDecodeObjectEx,
    HOOKID_CryptEncodeObject,
    HOOKID_CryptEncodeObjectEx,
    HOOKID_CryptMsgOpenToDecode,
    HOOKID_CryptMsgOpenToEncode,
    HOOKID_CryptMsgUpdate,
    HOOKID_CryptProtectData,
    HOOKID_CryptStringToBinaryA,
    HOOKID_CryptStringToBinaryW,
    HOOKID_CryptUnprotectData,
    // ncrypt
    HOOKID_NCryptProtectSecret,
    HOOKID_NCryptUnprotectSecret,
    // shell32
    HOOKID_ShellExecuteA,
    HOOKID_ShellExecuteW,
    HOOKID_ShellExecuteExA,
    HOOKID_ShellExecuteExW,
    // ws2_32
    HOOKID_connect,
    HOOKID_recv,
    HOOKID_send,
    HOOKID_sendto,
    // urlmon
    HOOKID_URLDownloadToFileA,
    HOOKID_URLDownloadToFileW,
    // bcrypt
    HOOKID_BCryptDecrypt,
    HOOKID_BCryptEncrypt,
    // winhttp
    HOOKID_WinHttpConnect,
    HOOKID_WinHttpOpen,
    HOOKID_WinHttpOpenRequest,
    HOOKID_WinHttpQueryHeaders,
    HOOKID_WinHttpReadData,
    HOOKID_WinHttpSendRequest,
    HOOKID_WinHttpSetCredentials,
    HOOKID_WinHttpWriteData,
    // wininet
    HOOKID_FtpOpenFileA,
    HOOKID_FtpOpenFileW,
    HOOKID_HttpOpenRequestA,
    HOOKID_HttpOpenRequestW,
    HOOKID_InternetConnectA,
    HOOKID_InternetConnectW,
    HOOKID_InternetOpenA,
    HOOKID_InternetOpenUrlA,
    HOOKID_InternetOpenUrlW,
    HOOKID_InternetOpenW,
    HOOKID_InternetReadFile,
    // user32
    HOOKID_EnumWindows,
    HOOKID_FindWindowA,
    HOOKID_FindWindowExA,
    HOOKID_FindWindowExW,
    HOOKID_FindWindowW,
    HOOKID_GetDlgItemTextA,
    HOOKID_GetDlgItemTextW,
    HOOKID_SetDlgItemTextA,
    HOOKID_SetDlgItemTextW,
    // rpcrt4.dll
    HOOKID_RpcStringBindingComposeA,
    HOOKID_RpcStringBindingComposeW,
    // shlwapi.dll
    HOOKID_StrStrIW,
    HOOKID_COUNT
    };



extern WCHAR WPROCNAME[260];
extern CHAR PROCNAME[260];


typedef void* (__stdcall *fun0arg)(void);
typedef void* (__stdcall *fun1arg)(void*);
typedef void* (__stdcall *fun2arg)(void*, void*);
typedef void* (__stdcall *fun3arg)(void*, void*, void*);
typedef void* (__stdcall *fun4arg)(void*, void*, void*, void*);
typedef void* (__stdcall *fun5arg)(void*, void*, void*, void*, void*);
typedef void* (__stdcall *fun6arg)(void*, void*, void*, void*, void*, void*);
typedef void* (__stdcall *fun7arg)(void*, void*, void*, void*, void*, void*, void*);
typedef void* (__stdcall *fun8arg)(void*, void*, void*, void*, void*, void*, void*, void*);
typedef void* (__stdcall *fun9arg)(void*, void*, void*, void*, void*, void*, void*, void*, void*);
typedef void* (__stdcall *fun10arg)(void*, void*, void*, void*, void*, void*, void*, void*, void*, void*);
typedef void* (__stdcall *fun11arg)(void*, void*, void*, void*, void*, void*, void*, void*, void*, void*, void*);
typedef void* (__stdcall *fun12arg)(void*, void*, void*, void*, void*, void*, void*, void*, void*, void*, void*, void*);
typedef void* (__stdcall *fun13arg)(void*, void*, void*, void*, void*, void*, void*, void*, void*, void*, void*, void*, void*);
typedef void* (__stdcall *fun14arg)(void*, void*, void*, void*, void*, void*, void*, void*, void*, void*, void*, void*, void*, void*);
typedef void* (__stdcall *fun15arg)(void*, void*, void*, void*, void*, void*, void*, void*, void*, void*, void*, void*, void*, void*, void*);


typedef struct {
    void* hook_addr;
    char* dll_name;
    char* fun_name;
    // (only) the above are initialized statically, preserve this order!
    void* orig_addr;
    void* hook_gate_addr;
    BOOLEAN is_set;
} HOOK;

typedef struct {
    char* dll_name_prefix;
    char* fun_name;
    int hook_id;
    BOOLEAN is_set;
} FORWARDER_HOOK;


__declspec(dllexport)
BOOL
init_libhook(
    void
    );

__declspec(dllexport)
HANDLE
inject_mwmon_dll(
    HANDLE process
    );

__declspec(dllexport)
BOOL
install_hooks(
    PCHAR dll_name,
    BOOL warn_if_not_loaded
    );

__declspec(dllexport)
BOOL
install_generic_hooks(
    PCHAR dll_name,
    BOOL warn_if_not_loaded
    );

#if CFG_HOOK_GETPROC_EXPORTS
__declspec(dllexport)
DWORD
install_dynamic_generic_hook(
    PBYTE dll_imagebase,
    char* export_name,
    PVOID* hook_addr
    );
#endif

__declspec(dllexport)
void
hook_enter(
    void
    );

__declspec(dllexport)
void
hook_leave(
    void
    );

__declspec(dllexport)
int
hook_depth(
    void
    );

__declspec(dllexport)
BOOL
register_hook_LdrLoadDll(
    void
    );