/**
 * mwmon/common.h: Common UM header.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#pragma once

#define WIN32_LEAN_AND_MEAN
#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <windows.h>

#include "umkmdefs.h"

#define OUT_PATH L"c:\\mwmon"

#ifdef _M_X64
#define MWM_BITS "64"
#define MWM_WBITS L"64"
#else
#define MWM_BITS "32"
#define MWM_WBITS L"32"
#endif

#define PATH_EXISTS(path) (GetFileAttributesW(path) != INVALID_FILE_ATTRIBUTES)


#define CHECK_CALL_BOOL(op) if (!(op)) {result = GetLastError(); err(#op " failed! Last error: 0x%X", result); goto done;}
#define CHECK_CALL_HANDLE(op) if ((op) == INVALID_HANDLE_VALUE) {result = GetLastError(); err(#op " returned invalid handle! Last error: 0x%X", result); goto done;}
#define CHECK_CALL_PTR(op) if ((op) == NULL) {result = GetLastError(); err(#op " failed! Last error: 0x%X", result); goto done;}
#define CHECK_CALL_STATUS(op) if ((result = (op)) != ERROR_SUCCESS) {err(#op " failed! Error: 0x%X", result); goto done;}
