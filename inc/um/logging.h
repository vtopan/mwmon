/*
 * logging.h: logging tools.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include "ntdll.h"

#include "common.h"

extern int DEBUG;

#define log(fmt, ...) out(0, "<T%d> " fmt "\n", GetCurrentThreadId(), ##__VA_ARGS__)
#define err(fmt, ...) out(1, "[!] " fmt "\n", ##__VA_ARGS__)
#define dbg(level, fmt, ...) {if (DEBUG && ((level) <= DEBUG)) out(0, "[#] " fmt "\n", ##__VA_ARGS__);}


BOOL
init_logging(
    WCHAR* file_name
);

void
out(
    BOOL is_err,
    const char* format,
    ...
);

BOOL
log_to_file(
    WCHAR* file_name
);

void
stop_logging(
    void
);