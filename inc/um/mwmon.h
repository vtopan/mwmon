/*
 * mwmon.exe header.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdio.h>
#include <stdarg.h>
#include <wchar.h>

#include "common.h"
#include "logging.h"
#include "libhook.h"
#include "misc.h"
