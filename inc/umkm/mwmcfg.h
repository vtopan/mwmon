/**
 * mwmcfg: Configuration file.
 *
 * Author: Vlad Topan (vtopan/gmail)
 */

#pragma once

#define CFG_DEBUG 0
#define CFG_DEBUG_HOOKS 0
#define CFG_HOOK_ALL_EXPORTS 0
#define CFG_HOOK_GETPROC_EXPORTS 1
#define CFG_SUSPEND_MS_AUTORUN_BLOATWARE 1
#define CFG_GENHOOK_DUMP_STRING_ARGS_HEUR 1
#define CFG_GENHOOK_DUMP_NONSTRING_ARGS_AS_HEX 0
