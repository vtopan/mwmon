/**
 * mwmon/umkmdefs.h: Common UM-KM definitions.
 */

#pragma once

#ifdef _KERNEL_MODE
#include <wdm.h>
#include <ntintsafe.h>
#else
#include <Windows.h>
#endif
#include "mwmcfg.h"

#define MWMVER "0.1.00020 (2021.05.09)"

#define COMM_PORT_NAME L"\\mwmon_cp"
#define MWMON_REL_PATH "\\mwmon"
#define MWMON_PATH "c:" MWMON_REL_PATH
#define MWMON_DLL_BASE "mwmonhk"
#ifdef MWMON_USE_DEBUG_DLLS
#define MWMON_DLL_NAME_32 MWMON_DLL_BASE "32.debug.dll"
#define MWMON_DLL_NAME_64 MWMON_DLL_BASE "64.debug.dll"
#else
#define MWMON_DLL_NAME_32 MWMON_DLL_BASE "32.dll"
#define MWMON_DLL_NAME_64 MWMON_DLL_BASE "64.dll"
#endif
#define MWMON_DLL_PATH_32 (MWMON_PATH "\\" MWMON_DLL_NAME_32)
#define MWMON_DLL_PATH_64 (MWMON_PATH "\\" MWMON_DLL_NAME_64)

// the following macros emit data into a buffer and update the offset
#define SHELL_BYTE(buffer, value, offset) buffer[offset++] = value;
#define SHELL_WORD(buffer, value, offset) *(WORD*)(buffer + offset) = (WORD)(value); offset += 2;
#define SHELL_DWORD(buffer, value, offset) *(DWORD*)(buffer + offset) = (DWORD)(value); offset += 4;
#define SHELL_QWORD(buffer, value, offset) *(ULONGLONG*)(buffer + offset) = (ULONGLONG)(value); offset += 8;
// SHELL_RAW assumes `value` is a C string constant
#define SHELL_RAW(buffer, value, offset) memcpy(&buffer[offset], value, sizeof(value) - 1); offset += sizeof(value) - 1;


typedef enum _MWM_MESSAGE_TYPE {
    MwmMtPlaceholder = 1
} MWM_MESSAGE_TYPE;


#pragma pack(push, 1)
typedef struct _MWM_MESSAGE {
    MWM_MESSAGE_TYPE Type;
    DWORD PayloadSize;
    union {
        BYTE Payload[0];
        ULONGLONG AsULONGLONG[0];
        DWORD AsDWORD[0];
        WORD AsWORD[0];
        };
} MWM_MESSAGE, *PMWM_MESSAGE;
#pragma pack(pop)

#define MWM_MESSAGE_SIZE(msg) ((msg)->PayloadSize + 8)
