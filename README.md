# mwmon

Windows malware analysis tool based on API monitoring; alpha stage as of Nov. 2019.

Vlad Topan (vtopan/gmail)


**WARNING: run malware exclusively in isolated VMs!**


## Concept / architecture

This project is intended to be a sample / PoC of functional malware monitoring via API hooking (using
a kernel driver to easily intercept process creation). It is by no means a production-ready, heavy-load
malware processing tool, but feedback / patches / improvements are welcome. Do note that it makes no
effort to conceal itself or to protect the hooks, so most contemporary malware could easily detect it.

New process creation is intercepted via a `PsSetLoadImageNotifyRoutine` callback which tracks the loading
of `ntdll.dll` (for the address of `LdrLoadDll`) and `kernel32.dll` (to actually hook `LdrLoadDll` once
`kernel32.dll` has finished loading to install the hooks on the next call). Note: this may mean some
executables won't be traced (e.g. if no other DLL is loaded explicitly via `LdrLoadDll`) - this will
be fixed when I have enough time to find a better alternative (e.g. an APC).

The `mwmonhk32/64.dll` is injected in new processes and proceeds to patch the EAT and IAT entries of
the target functions (randomly chosen to be somewhat relevant for malware monitoring; more can be
easily added, see `src/um/hooks.c`). Inline hooks are avoided - this makes code more resilient to M$
patches and less complex, but requires that each function to be traced must be hooked individually (i.e.
the calls to `CreateFileA` do not go through a hook set on `NtCreateFile`).

The API logs are created in `c:\mwmon`, as well as the binary buffer dumps for some APIs (e.g. `WriteFile`).


## Usage

- build the project (see below) or download the latest release `.zip` from [here](https://gitlab.com/vtopan/mwmon/-/tags)
- the one-shot (single-process) trace only requires the executable `mwmon32.exe` and the hooking DLL
    `mwmonhk32.dll` somewhere in the load path (e.g. the current folder and / or the same folder as
    the executable)
    - run `mwmon32.exe somefile.exe` and check the folder `c:\mwmon` for the logs
- to trace all newly spawned processes, install the *kernel-mode driver* (see below), which will inject
    bit-appropriate (32/64) DLLs in all newly spawned processes (with a whitelist)
    - the DLLs must be placed somewhere in the appropriate `%PATH%` (e.g. `mwmonhk32.dll` in
        `c:\windows\syswow64` and `mwmonhk64.dll` in `c:\windows\system32`)


### Installing the driver

- the driver is not WHQL-signed; you have three options to load it:
    - WHQL-sign the driver (I know, right?)
    - enable *TestSigning* (run `tools/once/enable_testsigning.bat`) and reboot
        - note: `bcdedit` (used by the batch file) needs a 64-bit *Administrator* command line (`cmd.exe`);
            the 32-bit one doesn't have the command in `%PATH%`
    - disable driver signature enforcement during boot (hold `Shift` & press *Restart*, *Troubleshoot*,
    *Advanced options*, *Startup Settings*, *F7*)
- *(optional)* enable kernel debugging on the machine (e.g. for a VirtualBox VM run `tools/once/enable_vbox_kmdbg.bat`)
- *(optional)* enable DebugPrint messages (import `tools/once/enable_debug_logging.reg`)
- run `tools/install_driver.bat` from a **64-bit** *Administrator* command line (`cmd.exe`)
    - the output should contain the line `STATE              : 4  RUNNING`; if not, something went wrong
- at this point, each new process will attempt to load the hooks (i.e. `mwmonhk32.dll`), which needs to be
    somewhere in `%PATH%` for this to work (e.g. in `c:\windows\syswow64` for the 32-bit DLL assuming a 64-bit OS)
- to stop the driver, either run `tools/stop_driver.bat` or simply `fltmc unload mwmon`
- to uninstall the driver, run `tools/remove_driver.bat`


## Build

- get the EWDK: <https://developer.microsoft.com/en-us/windows/hardware/license-terms-EWDK-2>
    - includes the Windows SDK, compilers (MSVC) and the DDK
    - mount the .iso using e.g. [ImDisk](http://www.ltr-data.se/opencode.html/#ImDisk)
- run `configure.bat` which will find the `.iso` mount drive and save it to `configured.cmd` (e.g. `set EWDK_DRIVE=X:`)
- run `build.bat` to build (using `msbuild` internally - the parameters it takes are passed directly to it), e.g.:
    - `build.bat -nologo -t:build -p:Configuration=Release+Debug -maxcpucount:4`
    - if a `custom_configured.cmd` file exists, it can set a `CopyDriverTo` env. variable to which
        the driver (and other useful deployment files) will be copied on successful builds (see the `.template` file)
- building from *Visual Studio* most likely won't work (the `.vcxproj` files are created manually and have no `Label`s)


## Feedback

Feedback is most welcome at *vtopan/gmail* or as issues on [gitlab](https://gitlab.com/vtopan/mwmon).
